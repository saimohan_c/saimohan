# Standard imports 
import requests
import psycopg2
import os,sys
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Third party imports
import pytest
import subprocess
from pconf import Pconf

sys.path.append(str(os.getcwd()))



@pytest.fixture(scope='module')
def setup_database():
    #Pconf.env()
    #Pconf.file('/dbconf.json', encoding='json')
    # Get all the config values parsed from the sources
    config = {
        "host":"localhost",
        "dbname":"reomnify_test",
        "user":"postgres",
        "password":"saimohan1",
        }
    """ Fixture to set up the in-memory database with test data """
    conn = psycopg2.connect(host=config['host'], user=config['user'],password=config['password'])

    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    conn.cursor().execute("DROP DATABASE IF EXISTS {0}".format(config['dbname']))
    conn.cursor().execute("CREATE DATABASE {0}".format(config['dbname']))
    conn = psycopg2.connect(host=config['host'], dbname=config['dbname'], user=config['user'],password=config['password'],options=f'-c search_path=public')

    print("Initializing the data base ... ")
    with conn.cursor() as cursor:
        
        cursor.execute(open("./reomnify_poi_ingestion/schemas/init.sql", "r").read())
        cursor.execute(open("./reomnify_poi_ingestion/schemas/migrate_alter.sql", "r").read())
        cursor.execute(open("./reomnify_poi_ingestion/schemas/seed.sql", "r").read())
        cursor.execute(open("./reomnify_poi_ingestion/schemas/migrate_seed.sql", "r").read())

        cursor.execute("SET search_path=public")
        cursor.execute("ALTER DATABASE {0} SET search_path=public".format(config['dbname']))
        cursor.execute("ALTER ROLE postgres  IN DATABASE {0} SET search_path=public".format(config['dbname']))
        conn.commit()

        cursor.execute("SHOW search_path;")
        result = cursor.fetchall()
        print("Search Path : ",result)
        
        '''cursor.execute("SELECT COUNT(*) FROM master_countries")
        result = cursor.fetchone()[0]
        print("No of countries currently our POI database supports : ",result)
        assert result == 1'''

    print("Done initializing the database ...")
    yield conn.cursor(),conn

    # conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    # conn.cursor().execute("DROP DATABASE reomnify_sarthak WITH (FORCE);")

def test_data_source(setup_database):

    from reomnify_poi_ingestion.common_utils.db_helper import get_query_data
    from reomnify_poi_ingestion.common_utils.ds_helper import get_data_source
    from reomnify_poi_ingestion.poi_harvesting.helpers.helper import get_current_timestamp


    cursor, conn = setup_database

    # config = Pconf.get()
    # os.environ['host'] = config['host']
    # os.environ['dbname'] = 'reomnify_sarthak'
    # os.environ['user'] = config['user']
    # os.environ['password'] = config['password'] 
    
    data_source_id = 'soulorigin_au'
    

    with cursor as cursor: 

        configurations = get_data_source(data_source_id)
        assert len(configurations) > 0
        timestamp = get_current_timestamp(configurations['frequency'][0])

        exit_code = os.system("pipenv run python reomnify_poi_ingestion/poi_harvesting/ds_runner.py --force " + data_source_id + "")
        assert exit_code == 0
        
        destination_table = configurations['destination_table'][0]
        print(destination_table)

        '''query = "SELECT * FROM "+ destination_table +" WHERE timestamp='"+timestamp+"'"
        data_source_results = get_query_data(query, cursor)

        print(query)

        assert len(data_source_results) > 0

        query = """select * from poi_merged pm
                        inner join master_data_source_instances mdsi on mdsi.id = pm.ds_instance_id
                        where timestamp='{0}' and mdsi.ds_id='{1}'""".format(timestamp,data_source_id)

        poi_merged_results = get_query_data(query, cursor)

        assert len(data_source_results) == len(poi_merged_results)
        
        categories = poi_merged_results.query('top_category == "" ')
        assert len(categories) == 0

        print("Category assigned well")

        geo_coordinates = poi_merged_results.query('lat == 0 & long == 0 ')
        assert len(geo_coordinates) == 0

        print("Geo coordinates assigned well")

        geo_coordinates = poi_merged_results.query('postal_code == 0')
        assert len(geo_coordinates) == 0

        print("Postal code assigned well")'''