INSERT INTO master_brands(ro_brand_id,brand_name,map_text )
VALUES('RO_BRAND_460','Red Rooster',
	   ARRAY['red rooster','redrooster']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type, domain, frequency, destination_table,
	handler, active, is_auth_required)
	VALUES ('red_rooster_au', 'https://www.redrooster.com.au/', 'api/stores/2/',
			'Restaurants', 'Takeaways', 'monthly',
			'red_rooster_au', 'red_rooster_au', 'True', false) ON CONFLICT (id) DO NOTHING;

INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','red_rooster_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('red_rooster_au','RO_BRAND_460') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_460','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- For soul origin

INSERT INTO master_brands(ro_brand_id,brand_name,map_text )
VALUES('RO_BRAND_461','Soul Origin',
	   ARRAY['soul origin','soulorigin']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required)
	VALUES ('soul_origin_au', 'https://www.soulorigin.com.au', '/wp-admin/admin-ajax.php',
			'Restaurants','','monthly',
			'soul_origin_au', 'soul_origin_au', 'True', false) ON CONFLICT (id) DO NOTHING;

INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('action=get_store_location&search=%2CAU&region_dr=all','soul_origin_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('soul_origin_au','RO_BRAND_461') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_461','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- For celcom
INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_462','Celcom',
		ARRAY['celcom','celcom']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('celcom_my', 'https://www.celcom.com.my', '/api/post_content_details',
			'Restaurants','','monthly',
			'celcom_my', 'celcom_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('?_format=hal_json&type=custom&name=store_locator_result','celcom_my', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('celcom_my','RO_BRAND_462') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_462','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- For carolina herrera
INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_463','Carolina Herrera',
		ARRAY['carolina herrera','carolinaherrera']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required)
	VALUES ('carolina_herrera_my', 'https://carolinaherreraboutiques.com', '',
			'Restaurants','','monthly',
			'carolina_herrera_my', 'carolina_herrera_my', 'True', false) ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('/en/api/stores?lat=4.210484&lng=101.975766&page={}','carolina_herrera_my', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('carolina_herrera_my','RO_BRAND_463') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_463','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- For chilis

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_464','Chilis',
		ARRAY['chilis','chilis']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('chilis_my', 'https://chilis.com.my', '/outlets',
			'brands','','monthly',
			'chilis_my', 'chilis_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','chilis_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('chilis_my','RO_BRAND_464') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_464','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- For mydottys

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_465','My Dottys',
		ARRAY['my dottys','mydottys']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('my_dottys_my', 'http://www.mydottys.com/', '',
			'brands','','monthly',
			'my_dottys_my', 'my_dottys_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','my_dottys_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('my_dottys_my','RO_BRAND_465') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_465','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- For Flash Gadgets

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_466','Flash Gadgets',
		ARRAY['flash gadgets','flash_gadgets']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('flash_gadgets_my', 'https://www.flashgadgets.com.my', '/where_to_find_us',
			'brands','','monthly',
			'flash_gadgets_my', 'flash_gadgets_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','flash_gadgets_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('flash_gadgets_my','RO_BRAND_466') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_466','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- For Focus Point

/*INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_467','Focus POint',
		ARRAY['focus point','focus_point']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('focus_point_my', 'https://www.focus-point.com', '/store-locator',
			'brands','','monthly',
			'focus_point_my', 'focus_point_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','focus_point_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('focus_point_my','RO_BRAND_467') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_467','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;*/


-- For Fred Perry

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_468','Focus Point',
		ARRAY['fred perry','fred_perry']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('fred_perry_my', 'https://www.fredperry.com/', '/shop-finder/location/malaysia',
			'brands','','monthly',
			'fred_perry_my', 'fred_perry_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','fred_perry_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('fred_perry_my','RO_BRAND_468') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_468','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- For Fuel Shack

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_469','Fuel Shack',
		ARRAY['fuel shack','fuel_shack']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('fuel_shack_my', 'https://fuelshack.com.my/', '',
			'brands','','monthly',
			'fuel_shack_my', 'fuel_shack_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','fuel_shack_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('fuel_shack_my','RO_BRAND_469') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_469','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- For Genki Sushi

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_470','Genki Sushi',
		ARRAY['genki sushi','genki_sushi']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('genki_sushi_my', 'https://www.genkisushi.com.my', '/locate-us/',
			'brands','','monthly',
			'genki_sushi_my', 'genki_sushi_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','genki_sushi_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('genki_sushi_my','RO_BRAND_470') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_470','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- For Giordano

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_471','Giordano',
		ARRAY['giordano','giordano']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('giordano_my', ' https://giordanoappsite.giordano.com', '/AppSiteMY/SVC/AppsFunc.svc/rest/GetShopsMap?',
			'brands','','monthly',
			'giordano_my', 'giordano_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('shopID=&style=&market=MY&langid=EN&longitude=0&latitude=0&callback=json','giordano_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('giordano_my','RO_BRAND_471') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_471','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- For Glo Laser centers

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_472','Glo Laser Centers',
		ARRAY['glo laser centers','glo_laser_centers']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('glo_laser_centers_my', ' https://www.glolasercentres.com/', '/locate-us/',
			'brands','','monthly',
			'glo_laser_centers_my', 'glo_laser_centers_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','glo_laser_centers_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('glo_laser_centers_my','RO_BRAND_472') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_472','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- For Valiram

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_473','Godiva',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_godiva_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Godiva,Malaysia','valiram_godiva_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_godiva_my','RO_BRAND_473') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_473','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Alpina Valiram

-- Audemars Piguet Valiram


-- Bally Valiram



-- Bath &amp; Body Works Valiram

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_477','Bath &amp; Body Works',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_bath_body_works_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Bath &amp; Body Works,Malaysia','valiram_bath_body_works_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_bath_body_works_my','RO_BRAND_477') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_477','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Blancpain Valiram


-- breguet valiram


-- Breitling valiram


-- Bremont valiram

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_482','Bremont',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_bremont_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Bremont,Malaysia','valiram_bremont_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_bremont_my','RO_BRAND_482') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_482','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Bremont valiram


-- Bvlgari valiram Only one store

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_483','Bvlgari',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_bvlgari_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Bvlgari,Malaysia','valiram_bvlgari_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_bvlgari_my','RO_BRAND_483') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_483','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Canali Valiram

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_484','Canali',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_canali_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Canali,Malaysia','valiram_canali_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_canali_my','RO_BRAND_484') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_484','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Cartier Valiram


-- Charles &amp; Keith Valiram

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_486','Charles &amp; Keith',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_charles_keith_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Charles &amp; Keith,Malaysia','valiram_charles_keith_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_charles_keith_my','RO_BRAND_486') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_486','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Chloe Valiram Only one store

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_487','Chloe',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_chloe_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Chloe,Malaysia','valiram_chloe_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_chloe_my','RO_BRAND_487') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_487','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Chopard Valiram


-- Clinique Valiram

-- Coach Valiram

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_490','Coach',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_coach_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Coach,Malaysia','valiram_coach_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_coach_my','RO_BRAND_490') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_490','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Dunhill Valiram

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_491','Dunhill',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_dunhill_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Dunhill,Malaysia','valiram_dunhill_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_dunhill_my','RO_BRAND_491') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_491','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Fashion Accessories Valiram

-- Flying Emporium

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_493','Flying Emporium',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_flying_emporium_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Flying Emporium,Malaysia','valiram_flying_emporium_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_flying_emporium_my','RO_BRAND_493') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_493','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Giuseppe Zanotti

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_511','Giuseppe Zanotti',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_giuseppe_zanotti_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Giuseppe Zanotti,Malaysia','valiram_giuseppe_zanotti_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_giuseppe_zanotti_my','RO_BRAND_511') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_511','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Hermes

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_512','Hermes',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_hermes_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Hermes,Malaysia','valiram_hermes_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_hermes_my','RO_BRAND_512') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_512','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Hugo Boss

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_513','Hugo Boss',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_hugo_boss_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Hugo Boss,Malaysia','valiram_hugo_boss_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_hugo_boss_my','RO_BRAND_513') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_513','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


--IWC Schaffhausen

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_494','IWC Schaffhausen',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_iwc_schaffhausen_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('IWC Schaffhausen,Malaysia','valiram_iwc_schaffhausen_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_iwc_schaffhausen_my','RO_BRAND_494') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_494','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- La Martina

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_514','La Martina',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_la_martina_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('La Martina,Malaysia','valiram_la_martina_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_la_martina_my','RO_BRAND_514') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_514','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Michael Kors
INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_495','Michael Kors',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_michael_kors_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Michael Kors,Malaysia','valiram_michael_kors_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_michael_kors_my','RO_BRAND_495') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_495','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Montblanc

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_496','Montblanc',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_montblanc_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Montblanc,Malaysia','valiram_montblanc_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_montblanc_my','RO_BRAND_496') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_496','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Nadodi

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_515','Nadodi',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_nadodi_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Nadodi,Malaysia','valiram_nadodi_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_nadodi_my','RO_BRAND_515') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_515','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;



-- Omega

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_497','Omega',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_omega_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Omega,Malaysia','valiram_omega_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_omega_my','RO_BRAND_497') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_497','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Pedro

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_498','Pedro',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_pedro_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Pedro,Malaysia','valiram_pedro_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_pedro_my','RO_BRAND_498') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_498','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

--Polo Ralph Lauren

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_516','Polo Ralph Lauren',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_polo_ralph_lauren_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Polo Ralph Lauren,Malaysia','valiram_polo_ralph_lauren_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_polo_ralph_lauren_my','RO_BRAND_516') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_516','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

--QUIVO

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_517','Quivo',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_Quivo_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Quivo,Malaysia','valiram_Quivo_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_Quivo_my','RO_BRAND_517') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_517','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Rolex

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_499','Rolex',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_rolex_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Rolex,Malaysia','valiram_rolex_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_rolex_my','RO_BRAND_499') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_499','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

--Salvatore Ferragamo

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_518','Salvatore Ferragamo',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_salvatore_ferragamo_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Salvatore Ferragamo,Malaysia','valiram_salvatore_ferragamo_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_salvatore_ferragamo_my','RO_BRAND_518') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_518','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Steve Madden

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_503','Steve Madden',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_steve_madden_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Steve Madden,Malaysia','valiram_steve_madden_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_steve_madden_my','RO_BRAND_503') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_503','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Swiss Watch Gallery

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_504','Swiss Watch Gallery',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_swiss_watch_gallery_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Swiss Watch Gallery,Malaysia','valiram_swiss_watch_gallery_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_swiss_watch_gallery_my','RO_BRAND_504') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_504','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

--Tissot

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_519','Tissot',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_tissot_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Tissot,Malaysia','valiram_tissot_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_tissot_my','RO_BRAND_519') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_519','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Tory Burch

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_505','Tory Burch',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_tory_burch_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Tory Burch,Malaysia','valiram_tory_burch_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_tory_burch_my','RO_BRAND_505') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_505','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

--Tudor

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_520','Tudor',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_tudor_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Tudor,Malaysia','valiram_tudor_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_tudor_my','RO_BRAND_520') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_520','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- TUMI

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_506','TUMI',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_tumi_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('TUMI,Malaysia','valiram_tumi_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_tumi_my','RO_BRAND_506') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_506','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

--TWG Tea Salon &amp; Boutique

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_507','TWG Tea Salon &amp; Boutique',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_twg_tea_salon_botique_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('TWG Tea Salon &amp; Boutique,Malaysia','valiram_twg_tea_salon_botique_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_twg_tea_salon_botique_my','RO_BRAND_507') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_507','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

--Victoria's Secret Beauty &amp; Accessories 
--Error

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_508','Victoria''s Secret Beauty &amp; Accessories',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_victorias_secret_beauty_accessories_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Victoria''s Secret Beauty &amp; Accessories,Malaysia','valiram_victorias_secret_beauty_accessories_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_victorias_secret_beauty_accessories_my','RO_BRAND_508') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_508','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Wear+When

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_509','Wear+When',
		ARRAY['valiram','valiram']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('valiram_wear_when_my', 'http://valiram.com', '/store-locator/',
			'brands','','monthly',
			'valiram_my', 'valiram_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('Wear+When,Malaysia','valiram_wear_when_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('valiram_wear_when_my','RO_BRAND_509') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_509','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Guess stores

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_500','Guess',
		ARRAY['guess','guess']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('guess_my', 'https://www.guess.my', '/pages/stores',
			'brands','','monthly',
			'guess_my', 'guess_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','guess_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('guess_my','RO_BRAND_500') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_500','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Habib Jewels stores

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_501','Habib Jewels',
		ARRAY['habib jewels','habib_jewels']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('habib_jewels_my', 'https://www.habibjewels.com', '/showrooms',
			'brands','','monthly',
			'habib_jewels_my', 'habib_jewels_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','habib_jewels_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('habib_jewels_my','RO_BRAND_501') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_501','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Health Lane Family Pharmacy

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_510','Health Lane Family Pharmacy',
		ARRAY['health lane family pharmacy','health_lane_family_pharmacy']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('health_lane_family_pharmacy_my', 'https://estore.healthlane.com.my', '/locate-us',
			'brands','','monthly',
			'health_lane_family_pharmacy_my', 'health_lane_family_pharmacy_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','health_lane_family_pharmacy_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('health_lane_family_pharmacy_my','RO_BRAND_510') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_510','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- HLA

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_521','HLA',
		ARRAY['hla','hla']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('hla_my', 'http://www.hla.com', '/website/js/store.js?v=1.0.0',
			'brands','','monthly',
			'hla_my', 'hla_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','hla_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('hla_my','RO_BRAND_521') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_521','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Hublot

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_522','Hublot',
		ARRAY['hublot','hublot']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('hublot_my', 'https://www.hublot.com', '/en-us/boutiques?center%5Bcoordinates%5D%5Blat%5D=3.139003&center%5Bcoordinates%5D%5Blng%5D=101.686855&dist=100&address=Kuala+Lumpur%2C+Federal+Territory+of+Kuala+Lumpur%2C+Malaysia',
			'brands','','monthly',
			'hublot_my', 'hublot_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','hublot_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('hublot_my','RO_BRAND_522') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_522','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Huawei

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_523','Huawei',
		ARRAY['huawei','huawei']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('huawei_my', 'https://consumer.huawei.com', '/my/retail/',
			'brands','','monthly',
			'huawei_my', 'huawei_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','huawei_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('huawei_my','RO_BRAND_523') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_523','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- I Love Yoo

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_524','I Love Yoo',
		ARRAY['i love yoo','i_love_yoo']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('i_love_yoo_my', 'https://www.iloveyoo.com.my', '/outlets',
			'brands','','monthly',
			'i_love_yoo_my', 'i_love_yoo_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','i_love_yoo_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('i_love_yoo_my','RO_BRAND_524') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_524','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Innisfree

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_525','Innisfree',
		ARRAY['innisfree','innisfree']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('innisfree_my', 'https://www.innisfree.com', '/my/en/store/getStore.do',
			'brands','','monthly',
			'innisfree_my', 'innisfree_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','innisfree_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('innisfree_my','RO_BRAND_525') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_525','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Inside Scoop

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_526','Inside Scoop',
		ARRAY['inside scoop','inside_scoop']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('insidescoop_my', 'https://insidescoop.com.my', '/pages/our-locations',
			'brands','','monthly',
			'insidescoop_my', 'insidescoop_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','insidescoop_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('insidescoop_my','RO_BRAND_526') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_526','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Hai Lau shan

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_527','Hai Lau Shan',
		ARRAY['hai lau shan','hai_lau_shan']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('hai_lau_shan_my', 'https://www.hkhls.com/', 'en/mapdata;en/mapsearch',
			'brands','','monthly',
			'hai_lau_shan_my', 'hai_lau_shan_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('enquiry_company_type={}&region={}','hai_lau_shan_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('hai_lau_shan_my','RO_BRAND_527') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_527','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Isetan

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_528','Isetan',
		ARRAY['Isetan','isetan']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('isetan_my', 'https://www.isetankl.com.my/', '',
			'brands','','monthly',
			'isetan_my', 'isetan_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','isetan_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('isetan_my','RO_BRAND_528') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_528','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Chilli Pan Mee

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_529','Chilli Pan Mee',
		ARRAY['Chilli Pan Mee','chilli_pan_mee']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('chilli_pan_mee_my', 'https://www.chillipanmee.com', '/our-locations/',
			'brands','','monthly',
			'chilli_pan_mee_my', 'chilli_pan_mee_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','chilli_pan_mee_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('chilli_pan_mee_my','RO_BRAND_529') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_529','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Juice Works

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_530','Juice Works',
		ARRAY['Juice Works','juice_works']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('juice_works_my', 'https://www.juiceworks.com.my', '/location/',
			'brands','','monthly',
			'juice_works_my', 'juice_works_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('#page-{}','juice_works_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('juice_works_my','RO_BRAND_530') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_530','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Ken Chan Curry

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_531','Ken Chan Curry',
		ARRAY['Ken Chan Curry','ken_chan_curry']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('ken_chan_curry_my', 'https://www.kenchancurry.com.my/', '',
			'brands','','monthly',
			'ken_chan_curry_my', 'ken_chan_curry_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','ken_chan_curry_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('ken_chan_curry_my','RO_BRAND_531') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_531','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Kenzo

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_532','Kenzo',
		ARRAY['Kenzo','kenzo']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('kenzo_my', 'https://www.kenzo.com', '/eu/en/allstoresjson',
			'brands','','monthly',
			'kenzo_my', 'kenzo_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','kenzo_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('kenzo_my','RO_BRAND_532') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_532','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Kiehls

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_533','Kiehls',
		ARRAY['Kiehls','kiehls']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('kiehls_my', 'https://www.kiehls.com.my', '/on/demandware.store/Sites-kiehls-my-Site/en_MY/Stores-Search?bounds=5.499600221274584_104.408015984375%7C3.6379420309151898_100.123348015625&center=4.5693754_102.265682&radius=1000&brands=',
			'brands','','monthly',
			'kiehls_my', 'kiehls_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','kiehls_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('kiehls_my','RO_BRAND_533') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_533','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Koi The

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_534','Koi The',
		ARRAY['Koi The','koi_the']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('koi_the_my', 'https://www.koithe.com', '/en/global/koi-malaysia',
			'brands','','monthly',
			'koi_the_my', 'koi_the_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','koi_the_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('koi_the_my','RO_BRAND_534') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_534','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Lacoste

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_535','Lacoste',
		ARRAY['Lacoste','lacoste']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('lacoste_my', 'https://global.lacoste.com', '/en/stores?country=malaysia&json=true',
			'brands','','monthly',
			'lacoste_my', 'lacoste_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('/en/stores?country=malaysia&city={}&json=true','lacoste_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('lacoste_my','RO_BRAND_535') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_535','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Lacucur

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_536','Lacucur',
		ARRAY['Lacucur','lacucur']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('lacucur_my', 'https://lacucur.com', '/outlets/',
			'brands','','monthly',
			'lacucur_my', 'lacucur_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','lacucur_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('lacucur_my','RO_BRAND_536') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_536','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Lancome

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_537','Lancome',
		ARRAY['Lancome','lancome']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('lancome_my', 'https://www.lancome.com.my', '/assets/js/store-locator/locations.xml?v2',
			'brands','','monthly',
			'lancome_my', 'lancome_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','lancome_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('lancome_my','RO_BRAND_537') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_537','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Lamer

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_538','Lamer',
		ARRAY['Lamer','lamer']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('lamer_my', 'https://www.lamer.com.my', '/rpc/jsonrpc.tmpl?dbgmethod=locator.doorsandevents',
			'brands','','monthly',
			'lamer_my', 'lamer_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','lamer_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('lamer_my','RO_BRAND_538') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_538','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Lego

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_539','Lego',
		ARRAY['Lego','lego']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('lego_my', 'https://www.lego.com', '/api/graphql/StoresDirectory',
			'brands','','monthly',
			'lego_my', 'lego_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','lego_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('lego_my','RO_BRAND_539') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_539','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Levis

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_540','Levis',
		ARRAY['Levis','levis']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('levis_my', 'https://api.storepoint.co', '/v1/15e3788b38c9bd/locations?rq',
			'brands','','monthly',
			'levis_my', 'levis_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','levis_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('levis_my','RO_BRAND_540') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_540','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Loccitane

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_541','Loccitane',
		ARRAY['Loccitane','loccitane']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('loccitane_my', 'https://my.loccitane.com', '/tools/datafeeds/StoresJSON.aspx?country=malaysia&category=null&city=&lat=&lon=&L=1&task=storelocatorV2',
			'brands','','monthly',
			'loccitane_my', 'loccitane_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','loccitane_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('loccitane_my','RO_BRAND_541') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_541','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Laneige

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_542','Laneige',
		ARRAY['Laneige','laneige']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('laneige_my', 'https://prd-bwapi.amorepacific.com', '/api/v1/shop/list',
			'brands','','monthly',
			'laneige_my', 'laneige_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','laneige_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('laneige_my','RO_BRAND_542') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_542','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Long Champ

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_543','Long Champ',
		ARRAY['Long Champ','long_champ']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('longchamp_my', 'https://www.longchamp.com', '/on/demandware.store/Sites-Longchamp-ASIA-Site/en_MY/Stores-FindStores?showMap=false&findInStore=false&productId=false&checkboxStore=stock&radius=300&lat=4.111959099769592&lng=109.45492935180664&entityType=Place&entitySubType=CountryRegion&countryRegion=MY&formattedSuggestion=Malaysia&timeZoneMoment=Asia%2FCalcutta',
			'brands','','monthly',
			'longchamp_my', 'longchamp_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','longchamp_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('longchamp_my','RO_BRAND_543') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_543','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;



-- Le Labo

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_544','Le Labo',
		ARRAY['Le Labo','le_labo']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('le_labo_my', 'https://www.lelabofragrances.com', '/front/app/store/search?execution=e1s1',
			'brands','','monthly',
			'le_labo_my', 'le_labo_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','le_labo_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('le_labo_my','RO_BRAND_544') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_544','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Louis Vuitton

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_545','Louis Vuitton',
		ARRAY['Louis Vuitton','louis_vuitton']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('louis_vuitton_my', 'https://eu.louisvuitton.com/', '/ajax/getStoreJson.jsp?storeLang=eng-e1&cache=NoStore&pageType=storelocator_section&flagShip=false&latitudeCenter=4.1492005815339805&longitudeCenter=109.6181485&latitudeA=10.68220049016408&longitudeA=94.69871490625&latitudeB=-2.438228708697903&longitudeB=124.53758209374999&doClosestSearch=true&zoomLevel=5&country=&categories=&clickAndCollect=false&productId=&countryId=&osa=null&Ntt=Malaysia',
			'brands','','monthly',
			'louis_vuitton_my', 'louis_vuitton_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','louis_vuitton_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('louis_vuitton_my','RO_BRAND_545') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_545','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Lovisa

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_546','Lovisa',
		ARRAY['Lovisa','lovisa']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('lovisa_my', 'https://lovisa-stores.herokuapp.com', '/all-stores',
			'brands','','monthly',
			'lovisa_my', 'lovisa_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','lovisa_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('lovisa_my','RO_BRAND_546') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_546','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Mahnaz Food

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_547','Mahnaz Food',
		ARRAY['Mahnaz Food','mahnaz_food']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('mahnaz_food_my', 'http://www.mercucita.com', '/branches/',
			'brands','','monthly',
			'mahnaz_food_my', 'mahnaz_food_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','mahnaz_food_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('mahnaz_food_my','RO_BRAND_547') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_547','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Luk Fook Jewellery

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_548','Luk Fook Jewellery',
		ARRAY['Luk Fook Jewellery','luk_fook_jewellery']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('luk_fook_jewellery_my', 'http://www1.lukfook.com.hk', '/LF-AMap/home/GetShopAbroad?region=Malaysia&keyword=',
			'brands','','monthly',
			'luk_fook_jewellery_my', 'luk_fook_jewellery_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','luk_fook_jewellery_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('luk_fook_jewellery_my','RO_BRAND_548') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_548','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Maje

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_549','Maje',
		ARRAY['Maje','maje']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('maje_my', 'http://www.maje.com.sg', '',
			'brands','','monthly',
			'maje_my', 'maje_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','maje_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('maje_my','RO_BRAND_549') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_549','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Marks and Spencers

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_550','Marks and Spencers',
		ARRAY['Marks and Spencers','marks_and_spencers']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('marks_and_spencers_my', 'https://www.marksandspencer.com', '/my/stores',
			'brands','','monthly',
			'marks_and_spencers_my', 'marks_and_spencers_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','marks_and_spencers_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('marks_and_spencers_my','RO_BRAND_550') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_550','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Massimo Dutti

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_551','Massimo Dutti',
		ARRAY['Massimo Dutti','massimo_dutti']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('massimo_dutti_my', 'https://www.massimodutti.com', '/itxrest/2/bam/store/35009507/physical-store?appId=1&languageId=-1&latitude=4.210484&longitude=101.975766&radioMax=200',
			'brands','','monthly',
			'massimo_dutti_my', 'massimo_dutti_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','massimo_dutti_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('massimo_dutti_my','RO_BRAND_551') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_551','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Pandora

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_552','Pandora',
		ARRAY['Pandora','pandora']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('pandora_my', 'https://www.pandora.net/', '/services/consumer/storelocator/getallstores.ashx?country=my',
			'brands','','monthly',
			'pandora_my', 'pandora_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','pandora_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('pandora_my','RO_BRAND_552') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_552','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Paris Miki

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_553','Paris Miki',
		ARRAY['Paris Miki','paris_miki']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('paris_miki_my', 'https://paris-miki.com.my', '/shop-list/',
			'brands','','monthly',
			'paris_miki_my', 'paris_miki_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','paris_miki_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('paris_miki_my','RO_BRAND_553') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_553','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Patek

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_554','Patek',
		ARRAY['Patek','patek']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('patek_my', 'https://www.patek.com', '/en/retail-service/authorized-retailers/asia/malaysia',
			'brands','','monthly',
			'patek_my', 'patek_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','patek_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('patek_my','RO_BRAND_554') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_554','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Versace

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_555','Versace',
		ARRAY['Versace','versace']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('versace_my', 'https://boutiques.versace.com', '/en/search?country=MY&qp=Malaysia&l=en',
			'brands','','monthly',
			'versace_my', 'versace_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','versace_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('versace_my','RO_BRAND_555') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_555','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Prada

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_556','Prada',
		ARRAY['Prada','prada']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('prada_my', 'https://www.prada.com', '/ww/en/store-locator.html?location=Malaysia&lat=4.210484&lng=101.975766',
			'brands','','monthly',
			'prada_my', 'prada_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','prada_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('prada_my','RO_BRAND_556') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_556','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Vincci

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_557','Vincci',
		ARRAY['Vincci','vincci']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('vincci_my', 'https://corporate.padini.com', '/wp-admin/admin-ajax.php?action=asl_load_stores&nonce=1d71ae1988&load_all=1&layout=1',
			'brands','','monthly',
			'vincci_my', 'vincci_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('30,Malaysia','vincci_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('vincci_my','RO_BRAND_557') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_557','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Padini Concept store

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_558','Padini Concept Store',
		ARRAY['Padini Concept Store','padini_concept_store']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('vincci_padini_my', 'https://corporate.padini.com', '/wp-admin/admin-ajax.php?action=asl_load_stores&nonce=1d71ae1988&load_all=1&layout=1',
			'brands','','monthly',
			'vincci_my', 'vincci_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('29,Malaysia','vincci_padini_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('vincci_padini_my','RO_BRAND_558') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_558','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Brands outlet

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_559','Brands Outlet',
		ARRAY['Brands Outlet','brands_outlet']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('vincci_brands_outlet_my', 'https://corporate.padini.com', '/wp-admin/admin-ajax.php?action=asl_load_stores&nonce=1d71ae1988&load_all=1&layout=1',
			'brands','','monthly',
			'vincci_my', 'vincci_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('28,Malaysia','vincci_brands_outlet_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('vincci_brands_outlet_my','RO_BRAND_559') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_559','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Whoosh

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_560','Whoosh',
		ARRAY['Whoosh','whoosh']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('whoosh_my', 'https://whoosheyewear.com', '/locate-us/',
			'brands','','monthly',
			'whoosh_my', 'whoosh_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','whoosh_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('whoosh_my','RO_BRAND_560') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_560','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- YSL Beauty

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_561','YSL Beauty',
		ARRAY['YSL Beauty','ysl_beauty']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('ysl_my', 'https://www.yslbeauty.com.my', '/assets/js/store-locator/locations.xml?v10',
			'brands','','monthly',
			'ysl_my', 'ysl_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','ysl_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('ysl_my','RO_BRAND_561') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_561','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Yuzu

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_562','Yuzu',
		ARRAY['Yuzu','yuzu']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('yuzu_my', 'http://yuzu.com.my', '/gardens/location/',
			'brands','','monthly',
			'yuzu_my', 'yuzu_my', 'True', false,'MY') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','yuzu_my', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('yuzu_my','RO_BRAND_562') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_562','MY') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;



-- Australia Body Catalyst

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_563','Body Catalyst',
		ARRAY['Body Catalyst','body_catalyst']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('body_catalyst_au', 'https://www.bodycatalyst.com.au', '/locations/',
			'brands','','monthly',
			'body_catalyst_au', 'body_catalyst_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','body_catalyst_au', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('body_catalyst_au','RO_BRAND_563') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_563','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Chempro

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_564','Chempro',
		ARRAY['Chempro','chempro']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('chempro_au', 'https://www.chempro.com.au', '/index.php?route=extension/module/wk_store_locater/setter',
			'brands','','monthly',
			'chempro_au', 'chempro_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('longitude=0&latitude=0&pr_id=all&location=Brisbane+QLD%2C+Australia','chempro_au', 20608) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('chempro_au','RO_BRAND_564') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_564','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Clearly Dental

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_565','Clearly Dental',
		ARRAY['Clearly Dental','clearly_dental']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('clearly_dental_au', 'https://www.clearlydental.com.au', '/locations',
			'brands','','monthly',
			'clearly_dental_au', 'clearly_dental_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','clearly_dental_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('clearly_dental_au','RO_BRAND_565') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_565','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Foot master

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_566','Foot Master',
		ARRAY['Foot Master','footmaster']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('footmaster_au', 'https://footmaster.com.au', '/pages/contact-us',
			'brands','','monthly',
			'footmaster_au', 'footmaster_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','footmaster_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('footmaster_au','RO_BRAND_566') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_566','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Pillow Talk

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_567','Pillow Talk',
		ARRAY['Pillow Talk','pillow_talk']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('pillow_talk_au', 'https://app.localisr.io', '/api/v1/collection?lat=-33.863276&lng=151.107977&radius=400&type=&view=all&map_permission=false&map_trigger=initial',
			'brands','','monthly',
			'pillow_talk_au', 'pillow_talk_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','pillow_talk_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('pillow_talk_au','RO_BRAND_567') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_567','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Zap Fitness

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_568','Zap Fitness',
		ARRAY['Zap Fitness','zap_fitness']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('zap_fitness_au', 'https://api.zapfitness.com.au', '/clubs/v1/allclubs',
			'brands','','monthly',
			'zap_fitness_au', 'zap_fitness_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','zap_fitness_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('zap_fitness_au','RO_BRAND_568') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_568','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Absoulte Make Over

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_569','Absoulte Make Over',
		ARRAY['Absoulte Make Over','absolute_makeover']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('absolute_makeover_au', 'https://www.absolutemakeover.com.au', '/api/settings/',
			'brands','','monthly',
			'absolute_makeover_au', 'absolute_makeover_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','absolute_makeover_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('absolute_makeover_au','RO_BRAND_569') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_569','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Acai Brother

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_570','Acai Brothers',
		ARRAY['Acai Brothers','acai_brothers']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('acai_brothers_au', 'https://acaibrothers.com', '/store-locator/',
			'brands','','monthly',
			'acai_brothers_au', 'acai_brothers_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','acai_brothers_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('acai_brothers_au','RO_BRAND_570') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_570','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Ace Cinemas

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_571','Ace Cinemas',
		ARRAY['Ace Cinemas','ace_cinemas']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('ace_cinemas_au', 'https://acecinemas.com.au', '/s/locations.html',
			'brands','','monthly',
			'ace_cinemas_au', 'ace_cinemas_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','ace_cinemas_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('ace_cinemas_au','RO_BRAND_571') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_571','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Acmedelavie

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_572','Acmedelavie',
		ARRAY['Acmedelavie','acmedelavie']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('acmedelavie_au', 'https://acmedelavie.com.au', '/store-location/',
			'brands','','monthly',
			'acmedelavie_au', 'acmedelavie_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','acmedelavie_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('acmedelavie_au','RO_BRAND_572') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_572','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Beacon Lighting

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_573','Beacon Lighting',
		ARRAY['Beacon Lighting','beacon_lighting']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('beacon_lighting_au', 'https://www.beaconlighting.com.au', '/beaconlocator/index/ajax/?_=1644561304713',
			'brands','','monthly',
			'beacon_lighting_au', 'beacon_lighting_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','beacon_lighting_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('beacon_lighting_au','RO_BRAND_573') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_573','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Beach House Bargrill

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_574','Beach House Bargrill',
		ARRAY['Beach House Bargrill','beach_house_bargrill']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('beach_house_bargrill_au', 'https://www.beachhousebargrill.com', '/locations/',
			'brands','','monthly',
			'beach_house_bargrill_au', 'beach_house_bargrill_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','beach_house_bargrill_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('beach_house_bargrill_au','RO_BRAND_574') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_574','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Bedshed

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_575','Bedshed',
		ARRAY['Bedshed','bedshed']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('bedshed_au', 'https://www.bedshed.com.au', '/find-a-store/index/search/?postcode=Australia',
			'brands','','monthly',
			'bedshed_au', 'bedshed_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','bedshed_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('bedshed_au','RO_BRAND_575') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_575','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Bendon Lingerie

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_576','Bendon Lingerie',
		ARRAY['Bendon Lingerie','bendon_lingerie']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('bendon_lingerie_au', 'https://www.bendonlingerie.com.au', '/store-locator',
			'brands','','monthly',
			'bendon_lingerie_au', 'bendon_lingerie_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','bendon_lingerie_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('bendon_lingerie_au','RO_BRAND_576') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_576','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Bill Bong

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_577','Bill Bong',
		ARRAY['Billbong','billbong']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('billbong_au', 'https://www.billabong.com.au', '/on/demandware.store/Sites-BB-AU-Site/en_AU/StoreLocator-StoreLookup?mapRadius=50000&filterBBStores=true&filterBBRetailers=true&latitude=-33.873199462890625&longitude=151.2095947265625',
			'brands','','monthly',
			'billbong_au', 'billbong_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','billbong_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('billbong_au','RO_BRAND_577') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_577','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Camera House

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_578','Camera House',
		ARRAY['Camera House','camera_house']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('camera_house_au', 'https://www.camerahouse.com.au', '/stores/index/dataAjax',
			'brands','','monthly',
			'camera_house_au', 'camera_house_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('?category={}','camera_house_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('camera_house_au','RO_BRAND_578') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_578','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Blue Star Eye Wear

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_579','Blue Star Eye Wear',
		ARRAY['Blue Star Eye Wear','blue_star_eye_wear']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('blue_star_eye_wear_au', 'https://www.bluestareyewear.com.au', '/store-finder/?pgnum=1',
			'brands','','monthly',
			'blue_star_eye_wear_au', 'blue_star_eye_wear_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','blue_star_eye_wear_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('blue_star_eye_wear_au','RO_BRAND_579') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_579','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Daiso Japan

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_580','My Daiso Japan',
		ARRAY['My Daiso Japan','my_daiso_japan']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('my_daiso_japan_au', 'https://mydaiso.com.au', '/wp-admin/admin-ajax.php?action=store_search&lat=-33.86882&lng=151.2093&max_results=25&search_radius=50&autoload=1',
			'brands','','monthly',
			'my_daiso_japan_au', 'my_daiso_japan_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','my_daiso_japan_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('my_daiso_japan_au','RO_BRAND_580') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_580','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia David Jones

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_581','David Jones',
		ARRAY['David Jones','david_jones']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('david_jones_au', 'https://www.davidjones.com', '/default.aspx?stores=availability&latitude=-25.274398&longitude=133.775136&serveas=ajax&stockonly=0',
			'brands','','monthly',
			'david_jones_au', 'david_jones_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','david_jones_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('david_jones_au','RO_BRAND_581') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_581','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Deco Rug

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_582','Deco Rug',
		ARRAY['Deco Rug','decorug']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('decorug_au', 'https://decorug.com.au', '/pages/stores',
			'brands','','monthly',
			'decorug_au', 'decorug_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','decorug_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('decorug_au','RO_BRAND_582') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_582','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Degani

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_583','Degani',
		ARRAY['Degani','degani']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('degani_au', 'https://degani.com.au', '/wp-admin/admin-ajax.php',
			'brands','','monthly',
			'degani_au', 'degani_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','degani_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('degani_au','RO_BRAND_583') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_583','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;

-- Australia Eb Games

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_584','Eb Games',
		ARRAY['Eb Games','ebgames']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('ebgames_au', 'https://www.ebgames.com.au', '/stores',
			'brands','','monthly',
			'ebgames_au', 'ebgames_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','ebgames_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('ebgames_au','RO_BRAND_584') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_584','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia ECCO

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_585','Ecco',
		ARRAY['Ecco','ecco']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('ecco_au', 'https://au.ecco.com', '/store-locator',
			'brands','','monthly',
			'ecco_au', 'ecco_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','ecco_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('ecco_au','RO_BRAND_585') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_585','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Gami Chicken

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_586','Gami Chicken',
		ARRAY['Gami Chicken','gami_chicken']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('gami_chicken_au', 'https://gamichicken.com.au', '/locations',
			'brands','','monthly',
			'gami_chicken_au', 'gami_chicken_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('/api/locations/','gami_chicken_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('gami_chicken_au','RO_BRAND_586') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_586','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Politix Stores

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_587','Politix',
		ARRAY['Politix','politix']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('politix_au', 'https://www.politix.com.au/', 'on/demandware.store/Sites-politix_au-Site/en_AU/PolitixStores-GetAllStores?countryCode=AU',
			'brands','','monthly',
			'politix_au', 'politix_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','politix_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('politix_au','RO_BRAND_587') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_587','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Pet Stock

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_588','Pet Stock',
		ARRAY['Pet Stock','petstock']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('petstock_au', 'https://connector.petstock.io', '/api/location/',
			'brands','','monthly',
			'petstock_au', 'petstock_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','petstock_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('petstock_au','RO_BRAND_588') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_588','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;


-- Australia Pharmacy 4 Less

INSERT INTO master_brands(ro_brand_id,brand_name,map_text)
VALUES('RO_BRAND_589','Pharmacy 4 Less',
		ARRAY['Pharmacy 4 Less','pharmacy_4_less']) ON CONFLICT (ro_brand_id) DO NOTHING;

INSERT INTO master_data_sources(
	id, origin, path, poi_type,domain, frequency, destination_table,
	handler, active, is_auth_required,iso_country_code)
	VALUES ('pharmacy_4_less_au', 'https://www.pharmacy4less.com.au', '/slocator/json/search/',
			'brands','','monthly',
			'pharmacy_4_less_au', 'pharmacy_4_less_au', 'True', false,'AU') ON CONFLICT (id) DO NOTHING;
	
INSERT INTO master_data_source_instances(
	params, ds_id, city_id)
	VALUES ('','pharmacy_4_less_au', 5) ON CONFLICT (params, ds_id, city_id) DO NOTHING;

insert into master_data_source_brand_mapping (ds_id, ro_brand_id) values('pharmacy_4_less_au','RO_BRAND_589') ON CONFLICT (ds_id,ro_brand_id) DO NOTHING;

insert into master_brand_country_mapping(ro_brand_id,iso_country_code) 
values('RO_BRAND_589','AU') ON CONFLICT (ro_brand_id,iso_country_code) DO NOTHING;