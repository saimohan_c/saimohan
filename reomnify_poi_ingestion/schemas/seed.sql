INSERT INTO master_countries(country,iso_country_code)
	VALUES ('Australia', 'AU') ON CONFLICT (country,iso_country_code) DO NOTHING;

INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('Victoria', 'VIC', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('New South Wales', 'NSW', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('Queensland', 'QLD', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('Western Australia', 'WA', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('South Australia', 'SA', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('Australian Capital Territory', 'ACT', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('Tasmania', 'TAS', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('Northern Territory', 'NT', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('ALL', 'ALL', 'AU')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;
INSERT INTO master_regions(region, region_short_code, iso_country_code) VALUES ('N/A', 'N/A', 'MY')ON CONFLICT (region, region_short_code, iso_country_code) DO NOTHING;


INSERT INTO master_cities(city, region, lat, "long",id, iso_country_code, postal_code)
VALUES ('Melbourne', 'Victoria', -37.840935, 144.946457, 2, 'AU', '3000') ON CONFLICT (city,postal_code,region,iso_country_code) DO NOTHING;

INSERT INTO master_cities(city, region, lat, "long",id, iso_country_code, postal_code)
VALUES ('Sydney', 'New South Wales', -33.847927, 150.65178, 3, 'AU', '2000') ON CONFLICT (city,postal_code,region,iso_country_code) DO NOTHING;

INSERT INTO master_cities(city, region, lat, "long",id, iso_country_code, postal_code)
VALUES ('Brisbane', 'Queensland', 27.3818631, 152.7130055, 4, 'AU', '4000') ON CONFLICT (city,postal_code,region,iso_country_code) DO NOTHING;

INSERT INTO master_cities(city, region, lat, "long",id, iso_country_code, postal_code)
VALUES ('Perth', 'Western Australia',-31.9523123, 115.861309, 6, 'AU', '6000') ON CONFLICT (city,postal_code,region,iso_country_code) DO NOTHING;

INSERT INTO master_cities(city, region, lat, "long",id, iso_country_code, postal_code)
VALUES ('Adelaide', 'South Australia', -34.9284989, 138.6007456, 7, 'AU', '5000') ON CONFLICT (city,postal_code,region,iso_country_code) DO NOTHING;

INSERT INTO master_cities (city, region, lat, long, id, iso_country_code) 
VALUES ('ALL', 'ALL', -25.2744, 133.7751, 5, 'AU') ON CONFLICT (city,postal_code,region,iso_country_code) DO NOTHING;

INSERT INTO master_cities (city, region, lat, long, id, iso_country_code) 
VALUES ('N/A', 'N/A', 0, 0, 20608, 'MY') ON CONFLICT (city,postal_code,region,iso_country_code) DO NOTHING;



INSERT INTO master_countries(country,iso_country_code)
	VALUES ('Malaysia', 'MY') ON CONFLICT (country,iso_country_code) DO NOTHING;