CREATE TABLE IF NOT EXISTS red_rooster_au(
	id serial PRIMARY KEY,
	store_id integer DEFAULT 0,
	store_no text DEFAULT '',
	title text DEFAULT '',
	slug text DEFAULT '',
	catering boolean DEFAULT false,
	delivery boolean DEFAULT false,
	catering_pickup boolean DEFAULT false,
	catering_delivery boolean DEFAULT false,
	drive_thru boolean DEFAULT false,
	delivery_aggregators text DEFAULT '',
	catering_towncity text default '',
	catering_post_code text default '',
	catering_url text default '',
	gsn integer DEFAULT 0,
    address text DEFAULT '',	
    city text DEFAULT '',
    post_code integer,
    phone text DEFAULT '',
    lat numeric DEFAULT 0,
    "long" numeric DEFAULT 0,
    state text DEFAULT '',	
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS soul_origin_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
    "long" numeric DEFAULT 0,
	address text DEFAULT '',	
    city text DEFAULT '',
	state text DEFAULT '',
    post_code integer,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS celcom_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code integer,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS carolina_herrera_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
	country text DEFAULT '',
    post_code text ,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS chilis_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS my_dottys_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS flash_gadgets_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS fred_perry_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS fuel_shack_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS genki_sushi_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS giordano_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS glo_laser_centers_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS valiram_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	brand text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS guess_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS habib_jewels_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS health_lane_family_pharmacy_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS hla_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS hublot_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS huawei_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS i_love_yoo_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS innisfree_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS insidescoop_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS hai_lau_shan_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS isetan_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS chilli_pan_mee_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS juice_works_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS ken_chan_curry_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS kenzo_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS kiehls_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS koi_the_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS lacoste_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS lacucur_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS lancome_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS lamer_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS lego_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS levis_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS loccitane_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	store_code text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	category text DEFAULT '',
	channel text DEFAULT '',
	store_type text DEFAULT '',
	PickInStore text DEFAULT '',
	ShipToStore text DEFAULT '',
	isRetailer text DEFAULT '',
	isPushedOnline text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS laneige_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS longchamp_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS le_labo_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS louis_vuitton_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	product_categories text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS lovisa_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	branch_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	has_piercing text DEFAULT '',
	has_cc text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS mahnaz_food_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS luk_fook_jewellery_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS maje_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS marks_and_spencers_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	product_categories text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS massimo_dutti_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS pandora_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	type text DEFAULT '',
	store_type text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS paris_miki_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS patek_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS versace_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	store_type text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS prada_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS vincci_my(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS whoosh_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS ysl_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS yuzu_my(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS body_catalyst_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS chempro_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS clearly_dental_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS footmaster_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS pillow_talk_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS zap_fitness_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	slug text DEFAULT '',
	facilities_provided text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS absolute_makeover_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS acai_brothers_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS ace_cinemas_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS beacon_lighting_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	fax text DEFAULT '',
	services text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS beach_house_bargrill_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS bedshed_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	fax text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS bendon_lingerie_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	fax text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS billbong_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS camera_house_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS blue_star_eye_wear_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS my_daiso_japan_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	fax text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS david_jones_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS decorug_au(
	id serial PRIMARY KEY,
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS degani_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS ebgames_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS ecco_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	product_categories text DEFAULT '',
	isECCOStore text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS gami_chicken_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	slug text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS politix_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	fax text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	store_type text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS petstock_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	services text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);


CREATE TABLE IF NOT EXISTS pharmacy_4_less_au(
	id serial PRIMARY KEY,
	store_id text DEFAULT '',
	name text DEFAULT '',
	address text DEFAULT '',
	city text DEFAULT '',
	state text DEFAULT '',
    post_code text,
	phone text DEFAULT '',
	email text DEFAULT '',
	lat numeric DEFAULT 0,
	"long" numeric DEFAULT 0,
    open_hours text DEFAULT '',
	isactive text DEFAULT '',
	covid_vaccine_store text DEFAULT '',
	ro_place_id text default '',
	ds_instance_id integer,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);