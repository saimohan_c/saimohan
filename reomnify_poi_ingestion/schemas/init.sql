CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';

SET default_tablespace = '';

SET default_with_oids = false;

-- ######################## ALL MASTER TABLES GO HERE AND MAKE SURE ORDER IS MAINTAINED #########################################3

CREATE TABLE IF NOT EXISTS master_countries(
	id serial primary key, 
	country text not null unique,
	iso_country_code text not null unique,
	geo_polygon text default '',
	created_at timestamp without time zone DEFAULT now(),
	UNIQUE (country,iso_country_code)
);

CREATE TABLE IF NOT EXISTS master_regions(
	id serial primary key,
	region text not null unique,
	region_short_code text not null unique,
	iso_country_code text not null,
	geo_polygon text default '',
	created_at timestamp without time zone DEFAULT now(),
	UNIQUE (region,region_short_code, iso_country_code),
	CONSTRAINT iso_country_code FOREIGN KEY (iso_country_code) REFERENCES master_countries(iso_country_code)
);

CREATE TABLE IF NOT EXISTS master_cities (
    id serial primary key NOT NULL,
	city text,
    region text,
    lat numeric,
    long numeric,    
    iso_country_code text not null,
	postal_code text default '',
	geo_polygon text default '',
    created_at timestamp without time zone DEFAULT now(),
	UNIQUE (city,postal_code,region,iso_country_code),	
    CONSTRAINT iso_country_code FOREIGN KEY (iso_country_code) REFERENCES master_countries(iso_country_code),
	CONSTRAINT region FOREIGN KEY (region) REFERENCES master_regions(region)
);

CREATE TABLE IF NOT EXISTS master_suburbs(
	id serial PRIMARY KEY,
	suburb text default '',
	postal_code text default '',
	region_short_code text not null,
	iso_country_code text not null,
	geo_polygon text default '',
	created_at timestamp without time zone DEFAULT now(),
	CONSTRAINT iso_country_code FOREIGN KEY (iso_country_code) REFERENCES master_countries(iso_country_code),
	CONSTRAINT region_short_code FOREIGN KEY (region_short_code) REFERENCES master_regions(region_short_code)
);

CREATE TABLE IF NOT EXISTS master_data_sources (
    id text primary key NOT NULL,
    origin text,
    path text,
    poi_type text,
    domain text,
    frequency text,
    destination_table text,
    handler text,
    is_auth_required boolean,
    iso_country_code text DEFAULT '',
    created_at timestamp without time zone DEFAULT now(),
    active boolean DEFAULT true
);

CREATE TABLE IF NOT EXISTS master_data_source_credentials (
    id serial primary key,
    user_id text,
    password text,
    refresh_token text,
    status text,
	ds_id text default '',
    created_at timestamp without time zone DEFAULT now(),
	UNIQUE (refresh_token,ds_id)
);

CREATE TABLE IF NOT EXISTS master_data_source_instances(
    id serial PRIMARY KEY NOT NULL,
    params text,
    ds_id text,
    city_id integer,
	status boolean default true,
    credentials integer,
    created_at timestamp without time zone DEFAULT now(),		
	CONSTRAINT ds_id FOREIGN KEY (ds_id) REFERENCES master_data_sources(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT city_id FOREIGN KEY (city_id) REFERENCES master_cities(id) ,
	CONSTRAINT master_data_source_credentials FOREIGN KEY (credentials) REFERENCES master_data_source_credentials(id) ON DELETE CASCADE ON UPDATE CASCADE,
	UNIQUE (params,ds_id, city_id)
);

CREATE TABLE IF NOT EXISTS master_categories(

	id serial PRIMARY KEY,	
    top_category text,
	sub_category text,
	ro_category_key text UNIQUE,
    description text
);

CREATE TABLE IF NOT EXISTS master_data_source_confidence_matrix(
	ds_id text,
	ro_category_key text,
	conf_for_all NUMERIC (3, 2), 
	conf_brands NUMERIC (3, 2),
	conf_ro_brand_ids NUMERIC (3, 2),
	conf_parent_ro_place_id NUMERIC (3, 2),
	conf_ro_place_id NUMERIC (3, 2) DEFAULT 0.5,
	conf_location_name NUMERIC (3, 2),
    conf_top_category NUMERIC (3, 2),
    conf_sub_category NUMERIC (3, 2),
    conf_lattitude NUMERIC (3, 2),
    conf_longitude NUMERIC (3, 2),
    conf_suburb NUMERIC (3, 2),
    conf_cityname NUMERIC (3, 2),
    conf_region NUMERIC (3, 2),
    conf_postal_code NUMERIC (3, 2),
    conf_iso_country_code NUMERIC (3, 2),

    conf_phone_number NUMERIC (3, 2),
    conf_fax NUMERIC (3, 2),
    conf_email NUMERIC (3, 2),
    conf_weburl NUMERIC (3, 2),
    conf_open_hours NUMERIC (3, 2),
    conf_street_address NUMERIC (3, 2),
	
    PRIMARY KEY(ds_id, ro_category_key),
	CONSTRAINT ds_id FOREIGN KEY (ds_id) REFERENCES master_data_sources(id),
	CONSTRAINT ro_category_key FOREIGN KEY (ro_category_key) REFERENCES master_categories(ro_category_key)
);

CREATE TABLE IF NOT EXISTS master_brands (
	id serial unique,
    ro_brand_id text primary key NOT NULL,
    brand_name text,
    parent_ro_brand_id text,    
    top_category text,
    sub_category text,
    stock_symbol text,
    stock_exchange text,    
    map_text text[],
    created_at timestamp without time zone DEFAULT now(),
    iso_country_code text default ''
);

CREATE TABLE IF NOT EXISTS master_data_source_execution_time_logs(
	id serial PRIMARY KEY,	
	ds_id text DEFAULT '',
	ds_instance_id integer,
	execution_start_on text DEFAULT '',
	execution_complete_on text DEFAULT '',
    total_execution_time text DEFAULT '',
	no_of_poi integer DEFAULT 0,	
	"timestamp" text,
	created_at timestamp without time zone DEFAULT now(),	
	CONSTRAINT fk_ds_instance
    FOREIGN KEY(ds_instance_id) 
	REFERENCES master_data_source_instances(id)
);

CREATE TABLE IF NOT EXISTS master_brand_country_mapping(
	id serial PRIMARY KEY,
	ro_brand_id text not null,
	iso_country_code text not null,
	created_at timestamp without time zone DEFAULT now(),
	UNIQUE (ro_brand_id,iso_country_code),
    CONSTRAINT ro_brand_id FOREIGN KEY (ro_brand_id) REFERENCES master_brands(ro_brand_id),
	CONSTRAINT iso_country_code FOREIGN KEY (iso_country_code) REFERENCES master_countries(iso_country_code)
);

CREATE TABLE IF NOT EXISTS master_data_source_brand_mapping(
	id serial PRIMARY KEY,
	ro_brand_id text not null,
	ds_id text not null,
	created_at timestamp without time zone DEFAULT now(),
	UNIQUE (ro_brand_id,ds_id),
    CONSTRAINT ro_brand_id FOREIGN KEY (ro_brand_id) REFERENCES master_brands(ro_brand_id),
	CONSTRAINT ds_id FOREIGN KEY (ds_id) REFERENCES master_data_sources(id)	
);

-- ########################  END OF MASTER TABLES ###################################################################3

CREATE TABLE IF NOT EXISTS poi_merged (
    ro_place_id text PRIMARY KEY NOT NULL,
    parent_ro_place_id text,
    location_name text,
	location_name_cleansed text default '',
    ro_brand_ids text[],
    brands text[],
    top_category text,
    sub_category text,    
    lat numeric,
    long numeric,
    street_address text,
    city text,
    region text,
    postal_code text,
    iso_country_code text,
    phone_number text,
    open_hours text,
	email text,
    fax text,
    weburl text,
    category_tags text,    
    "timestamp" text,    
    created_at timestamp without time zone DEFAULT now(),
    ds_instance_id integer,
	last_relevance_date date,
	iteration_dimension text DEFAULT '',	
	CONSTRAINT ds_instance_id FOREIGN KEY (ds_instance_id)
        REFERENCES master_data_source_instances (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);


