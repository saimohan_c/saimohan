import pandas as pd
import psycopg2
from psycopg2 import pool
from pconf import Pconf

config = {
"host":"localhost",
"dbname":"reomnify_test",
"user":"postgres",
"password":"saimohan1",

 }

from reomnify_poi_ingestion.common_utils import log_helper 


logger = log_helper.get_logger('root')

# Get all the config values parsed from the sources
#config = Pconf.get()

logger.info("Connecting to database ...")
# print(config['host'],config['dbname'],config['user'],config['password'])

# Creating a common database connection pool
postgreSQL_pool = pool.SimpleConnectionPool(1, 20, user=config['user'],
                                            password=config['password'],
                                            host=config['host'],
                                            port="5432",
                                            database=config['dbname'])

postgreSQL_pool.autocommit = True 

def get_connection_url():

    try:

        Pconf.env()
        Pconf.file('/dbconf.json', encoding='json')

        # Get all the config values parsed from the sources
        #config = Pconf.get()
        
        print("Connecting to database ...")
        # print(config['host'],config['dbname'],config['user'],config['password'])
        return "postgresql://"+config['user']+":"+config['password']+"@"+config['host']+":5432/"+config['dbname']

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

def open_db_connect():

    conn = open_db_connect_new()
    db_connection = next(conn, None)
    return db_connection

def open_db_connect_new():
    """
    psycopg2 connection context manager.
    Fetch a connection from the connection pool and release it.
    """
    try:
        connection = postgreSQL_pool.getconn()
        connection.autocommit = True
        yield connection
        postgreSQL_pool.putconn(connection)
        
    finally:
        postgreSQL_pool.putconn(connection)
        print("Released a connection in finally block .. ")

def close_db_connection(conn):
    if conn is not None:
        conn.close()
        print('Database connection closed.')

def get_query_data(query,db_cursor=None):

    if db_cursor is None:
        db_cursor =  open_db_connect().cursor()
        
    # Execute query
    db_cursor.execute(query)

    # Fetched all records
    records = db_cursor.fetchall()

    # Extract the column names
    col_names = []
    for elt in db_cursor.description:
        col_names.append(elt[0])

    df = pd.DataFrame(records, columns=col_names)

    return df