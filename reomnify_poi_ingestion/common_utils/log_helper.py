
## Logger Details
import logging
import logging.config
from os import path

def get_logger(name) :

    log_file_path = path.join(path.dirname(path.abspath(__file__)),'logging.conf')
    logging.config.fileConfig(log_file_path)
    return logging.getLogger(name)