from reomnify_poi_ingestion.common_utils import db_helper
from reomnify_poi_ingestion.poi_harvesting.helpers.categories import ro_categories
import os
import sys
import json
import pandas as pd
import numpy as np
import traceback
from reomnify_poi_ingestion.common_utils import log_helper 

logger = log_helper.get_logger('dedupe_executer.py')

def get_datasources_by_country_to_execute(iso_country_code):

    # Get databse cursor
    db_cursor = db_helper.open_db_connect().cursor()

    # Query string
    query = """SELECT string_agg(id::text, ',') FROM master_data_sources
                where iso_country_code = '{0}'"""

    # Execute query
    db_cursor.execute(query.format(iso_country_code))

    # Fetched all records
    record_string = db_cursor.fetchall()

    return record_string

def get_datasources_for_brands_by_country_to_execute(iso_country_code):

    # Get databse cursor
    db_cursor = db_helper.open_db_connect().cursor()

    # Query string
    query = """SELECT string_agg(DISTINCT(dsi.ds_id)::text, ',')
               FROM master_data_source_instances dsi
               INNER JOIN master_data_sources ds ON ds.id=dsi.ds_id
               INNER JOIN master_data_source_brand_mapping dsbm ON dsbm.ds_id = dsi.ds_id
               WHERE ds.iso_country_code= '{0}'"""

    # Execute query
    db_cursor.execute(query.format(iso_country_code))

    # Fetched all records
    record_string = db_cursor.fetchall()

    return record_string

def get_data_source(data_source_id):

    # Get databse cursor
    db_cursor =  db_helper.open_db_connect().cursor()   

    try:
        query = """select ds.id, master_cities.id as city_id, master_cities.city, master_data_source_instances.id as ds_instance_id,
                  origin, path, destination_table, frequency, handler,is_auth_required,params, 
                  master_cities.city as city_name, master_regions.region as region, master_regions.region_short_code as region_short_code,
				  master_countries.country as country, lat, long, master_cities.iso_country_code as iso_country_code,
                  user_id, password, refresh_token, master_data_source_instances.status as ds_instance_status
                from master_data_sources ds
                INNER JOIN master_data_source_instances ON  ds.id = master_data_source_instances.ds_id
                INNER JOIN master_cities ON  master_data_source_instances.city_id = master_cities.id
                INNER JOIN master_regions ON  master_regions.region = master_cities.region
				INNER JOIN master_countries ON  master_countries.iso_country_code = master_cities.iso_country_code
                LEFT OUTER JOIN master_data_source_credentials ON  master_data_source_instances.ds_id = master_data_source_credentials.ds_id
                where ds.id='{0}' and ds.active=true order by master_data_source_instances.id"""

        query = query.format(data_source_id)

        df = db_helper.get_query_data(query, db_cursor)

        return df

    except Exception as e:
        logger.error(traceback.print_exc())
        logger.error(e)

def get_cities_by_country(iso_country_code):

    # Get databse cursor
    db_cursor =  db_helper.open_db_connect().cursor()   

    # Query string
    query = """SELECT id,city,region,lat,long,iso_country_code,postal_code 
            FROM(
                SELECT DISTINCT ON (lat,long) *
                FROM(
                    select DISTINCT ON (postal_code) *
                    from master_cities  as ms
                    where iso_country_code='{0}' and region !='ALL'
                    )ps
                )s ORDER BY id;""".format(iso_country_code)

    df = db_helper.get_query_data(query, db_cursor)

    return df

def get_regions_by_country(iso_country_code):

    # Get databse cursor
    db_cursor =  db_helper.open_db_connect().cursor()   

    query = """select region,region_short_code,iso_country_code
            from master_regions 
            where iso_country_code='{0}' and region !='ALL' ;""".format(iso_country_code)

    df = db_helper.get_query_data(query, db_cursor)

    return df

def get_brands(input_str):

    # Get databse cursor
    db_cursor =  db_helper.open_db_connect().cursor()   
    
    if "'" in input_str:
        input_str = input_str.replace("'", "")
        print(input_str)

    brand_query = """select ro_brand_id, brand_name from master_brands where '{0}' = ANY(map_text)"""
    brand_query = brand_query.format(input_str)

    db_cursor.execute(brand_query)

    # Fetched all records
    records = db_cursor.fetchall()

    # Extract the column names
    col_names = []
    for elt in db_cursor.description:
        col_names.append(elt[0])

    brands_df = pd.DataFrame(records, columns=col_names)

    return brands_df

def get_all_brands(input_str):

    # Get databse cursor
    db_cursor =  db_helper.open_db_connect().cursor()   

    brand_query = """select ro_brand_id, brand_name,map_text from master_brands"""

    db_cursor.execute(brand_query)

    # Fetched all records
    records = db_cursor.fetchall()

    # Extract the column names
    col_names = []
    for elt in db_cursor.description:
        col_names.append(elt[0])

    brands_df = pd.DataFrame(records, columns=col_names)

    return brands_df

def get_brands_by_input_str(input_str):

    # Get databse cursor
    db_cursor =  db_helper.open_db_connect().cursor()   

    brands_df = get_all_brands(db_cursor, input_str)

    brand_ids = []
    brand_names = []

    for brand_index in range(len(brands_df)):
        mapping_texts = brands_df['map_text'][brand_index]

        if mapping_texts is not None:
            for map_text_index in range(len(mapping_texts)):
                map_text = brands_df['map_text'][brand_index][map_text_index]

                if map_text in input_str.lower():
                    brand_ids.append(
                        brands_df['ro_brand_id'][brand_index])
                    brand_names.append(
                        brands_df['brand_name'][brand_index])
                    break

    return brand_ids, brand_names

def get_ro_place_id(db_cursor=None):

    # Get databse cursor
    db_cursor =  db_helper.open_db_connect().cursor()   
    
    db_cursor.execute("SELECT uuid_generate_v4()")

    uuid = db_cursor.fetchone()
    uuid = uuid[0]

    return "ro:" + uuid

def delete_previous_records_of_same_timestamp(ds_destination_table, timestamp, iso_country_code, ds_instance_id):

    print("Deleting previous records if any exist for same timestamp and data source instance")
    try:
        # Get databse cursor
        db_cursor =  db_helper.open_db_connect().cursor()   

    
        query = """ delete from {0} where timestamp='{1}' and ds_instance_id='{2}' 
        """.format(ds_destination_table, timestamp, ds_instance_id)
        
        db_cursor.execute(query)

    # delete data for same timestamp
        query = """ delete from poi_merged where timestamp='{0}' and ds_instance_id='{1}' and iso_country_code='{2}'
        """.format(timestamp, ds_instance_id, iso_country_code)
        
        db_cursor.execute(query)
        
    except:
        logger.error(traceback.print_exc())

def save_master_data_source_execution_time_logs(info):

    # Get databse cursor
    db_cursor =  db_helper.open_db_connect().cursor()   
    
    # Get data source execution poi count
    query = """ select count(*) from poi_merged where ds_instance_id={0} and timestamp='{1}'
            """.format(info['ds_instance_id'],info['timestamp'])
    
    db_cursor.execute(query)

    records = db_cursor.fetchall()
    no_of_poi = records[0]

    # Delete previous data source execution time logs
    query = """ delete from master_data_source_execution_time_logs where ds_instance_id={0} and timestamp='{1}'
            """.format(info['ds_instance_id'],info['timestamp'])
    
    db_cursor.execute(query)

    # Save data source execution time log
    query = """INSERT INTO master_data_source_execution_time_logs(
	        ds_id, ds_instance_id, execution_start_on, 
            execution_complete_on,total_execution_time, no_of_poi, "timestamp")
	        VALUES (%s, %s, %s, %s, %s, %s, %s);
            """
    record_to_insert = (info['data_source_id'], info['ds_instance_id'],
                        info['execution_start_on'], info['execution_complete_on'],
                        info['total_execution_time'], no_of_poi, info['timestamp'])

    db_cursor.execute(query, record_to_insert)

def get_poi_category(category_key):
    top_category,sub_category = '',''

    category = list(
            filter(lambda row: row['key'] == category_key, ro_categories))

    if len(category) > 0:
        top_category = category[0]['top_category']
        sub_category = category[0]['sub_category']

    category = {"top_category":top_category,"sub_category":sub_category}

    return category
