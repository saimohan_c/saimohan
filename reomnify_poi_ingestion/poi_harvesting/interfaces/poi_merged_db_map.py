class poi_merged:

    def __init__(self):
        self.ro_place_id = ''
        self.parent_ro_place_id = None
        self.location_name = ''
        self.location_name_cleansed= ''
        self.lat=0
        self.long=0
        self.street_address=''
        self.city=''
        self.region=''
        self.postal_code='0'
        self.phone_number=''
        self.email=''
        self.fax=''
        self.weburl=''
        self.open_hours=''
        self.last_relevance_date=None
        self.category_tags=''
        self.top_category=''
        self.sub_category=''
        self.iso_country_code=''
        self.ro_brand_ids=[]
        self.brands=[]
        self.ds_instance_id=0
        self.timestamp=''
        self.created_at=''