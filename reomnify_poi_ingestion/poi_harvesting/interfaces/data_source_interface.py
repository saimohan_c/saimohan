# System Imports
import os,io,sys,re,json,demjson,ast,yaml,xmltodict,csv,time,html
import abc, traceback, pandas as pd
import datetime,yaml,codecs
import numpy as np

from bs2json import bs2json
from sqlalchemy import create_engine
from sqlalchemy.dialects.postgresql import insert
from abc import ABC, abstractmethod
from datetime import datetime as dt
from datetime import date
from urllib.parse import unquote

import cloudscraper

sys.path.append(str(os.getcwd()))

## DB Imports
import reomnify_poi_ingestion.poi_harvesting.crud_database.constants as constants

from reomnify_poi_ingestion.common_utils.db_helper import get_query_data,get_connection_url
from reomnify_poi_ingestion.common_utils.ds_helper import get_cities_by_country,get_regions_by_country,get_ro_place_id,get_brands_by_input_str,get_poi_category,delete_previous_records_of_same_timestamp

# Common Uitilites Setting
import reomnify_poi_ingestion.common_utils.log_helper as log_helper

from interfaces.poi_merged_db_map import poi_merged
from reomnify_poi_ingestion.poi_harvesting.helpers.helper import get_current_timestamp,get_current_time,get_open_hours_formatted
from reomnify_poi_ingestion.poi_harvesting.helpers.helper import get,post,get_url_html_content,get_bs4_soup_content
from reomnify_poi_ingestion.poi_harvesting.helpers.helper import get_12hr_format_time,get_day_key,get_day_no,get_state_code
from reomnify_poi_ingestion.poi_harvesting.helpers.configurations import TEMPORARILY_CLOSED,PERMANENTLY_CLOSED,CLOSED,TWENTY_FOUR_HOURS,OPENING_SOON,TBL_SHOPPING_CENTRE_AU,STOCKIST,STORE

logger = log_helper.get_logger('ds_runner.py')

class DataSourceInterface(ABC):
    """ Data source data transformation interface """
    
    # @abstractmethod
    def get_request_url(self):
        """Get request URL string."""
        raise NotImplementedError

    # @abstractmethod
    def load_source_data(self, data_source_data: object):
        """Load data source into the database."""
        raise NotImplementedError

    # @abstractmethod
    def transform_source_data(self, df: object):
        """ Transform data source data into poi merged"""
        raise NotImplementedError

    def to_sql_on_conflict_do_nothing(pd_table, conn, keys, data_iter):
        # This is very similar to the default to_sql function in pandas
        # Only the conn.execute line is changed
        data = [dict(zip(keys, row)) for row in data_iter]
        conn.execute(insert(pd_table.table).on_conflict_do_nothing(), data)
    
    @staticmethod
    def insert_to_data_lake(records_to_insert,columns,data_source_table,type):
        
        if type == 'SQL':            
            engine = create_engine(get_connection_url())
            df = pd.DataFrame(columns=columns, data=records_to_insert)
            df.to_sql(data_source_table, engine,if_exists='append', chunksize=5000 , index = False,method=DataSourceInterface.to_sql_on_conflict_do_nothing)
            
        else:
            print("Cloud")

        
    @staticmethod
    def create_merged_object(store) :
        poi_merged_obj = poi_merged()

        for column in constants.POI_MERGED_COLUMNS :
            
                if column in store.keys():
                    setattr(poi_merged_obj,column,store[column])
        
        return poi_merged_obj

    @staticmethod
    def insert_data(df,func,func2,data_source_columns,data_source_configuration):

        timestamp = get_current_timestamp(data_source_configuration['frequency'])
        created_at = get_current_time()

        record_append_tuple = (data_source_configuration['ds_instance_id'],timestamp, created_at)
        records_to_insert = DataSourceInterface.get_records_to_insert(df,func,record_append_tuple,source = 'source_table')
        DataSourceInterface.insert_to_data_lake(records_to_insert,data_source_columns,data_source_table=data_source_configuration['destination_table'],type='SQL')

        records_to_insert =  DataSourceInterface.get_data_source_table_data(data_source_configuration['destination_table'],data_source_configuration['timestamp'],data_source_configuration['ds_instance_id'])
        
        df = pd.DataFrame(records_to_insert, columns=data_source_columns)
        df['destination_table'] = data_source_configuration['destination_table']

        df = func2(df)
        records_to_insert = DataSourceInterface.get_records_to_insert(df,source= 'poi_merged_table')
        DataSourceInterface.insert_to_data_lake(records_to_insert,columns=constants.POI_MERGED_COLUMNS ,data_source_table='poi_merged',type='SQL')

    @staticmethod
    def get_records_to_insert(df,func=None, record_append_tuple=None, source='source_table'):

        records_to_insert = []

        if source == 'poi_merged_table' :
            for index, store in df.iterrows():
                poi_merged_obj = DataSourceInterface.create_merged_object(store)
                
                record_to_insert = DataSourceInterface.to_record(poi_merged_obj)
                records_to_insert.append(record_to_insert)

        if source == 'source_table' :
            
            for store in df:

                try:  
                    record_to_insert = func(store)
                    
                    if record_to_insert is None or len(record_to_insert) == 0:
                        continue

                    ro_place_id = get_ro_place_id()
                    
                    record_to_insert += (ro_place_id,)
                    record_to_insert += record_append_tuple
                    
                    records_to_insert.append(record_to_insert)
                except:
                    logger.error(traceback.print_exc())
                    continue
                    
        return records_to_insert
    
    @staticmethod 
    def get_data_source_table_data(destination_table,timestamp,ds_instance_id):
        
        query = """select * from {0}
                    where timestamp='{1}' and ds_instance_id={2}""".format(destination_table,timestamp,ds_instance_id)

        records_to_insert= get_query_data(query)
        
        return records_to_insert
    
    @staticmethod
    def to_record(poi_merged_obj) :

        return (poi_merged_obj.ro_place_id,poi_merged_obj.parent_ro_place_id,poi_merged_obj.location_name, 
                poi_merged_obj.location_name_cleansed,poi_merged_obj.lat, poi_merged_obj.long, poi_merged_obj.street_address, 
                poi_merged_obj.city,poi_merged_obj.region,poi_merged_obj.postal_code,poi_merged_obj.phone_number,
                poi_merged_obj.email,poi_merged_obj.fax,poi_merged_obj.weburl,poi_merged_obj.open_hours,poi_merged_obj.last_relevance_date,
                poi_merged_obj.category_tags,poi_merged_obj.top_category, poi_merged_obj.sub_category,
                poi_merged_obj.iso_country_code,poi_merged_obj.ro_brand_ids, poi_merged_obj.brands,
                poi_merged_obj.ds_instance_id, poi_merged_obj.timestamp, poi_merged_obj.created_at)

    @staticmethod
    def get_stores_by_city(iso_country_code,func=None):

        cities = get_cities_by_country(iso_country_code)

        all_stores = []

        for index, city in cities.iterrows():
            
            try:
                stores = func(city)
                all_stores.extend(stores)
            except:
                logger.error(traceback.print_exc())
                continue

        return all_stores