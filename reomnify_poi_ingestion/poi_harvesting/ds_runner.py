import sys,os
import datetime
import traceback

sys.path.append(str(os.getcwd()))

from reomnify_poi_ingestion.common_utils.db_helper import open_db_connect,get_query_data
from reomnify_poi_ingestion.common_utils.ds_helper import get_data_source, delete_previous_records_of_same_timestamp,get_datasources_by_country_to_execute,get_datasources_for_brands_by_country_to_execute,save_master_data_source_execution_time_logs
import reomnify_poi_ingestion.common_utils.log_helper as log_helper
from reomnify_poi_ingestion.poi_harvesting.helpers.helper import get_current_timestamp
import handlers.index as index_handler

logger = log_helper.get_logger('ds_runner.py')

ds_to_skip = []

def get_database_cursor():
    db_connection = open_db_connect()

    if db_connection is None:
        logger.info("Unable to connect database")
        return None

    db_cursor = db_connection.cursor()

    return db_cursor

def execute_data_source(data_source_id,sys_argv):

    db_cursor = get_database_cursor()

    if db_cursor is None:
        return

    # Get all data source configurations
    configurations = get_data_source(data_source_id)

    # Continue if configuration is not avaliable
    if configurations is None or len(configurations) == 0:
        logger.info("Data Source is not avaliable, Please check Data Source ID")
        return

    start_data_source_execution(db_cursor, configurations, sys_argv)

def start_data_source_execution(db_cursor, configurations,sys_argv):
    is_force_to_run_ds = sys_argv['is_force_to_run_ds'] if 'is_force_to_run_ds' in sys_argv.keys() else False

    # Extracted data for data source and insert it into destination table
    
    for index, configuration in configurations.iterrows():
        
        configuration['timestamp'] = get_current_timestamp(configuration['frequency'])

        ds_instance_status = configuration['ds_instance_status']
        
        ds_instance_id, data_source_id, city = configuration["ds_instance_id"],configuration["id"],configuration["city"]
       
        ds_destination_table =configuration['destination_table']
        timestamp,iso_country_code = configuration['timestamp'],configuration['iso_country_code']
        is_ds_already_executed = False

        try:
            logger.info(str(ds_instance_status))

            if ds_instance_status == False:
                delete_previous_records_of_same_timestamp(ds_destination_table, timestamp, iso_country_code, ds_instance_id)
                continue

            logger.info("#################################################")
            logger.info("Data Source Id:" + data_source_id)
            logger.info("City & Data Source Instance Id: " + str(city) + "," + str(ds_instance_id))

            if is_force_to_run_ds == False:
                is_ds_already_executed = is_ds_instance_already_executed(db_cursor, ds_instance_id, ds_destination_table, timestamp)

            logger.info(str(is_ds_already_executed))

            if is_ds_already_executed == False:
                logger.info('Deleting previous records')
                delete_previous_records_of_same_timestamp(ds_destination_table, timestamp, iso_country_code, ds_instance_id)
                logger.info(timestamp)
                execution_start_on = datetime.datetime.now().strftime('%H:%M:%S')
                
                index_handler.init(db_cursor,configuration)

                execution_complete_on = datetime.datetime.now().strftime('%H:%M:%S')

                total_execution_time = (datetime.datetime.strptime(execution_complete_on,'%H:%M:%S')
                                       - datetime.datetime.strptime(execution_start_on,'%H:%M:%S'))

                info = {
                    'data_source_id': data_source_id,
                    'ds_instance_id': ds_instance_id,
                    'timestamp': timestamp,
                    'execution_start_on':execution_start_on,
                    'execution_complete_on':execution_complete_on,
                    'total_execution_time':total_execution_time 
                }

                save_master_data_source_execution_time_logs(info)

            else:
                logger.info("This data source instance already executed")

        except Exception as e:
            logger.error(traceback.print_exc())
            raise
            
def is_ds_instance_already_executed(db_cursor, ds_instance_id, ds_destination_table, timestamp):
    
    logger.info("Checking data source instance already executed or not")

    query = """ select count(*) as records from {0} where timestamp='{1}' and ds_instance_id='{2}'
        """.format(ds_destination_table, timestamp, ds_instance_id)

    ds_table_records = get_query_data(query, db_cursor)
    ds_table_records = ds_table_records['records'][0]

    # delete data for same timestamp
    query = """ select count(*)  as records from {0} where timestamp='{1}' and ds_instance_id='{2}' 
            """.format('poi_merged', timestamp, ds_instance_id)

    poi_merged_table_records = get_query_data(query, db_cursor)
    poi_merged_table_records = poi_merged_table_records['records'][0]

    if (ds_table_records != 0 and poi_merged_table_records != 0 and ds_table_records == poi_merged_table_records):
        
        logger.info("Total data source instance records:" +str(ds_table_records))
        logger.info("Total data source instance poi merged records:" +str(poi_merged_table_records))
        return True

    return False

def main(argv):

    sys_argv = {}
    logger.info(argv)

    if len(argv) == 2:
        argv1 = argv[0].lower()
        
        if (argv1 != '--force') and (argv1 != '--country') and(argv1 != '--brands'):
            logger.info("--force or  --country or --brands parameter is missing")
            return

        if argv1 == '--force':

            sys_argv['is_force_to_run_ds'] = True
            data_sources = argv[1]

        elif argv1 == '--brands':

            country_code = argv[1]
            logger.info("Getting brands for ",country_code)            
            data_sources = get_datasources_for_brands_by_country_to_execute(country_code)
            data_sources = data_sources[0][0]
            logger.info("Got brand data sources from database",len(data_sources),'::',data_sources)      
        
        else :

            country_code = argv[1]

            logger.info("As no parameter is passed running all the data sources.")
            data_sources = get_datasources_by_country_to_execute(country_code)
            data_sources = data_sources[0][0]
            logger.info("Got data sources from database",len(data_sources),'::',data_sources)        


    elif len(argv) == 1:
        data_sources = argv[0]
    
    data_sources = data_sources.split(",")
    logger.info("Number of data source for execution:" + str(len(data_sources)))

    for data_source_id in data_sources:

        if data_source_id in ds_to_skip : continue
        logger.info("Start execution for : " + str(data_source_id))

        try:
            execute_data_source(data_source_id,sys_argv)

        except Exception:
            
            logger.error("Failure occured in processing the following data source : "+ str(data_source_id))
            logger.error(traceback.print_exc())
        
        logger.info("In the finally block of exception. Done with the data source : "+ str(data_source_id))

if __name__ == '__main__':
    main(sys.argv[1:])
