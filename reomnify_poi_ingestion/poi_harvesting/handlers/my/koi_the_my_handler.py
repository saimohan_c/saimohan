from interfaces.data_source_interface import *
from crud_database.constants import KOI_THE_MY_COLUMNS

class KoiTheMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Koi The Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Koi The stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = KOI_THE_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Koi The stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        soup = soup.find('div',{'class':'tab-pane fade in active'})
        stores = soup.find_all('li',{'class':'col-sm-6 col-md-4 paLRsm30 col-xs-1a2'})
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store.find('h3',{'class':'col fz-h6'})
        title = title.text if title is not None else ''

        p_Tags = store.find_all('p',{'class':'txt fz-default'})
        a_link = p_Tags[1].find('a')
        a_link = a_link['href']
        response = get_url_html_content(a_link,headers=self.headers)
        soup = get_bs4_soup_content(response)

        lat,lng = self.get_store_geo_coordinates(soup)

        address,city,state,post_code,phone = self.get_store_address(soup)        
        open_hours = self.get_open_hours(soup)
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            script_Tags = store.find_all('script')
            script_Tags = list(filter(lambda script: 'LatLng' in script.text,script_Tags))
            script_Tags = str(script_Tags[0]).split(');')[0]
            lat_lng = script_Tags.split('LatLng(')[1]
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        p_Tags = store.find_all('p',{'class':'fz-default'})

        address = p_Tags[1]
        address = address.text.strip() if address is not None else ''
        post_code = 0
        city = ''
        state = ''
        phone = p_Tags[0]
        phone = phone.text.strip() if phone is not None else ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):
        p_Tags = store.find_all('p',{'class':'fz-default'})
        working_hours = p_Tags[2]
        working_hours = working_hours.text if working_hours is not None else ''
        working_hours = working_hours if ')' not in str(working_hours) else ''
        open_close_hours = working_hours.split('-')
        opening_hours = []
        days = ['sun','mon','tue','wed','thu','fri','sat']
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else '10:00'
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else '22:00'
            opening_hours.append(day_open_hours)
        #working_hours_different_format  = working_hours_different_format.split('\r\n')
        #print(working_hours_different_format)
        return get_open_hours_formatted(opening_hours)