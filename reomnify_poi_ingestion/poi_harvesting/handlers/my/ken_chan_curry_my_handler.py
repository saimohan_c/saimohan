from interfaces.data_source_interface import *
from crud_database.constants import KEN_CHAN_CURRY_MY_COLUMNS

class KenChanCurryMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Ken Chan Curry Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Ken Chan Curry stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = KEN_CHAN_CURRY_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Ken Chan Curry stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('div',{'class':'hJDwNd-AhqUyc-II5mzb purZT-AhqUyc-II5mzb pSzOP-AhqUyc-II5mzb JNdkSc yYI8W'})
        
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store.find('div',{'class':'CjVfdc'})
        title = title.text if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)
        store_address_details = store.find('div',{'class':'hJDwNd-AhqUyc-II5mzb pSzOP-AhqUyc-II5mzb jXK9ad D2fZ2 GNzUNc'})

        address,city,state,post_code,phone = self.get_store_address(store_address_details)        
        open_hours = self.get_open_hours(store_address_details)
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            map_button = store.find('iframe')
            src = map_button['src']
            if src == '':
                return 0,0
            lat_lng = src.split('&ll=')[1]
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[0])
            lng = lat_lng[1]
            lng = float(lng.split('&')[0])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        address = store.find('small',{'class':'CDt4Ke zfr3Q TMjjoe'}).text if store is not None else ''
        post_code =0
        city = ''
        state = ''
        all_p_tags = store.find_all('p',{'class':'CDt4Ke zfr3Q'})
        phone = all_p_tags[1].text.split('TEL')[1].strip() if all_p_tags is not None else ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):
       
        all_p_tags = store.find_all('p',{'class':'CDt4Ke zfr3Q'})
        days = all_p_tags[3].text
        timings = all_p_tags[4].text
        opening_hours = []
        days = days.split('to')
        start_day = get_day_no(days[0].strip())
        end_day = get_day_no(days[1].strip())
        open_close_hours = timings.split('-')
        for day in range(start_day,end_day+1):
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(day)
            day_open_hours['open_time'] = open_close_hours[0].strip(' ') if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip(' ') if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)