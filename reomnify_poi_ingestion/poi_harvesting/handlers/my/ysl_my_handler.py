from interfaces.data_source_interface import *
from crud_database.constants import YSL_MY_COLUMNS

class YSLBeautyMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start YSL Beauty Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load YSL Beauty stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = YSL_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load YSL Beauty stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        print(html_content)
        soup = get_bs4_soup_content(html_content)
        print(soup)
        stores = soup.find_all('marker')
        print(stores)

        logger.info("Total Stores:"+str(len(stores)))

        return stores

    def get_store_details(self, store):
        
        title = store['name']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = ''
        
        return (title,address, city, post_code,phone,lat, lng,state,open_hours)

    def get_store_geo_coordinates(self,store):


        lat,lng=0,0
        try:
            lat = float(store['lat'])
            lng = float(store['lng'])
        except:
            lat = 0
            lng = 0
        return lat,lng

    def get_store_address(self,store):
        
        post_code = store['postal']
        city = store['city']
        state = store['state']
        phone = store['phone']
        address = store['address']
        
        return address,city,state,post_code,phone
