from interfaces.data_source_interface import *
from crud_database.constants import CHILLIPANMEE_MY_COLUMNS

class ChilliPanMeeMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Chilli Pan Mee Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Chilli Pan Mee stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = CHILLIPANMEE_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Chilli Pan Mee stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores_div = soup.find('div',{'class':'et_pb_code_inner'})
        stores = stores_div.find_all('div')
        stores = list(filter(lambda store: 'Singapore' not in str(store),stores))
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = 'ChilliPanMee'

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        #open_hours = self.get_open_hours(store)
        open_hours = ''
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        a_tag = store.find('a',{'class':'locationimg'})
        href = a_tag['href']
        response = get(href)
        response = response.url
        if '@' in response:
            lat_lng = response.split('/@')[1]
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        
        return lat,lng

    def get_store_address(self,store):
        
        p_tags = store.find_all('p')
        post_code =0
        city = ''
        state = ''
        address = store.find('p',{'class':'locationaddress'})

        address = address.text.replace('\n','').strip() if address is not None else ''
        phone = p_tags[1]
        if ':' in phone.text:
            phone = phone.text.split(':')[1]
            phone = phone.strip() if phone is not None else ''
        else:
            phone = ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
    