from interfaces.data_source_interface import *
from crud_database.constants import PANDORA_MY_COLUMNS

class PandoraMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Pandora Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'accept': '*/*',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Pandora stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = PANDORA_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Pandora stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region','store_type':'category_tags'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        stores = get(url,headers=self.headers)
        stores = stores.text.split("['my'] =")[1]
        stores = json.loads(stores.split('; Consumer.StoreLocator')[0])
        logger.info("Total stores: "+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store['Caption']
        title = title if title is not None else ''

        store_id = store['ID']
        store_id = store_id if store_id is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)
        
        type = store['Type']
        store_type = STORE if store['Type'] == "Pandora Store" else STOCKIST

        address,city,state,post_code,phone = self.get_store_address(store)

        open_hours = self.get_open_hours(store)

        email = store['Contact']['Email']
        #PANDORA_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','type','store_type''ro_place_id','ds_instance_id','timestamp','created_at']

        return (store_id,title,address, city, post_code,phone,email,lat,lng,state,open_hours,type,store_type)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            lat = float(store['GeoInfo']['Latitude'])
            lng = float(store['GeoInfo']['Longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        Address = store['Address']
        post_code = Address['PostalCode']
        state = Address['State']
        city = Address['City']
        address = Address['Street']
        phone = store['Contact']['Phone']

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone

    def get_open_hours(self,store):

        working_hours = store['Schedule'][0]
        start_day = working_hours['FirstDay']
        end_day = working_hours['LastDay']
        opening_hours = []
        for day in range(start_day,end_day+1):
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(day)
            day_open_hours['open_time'] = working_hours['OpensAt'].strip() if working_hours['OpensAt'] is not None else ''
            day_open_hours['close_time'] = working_hours['ClosesAt'].strip() if working_hours['ClosesAt'] is not None else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)
    