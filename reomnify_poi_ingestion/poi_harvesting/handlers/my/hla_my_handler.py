from interfaces.data_source_interface import *
from crud_database.constants import HLA_MY_COLUMNS

class HLAMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start HLA Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'http://www.hla.com/website/store.html',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load HLA stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = HLA_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load HLA stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.text.split('function render()')[0]
        stores = stores.split("var stores =")[1].strip()
        stores = json.loads(str(stores))
        stores = stores['Malaysia']
        stores = stores.items()

        new_stores_list = []
        for store in stores:
            store_state = store[0]
            for sub_store in store[1]:
                sub_store['state'] = store_state
                new_stores_list.append(sub_store)

        logger.info("Total stores: "+str(len(new_stores_list)))
        return stores

    def get_store_details(self, store):
                
        title = store['name']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,post_code = self.get_store_address(store)
        phone = store['tel']
        state = store['state']
        open_hours = ''
        #open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        return 0,0

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        
        address = store['address']

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,post_code

    