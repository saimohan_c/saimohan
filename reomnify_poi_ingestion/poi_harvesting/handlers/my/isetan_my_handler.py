from interfaces.data_source_interface import *
from crud_database.constants import ISETAN_MY_COLUMNS

class IsetanMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Isetan Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Isetan stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = ISETAN_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Isetan stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores_ul = soup.find('ul',{'class':'wd-sub-menu sub-menu color-scheme-dark'})
        stores_a_tags = stores_ul.find_all('a')
        stores_a_tags = [store_a_tag['href'] for store_a_tag in stores_a_tags]
        stores = []
        for store_a_tag in stores_a_tags:
            new_html_content = get_url_html_content(store_a_tag,headers=self.headers)
            soup = get_bs4_soup_content(new_html_content)
            stores.append(soup)

        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store.find('div',{'class':'liner-continer'})
        title = title.find('h4')
        title = title.text if title is not None else ''
        store_complete_details = store.find_all('div',{'class':'wpb_wrapper'})
        store_complete_details = list(filter(lambda store: 'Address' in str(store.text),store_complete_details))
        store_complete_details = store_complete_details[1]

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store_complete_details)        
        open_hours = self.get_open_hours(store_complete_details)
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        
        iframe = store.find('iframe')
        iframe = iframe['src']
        response = get(iframe)
        response = response.url
        lat_lng = response.split('!2d')[1]
        lat_lng = lat_lng.split('!3d')
        lat = float(lat_lng[0])
        if '!2m' in lat_lng[1]:
            lng = lat_lng[1].split('!2m')[0]
            lng = float(lng)
        else:
            lng = lat_lng[1].split('!3m')[0]
            lng = float(lng)
        return lat,lng

    def get_store_address(self,store):
        
        store_address_p_tags = store.find_all('p')
        post_code =0
        city = ''
        state = ''
        address = store_address_p_tags[0].text.split('Address')[1].strip().replace("<br /","")
        phone = store_address_p_tags[2].text.split('Tel')[1].strip().replace('<br /','')
        phone = phone.split('Fax')[1].strip() if 'Fax' in phone else phone.strip()

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):

        opening_hours = []
        store_address_p_tags = store.find_all('p')
        working_hours = store_address_p_tags[1].text
        if working_hours is None:
            return
        days = ['sun','mon','tue','wed','thu','fri','sat']
        for day in days:
            
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = '10.00a.m'
            day_open_hours['close_time'] = '10.00p.m'
            opening_hours.append(day_open_hours)
        
        return get_open_hours_formatted(opening_hours)
