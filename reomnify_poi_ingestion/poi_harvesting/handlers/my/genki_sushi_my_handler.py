from interfaces.data_source_interface import *
from crud_database.constants import GENKI_SUSHI_MY_COLUMNS

class GenkiSushiMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Genki Suchi Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://www.genkisushi.com.my/',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params']
        params = '' if pd.isna(params) else params

        
        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Genki Sushi stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = GENKI_SUSHI_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Genki SUshi stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'list row tab-link'})
        store_locations = soup.find_all('div',{'class':'map-wrap tab-content'})

        for store in range(len(stores)):
            if stores[store]['data-id'] == store_locations[store]['data-id']:
                stores[store].append(store_locations[store])

        return stores

    def get_store_details(self, store):
        
        title = store.find('h4',{'class':'margtop'}).text
        lat,lng = self.get_store_geo_coordinates(store)

        store_details = (store.find('p').text).replace('\n','')
        address_splitted = store_details.split('Tel: ')

        address,city,state,post_code,phone = self.get_store_address(address_splitted)

        new_open_hours = (address_splitted[-1].split('Operating Hours:')[1]).strip() if "Operating Hours:" in address_splitted[-1] else ''
        open_hours = self.get_open_hours(new_open_hours)
        
        return (title,address, city, post_code,phone,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        
        lat,lng=0,0
        iframe = store.find('iframe')
        
        try:
            src = iframe['src']
            
            lat_lng = src.split('!2d')
            
            lat_lng = lat_lng[1].split('!3d')
            
            lng = lat_lng[0]
            
            lng_split = lat_lng[1].split('!')
            
            lat = lng_split[0]
            
        except:
            lat_lng = 0
        
        return lat,lng

    def get_open_hours(self, store_page):
        

        opening_hours = []
        
        open_close_hours = store_page.split(' daily')[0] if store_page is not None else ''

        open_close_hours = open_close_hours.split(' to ') if open_close_hours is not None else ''

        open_hours = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
        close_hours = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
        days = ['sun','mon','tue','wed','thu','fri','sat']

        for day in days:
                day_open_hours = {}
                day_open_hours['day'] = day
                day_open_hours['open_time'] = open_hours
                day_open_hours['close_time'] = close_hours
                opening_hours.append(day_open_hours)
  
        return get_open_hours_formatted(opening_hours)

    def get_store_address(self,store):
        
        post_code =0
        city = ''

        address = store[0].strip() if store[0].strip() != 'Coming Soon' else ''
        try:
            phone = store[1].split('Operating Hours:')[0]
        except:
            phone = ''
        state = address.split(',')[-1]
        
        #address_split = address_splitted[0].split(',')
        #print(address_split)
        
        if re.search('[0-9]{5}',state):
            state_split = re.split(r'[0-9]{5}',state)
            state = state_split[-1]
        
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
