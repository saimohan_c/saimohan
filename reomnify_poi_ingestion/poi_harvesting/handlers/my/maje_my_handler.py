from interfaces.data_source_interface import *
from crud_database.constants import MAJE_MY_COLUMNS

class MajeMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Maje Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'accept': '*/*',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
        }
 
        self.stores_lats_longs = None
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Maje stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = MAJE_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Maje stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        ul = soup.find('ul',{'id':'mapListHKm'})
        stores = ul.find_all('li',{'class':'mapLoc'})
        stores = list(filter(lambda store:'Malaysia' in str(store.text),stores))

        scripts = soup.find_all('script')
        lat_lng_script = list(filter(lambda script:'longs' in str(script),scripts))
        lat_lng_script =  lat_lng_script[0].text
        lat_lng_script = lat_lng_script.split('var markers = [];')[1]
        lat_lng_script = lat_lng_script.split('for(var i=0; i<4; i++){')[0].strip()

        self.stores_lats_longs = lat_lng_script

        logger.info("Total stores: "+str(len(stores)))

        return stores

    def get_store_details(self, store):

        p_tags = store.find_all('p')
        title = 'Maje '+p_tags[0].text if p_tags is not None else 'Maje'

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(p_tags)

        open_hours = ''

        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        print(store['data-index'])
        store_lat = store['data-index']
        store_lat = str(store_lat).split(',')[0]
        
        lat,lng = 0,0

        lat_lng_list = self.stores_lats_longs.split('var langs =')[1]
        lat_lng_list = lat_lng_list.split('var longs = ')

        lats = json.loads(lat_lng_list[0].strip().replace(';',''))
        lngs = json.loads(lat_lng_list[1].strip().replace(';',''))

        try:
            lat_index = lats.index(store_lat)
            lng  = float(lngs[lat_index])
            lat = float(store_lat)
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        post_code = 0
        state = ''
        city = ''
        address = store[1]
        address = address.text if address is not None else ''
        phone = store[2]
        phone = phone.text.split('Tel:')[1].strip() if phone is not None else ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
