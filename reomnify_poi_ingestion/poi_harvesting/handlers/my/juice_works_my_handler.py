from interfaces.data_source_interface import *
from crud_database.constants import JUICE_WORKS_MY_COLUMNS

class JuiceWorksMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Juice Works Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Juice Works stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = JUICE_WORKS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Juice Works stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        params = self.configuration['params']
        stores = []
        stores_titles = []
        stores_directions = []
        stores_addresses = []
        stores_contacts = []
        
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores_divs = soup.find('div',{'id':'outletCon'})
        if stores_divs is not None:
            stores_titles.extend(stores_divs.find_all('div',{'class':'outlet-title'}))
            stores_directions.extend(stores_divs.find_all('div',{'class':'outlet-map'}))
            stores_addresses.extend(stores_divs.find_all('div',{'class':'outlet-address'}))
            stores_contacts.extend(stores_divs.find_all('div',{'class':'outlet-telfax'}))
        
            for title,direction,address,contact in zip(stores_titles,stores_directions,stores_addresses,stores_contacts):
                stores_complete_details = [title,direction,address,contact]
                stores.append(stores_complete_details)

        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store[0].text
        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        #open_hours = self.get_open_hours(store)
        open_hours = ''
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            map_button = store[1].find('a')
            href = map_button['href']
            if href == '':
                return 0,0
            response = get(href)
            response = response.url
            split_character = '/%40' if '/%40' in response else '/@'
            lat_lng = response.split(split_character)[1]
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        except:
            lat,lng = 0,0
        

        return lat,lng

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''
        address = store[2].text.replace('<br />','')
        address = address.replace('<br/>','')
        address = address.replace('\r','').strip()
        phone = store[3].text.split('Fax:')[1].strip()
        phone = '' if phone == '--' else phone
        phone = '' if phone.strip() == 'coming soon' else phone

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
    