from interfaces.data_source_interface import *
from crud_database.constants import HAI_LAU_SHAN_MY_COLUMNS

class HaiLauShanMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Hai Lau Shan Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path'].split(';')[0]
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Hai Lau Shan stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = HAI_LAU_SHAN_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Hai Lau Shan stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        soup = get(url,headers= self.headers)
        countries = soup["data"]["countries"]
        for key,value in countries.items():
            if value['title'] == 'Malaysia':
                malaysia_country_code = value['id']
        cities = soup['data']['cities']
        city_ids = []
        for key,value in cities.items():
            if value['country'] == malaysia_country_code:
                    city_ids.append(value['id'])
        company_types = soup['data']['company_types'].keys()
        
        path = self.configuration['path'].split(';')[1]
        new_url = self.configuration['origin']+path
        params = self.configuration['params'].split('={}')
        enquiry_company_type = params[0]
        request_region = params[1].replace('&','')
        stores = []
        for company_type in company_types:
            for city in city_ids:
                form_data = {
                    enquiry_company_type : company_type,
                    request_region : city
                }
                sub_stores = post(new_url,headers=self.headers,data=form_data)
                if len(sub_stores['data'])>0:
                    stores.append(sub_stores['data'])
        stores = [sub_store for store in stores for sub_store in store]
        logger.info('Total Stores: '+str(len(stores)))
        return stores

    def get_store_details(self, store):
        
        title = store['title']

        lat,lng = self.get_store_geo_coordinates(store['ll'])

        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = ''
        #open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):


        try:
            lat = float(store['lat'])
            lng = float(store['lng'])
        except:
            lat,lng = 0,0
        
        return lat,lng

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''
        address = store['description']
        phone = store['contact_number']
        
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone

    