from interfaces.data_source_interface import *
from crud_database.constants import LE_LABO_MY_COLUMNS

class LeLaboMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Le Labo Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'authority': 'www.lelabofragrances.com',
    'cache-control': 'max-age=0',
    'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
    'sec-ch-ua-mobile': '?0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'none',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
    'cookie': 'JSESSIONID=0C40B8242D0FEFC010733813920B8EB3.10-10-13-125; DEFAULT_ACDC_REGION=IN; acdc_currency=EUR; acdc_vat=false; OptanonAlertBoxClosed=2022-01-10T10:27:18.363Z; _gcl_au=1.1.1850646851.1641810438; _ga=GA1.2.1065708215.1641810439; _fbp=fb.1.1641810439689.927184237; acdc_region=MY; ACDC_LOCALE=EN; acdc_country_code=MY; acdc_int_region=MY; acdc_country=Malaysia; _clck=1t0c41n|1|ey3|0; _gid=GA1.2.1725050268.1642066048; csrfToken=5040204a-23d7-4d24-a048-2d8abda3b6d1; __cf_bm=p9_k9tDZV3IfArQAoY8AltilpIkc9zHavISu3vJSjj8-1642074085-0-AYhJWWYTT/UEeM5YNCrsM56Li0fxVCeghd18FvMGH/fX5SnkD4IarVN+SlRH1o5klteENx3hKufBgEWlfxOdHiQ=; OptanonConsent=isIABGlobal=false&datestamp=Thu+Jan+13+2022+17%3A13%3A28+GMT%2B0530+(India+Standard+Time)&version=5.13.0&landingPath=NotLandingPage&groups=1%3A1%2C2%3A1%2C4%3A1%2C0_246779%3A1&AwaitingReconsent=false; _uetsid=0552a1c0745311ecafa2f7127cbcf0d7; _uetvid=e1a52f4071ff11ec962005a805d15a14; _gat=1; _clsk=13q9cgh|1642074218542|4|1|d.clarity.ms/collect',

        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Le Labo stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LE_LABO_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Le Labo stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('li',{'class':'store-country-MY'})
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = ''
        store_id = ''
        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        #open_hours = self.get_open_hours(store)
        open_hours = ''
        return (store_id,title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        address = ''
        post_code = 0
        city = ''
        state = ''
        phone = ''
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):
       
        working_hours = store['storeHours']

        opening_hours = []
        for day_timing in working_hours:
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(day_timing['day'])
            day_open_hours['open_time'] = day_timing['openingTime']
            day_open_hours['close_time'] = day_timing['closingtime']
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)