from interfaces.data_source_interface import *
from crud_database.constants import HEALTH_LANE_FAMILY_PHARMACY_MY_COLUMNS

class HealthLaneFamilyPharmacyMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Health Lane Family Pharmacy Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Health Lane Family Pharmacy stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = HEALTH_LANE_FAMILY_PHARMACY_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Health Lane Family Pharmacy stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find('textarea',{'id':'irs-all-store-json'})
        stores = list(filter(lambda store: '<br \=""/>' not in str(store),stores))
        stores = [str(store) for store in stores] 
        stores = ''.join(stores)
        stores = stores.replace('null','None')
        stores = list(eval(stores))

        logger.info("Total Stores:"+str(len(stores)))

        return stores

    def get_store_details(self, store):
        
        title = store['store_name']
        store_id = store['store_id']
        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = self.get_open_hours(store)
        
        return (store_id,title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        try:
            lat = float(store['lan'])
            lng = float(store['lng'])
        except:
            lat = 0
            lng = 0
        
        return lat,lng

    def get_store_address(self,store):
        
        try:
            post_code = store['postcode']
            city = store['store_city']
            state = ''
            phone = store['phonenumber']
            address = store['store_address']
        except:
            post_code,city,state,phone,address = 0,'','','',''

        return address,city,state,post_code,phone

    def get_open_hours(self, store):
    
        opening_hours = []
        working_hours = store['working_hours']
        if working_hours == "":
            return ''

        if 'day -' in working_hours:
            working_hours = working_hours.split('day -')
        else:
            working_hours = working_hours.split('day to')

        start_day = working_hours[0].strip()

            #MOnday - Tuesday
            
        open_close_hours = working_hours[1].split('day') if len(working_hours)>1 else []
        weekends_timing  = open_close_hours[2] if len(open_close_hours)>2 else ''
        end_day = open_close_hours[0].strip()
        open_close_hours = open_close_hours[1]
        open_close_hours = open_close_hours.split('-')
        open_hours = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
        close_hours = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
        
        close_hours = close_hours.split('pm')
        weekend_day = close_hours[1]
        close_hours = close_hours[0]+'pm'
            
        end_day = get_day_no(end_day[:3])
        
        start_day = get_day_no(start_day)

        for day in range(start_day,end_day+1):
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(day)
            day_open_hours['open_time'] = open_hours
            day_open_hours['close_time'] = close_hours
            opening_hours.append(day_open_hours)

        if weekend_day !='':  
            weekend_day = get_day_no(weekend_day) 
            weekends_timing = weekends_timing.split('-')
            weekend_open_hours = weekends_timing[0].strip() if len(weekends_timing)>0 else ''
            weekend_close_hours = weekends_timing[1].strip() if len(weekends_timing)>1 else ''
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(weekend_day)
            day_open_hours['open_time'] = weekend_open_hours
            day_open_hours['close_time'] = weekend_close_hours
            opening_hours.append(day_open_hours)
        
        return get_open_hours_formatted(opening_hours)