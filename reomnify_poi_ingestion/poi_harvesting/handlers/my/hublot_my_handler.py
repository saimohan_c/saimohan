from interfaces.data_source_interface import *
from crud_database.constants import HUBLOT_MY_COLUMNS

class HublotMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Hublot Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Hublot stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = HUBLOT_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Hublot stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'js_bq_teaser bq_teaser bq_teaser--boutique bq_teaser--can-service js_type_boutique'})
        
        logger.info("Total Stores:"+str(len(stores)))

        return stores

    def get_store_details(self, store):
        
        origin = self.configuration['origin']
        href = store.find('a',{'class':'bq_teaser__title'})
        href = href['href'] 
        store = origin+href
        store = get_url_html_content(store,headers=self.headers)
        store = get_bs4_soup_content(store)

        title = store.find('h1',{'class':'title-40 bq_header__title'})
        title = title.text.strip() if title is not None else ''
        
        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email = self.get_store_address(store)

        open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        try:
            a_tag = store.find('a',{'class':'js_bq_static_map_link bq_static_map__link_image'})
            lat_lng = a_tag['data-query'].split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        except:
            lat = 0
            lng = 0
        
        return lat,lng

    def get_store_address(self,store):
        
        post_code,phone = 0,0
        state,city,email = '','',''
        address = store.find('p',{'class':'quote-16 bq_header__address'})
        address = address.text.strip() if address is not None else ''
        phone = store.find('p',{'class':'bq_information__contact_item_value'}).text.strip()
        email = store.find_all('p',{'class':'bq_information__contact_item_value'})[1].text.strip()

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code,phone,email

    def get_open_hours(self, store):
    
        opening_hours = []
        working_hours = store.find_all('div',{'class':'bq_information__schedule_day body-16'})
        days = []
        timings = []
        for day_time in working_hours:
            day = day_time.find('div',{'class':'bq_information__schedule_day_section'}).text.strip()
            timing = day_time.find('span',{'class':'bq_information__schedule_time'}).text.strip()
            days.append(day)            
            timings.append(timing)

        for day,timing in zip(days,timings):
            day_open_hours = {}
            day_open_hours['day'] = day
            open_close_hours = timing.split('-')
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        
        return get_open_hours_formatted(opening_hours)