from interfaces.data_source_interface import *
from crud_database.constants import LEVIS_MY_COLUMNS

class LevisMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Levis Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Levis stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LEVIS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Levis stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        soup = get(url,headers=self.headers)
        stores = soup['results']['locations']
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store['name']
        store_id = store['id']
        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)
        return (store_id,title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            
            lat = float(store['loc_lat'])
            lng = float(store['loc_long'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        address = store['streetaddress']
        post_code = 0
        city = ''
        state = ''
        phone = store['phone']
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):
       
        monday = store['monday']
        monday = monday if monday is not None else ''
        tuesday = store['tuesday']
        tuesday = tuesday if tuesday is not None else ''
        wednesday = store['wednesday']
        wednesday = wednesday if wednesday is not None else ''
        thurday = store['thursday']
        thurday = thurday if thurday is not None else ''
        friday = store['friday']
        friday = friday if friday is not None else ''
        saturday = store['saturday']
        saturday = saturday if saturday is not None else ''
        sunday = store['sunday']
        sunday = sunday if sunday is not None else ''
        days = ['sun','mon','tue','wed','thu','fri','sat']
        operating_hours = [sunday,monday,tuesday,wednesday,thurday,friday,saturday]

        opening_hours = []
        for day,timing in zip(days,operating_hours):
            day_open_hours = {}
            open_close_hours = timing.split('-')
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)