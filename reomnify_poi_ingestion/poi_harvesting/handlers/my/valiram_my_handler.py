from interfaces.data_source_interface import *
from crud_database.constants import VALIRAM_MY_COLUMNS

class ValiramMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Valiram Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'http://valiram.com/',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Valiram stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = VALIRAM_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Valiram stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        scripts = soup.find_all('script')
        scripts = [store.text for store in scripts if re.search("maplistScriptParamsKo",store.text)]
        script = str(scripts[0]).split('maplistScriptParamsKo =')[1]
        script_data = (script.replace(';\n/* ]]> */\n','')).strip()     
        data = json.loads(script_data)
        data = data['KOObject']

        stores = data[0]
        stores = stores['locations']
 
        params = self.configuration['params'].split(',')
        print(params)
        brand = params[0]
        country = params[1]

        try:
            stores = list(filter(lambda store: len(store['customCategories']['brand'])>0 and ((store['customCategories']['brand'][0]['name']==brand if (store['customCategories']['brand'][0]['name']==brand) else store['customCategories']['brand'][1]['name']==brand) and store['customCategories']['country'][0]['name']==country),stores))
        except:
            stores = list(filter(lambda store: len(store['customCategories']['country'])==0 and "Malaysia" in str(store) and store['customCategories']['brand'][0]['name']==brand,stores))
        #for store in stores:
            #print(len(store['customCategories']['brand']))

        
        
        logger.info("Total Stores:"+str(len(stores)))

        return stores

    def get_store_details(self, store):
        
        title = store['title']

        lat,lng = self.get_store_geo_coordinates(store)

        store_description = get_bs4_soup_content(store['description'])

        address,city,state,post_code,phone,email = self.get_store_address(store_description)
        
        open_hours = self.get_open_hours(store_description)

        brand = self.configuration['params'].split(',')[0]
        
        return (title,brand,address, city, post_code,phone,email,lat, lng,state,open_hours)

    def get_store_geo_coordinates(self,store):


        lat,lng=0,0
        try:
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat = 0
            lng = 0
        return lat,lng

    def get_open_hours(self, store):
       
        opening_hours = []
        try:
            table = store.find('div',{'class':'map_column_details opening_hour'}).text
            open_close_hours = (table.split('Opening Hour')[1]).split('-')
        except:
            open_close_hours = ''
        days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)





    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''
        phone = store.find('div',{'class':'map_column_details phone'})
        phone = phone.text.split('Phone')[1] if phone is not None else ''
        address = store.find('div',{'class':'map_column_details address'}).text
        address = address.replace('\r','')
        address = address.replace('\n','').split('Address')[1]
        try:
            email = store.find('div',{'class':'map_column_details email'}).text.split('Email')[1]
        except:
            email = ''
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        #map_column_details phone\
        return address,city,state,post_code,phone,email
