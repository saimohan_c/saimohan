from interfaces.data_source_interface import *
from crud_database.constants import INNISFREE_MY_COLUMNS

class InnisfreeMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Innisfree Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Innisfree stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = INNISFREE_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Innisfree stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        stores = get(url,headers=self.headers)
        stores = stores['storeLists']
        
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store['storeNm']

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        try:
            lat = float(store['coor1'])
            lng = float(store['coor2'])
        except:
            lat = 0
            lng = 0
        return lat,lng

    def get_store_address(self,store):
        
        state = store['zip'] if store['zip'] is not None else ''
        city = store['city'] if store['city'] is not None else ''
        address = str(store['addr1'])+str(store['addr2']) if store['addr1'] is not None else ''
        phone = store['phoneNo'] if store['phoneNo'] is not None else ''

        post_code = store['state'] 
        if post_code is not None:
            if post_code.isdigit():
                post_code = post_code
                state = store['state'] if store['zip'] is None else store['zip']
            elif store['zip'] is not None:
                if store['zip'].strip().isdigit():
                    post_code = store['zip']
                    state = store['state'] if store['state'] is not None else ''
                elif store['city'] is not None:
                    if store['city'].strip().isdigit():
                        post_code = store['city']
                        city = ''
            elif store['city'] is not None:
                if ',' not in store['city']:
                    if store['city'].strip().isdigit():
                        post_code = store['city']
                        city = ''
                        state = store['state']
                else:
                    city_split = store['city'].split(',')
                    state = city_split[-1]
                    city = ''
                    post_code = re.findall('[0-9]{5}',store['city'])
                    post_code = post_code[0]    
        else:
            post_code = 0 

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        return address,city,state,post_code,phone

    def get_open_hours(self, store):
    
        opening_hours = []
        working_hours = store['storeHours']
        if ',' not in working_hours:
            open_close_hours = working_hours.split(')')
            normal_days = open_close_hours[0]
            work_time = open_close_hours[1]
            normal_days = normal_days.split('-')
            normal_start_day = get_day_no(normal_days[0].replace("(",''))
            normal_end_day = get_day_no(normal_days[1])
            open_close_hours = work_time.split('-')
            for day in range(normal_start_day,normal_end_day+1):
            
                day_open_hours = {}
                day_open_hours['day'] = get_day_key(day)
                day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)
        else:
            open_close_hours = working_hours.split(',')
            normal_days_timings = open_close_hours[0].split(')')
            normal_days = normal_days_timings[0]
            work_time = normal_days_timings[1]
            normal_days = normal_days.split('-')
            normal_start_day = get_day_no(normal_days[0].replace("(",'').strip())
            normal_end_day = get_day_no(normal_days[1].strip())

            weekends_timings = open_close_hours[1].split(')')
            weekends = weekends_timings[0].strip()
            weekend_days_timings = weekends_timings[1].strip()
            weekend_open_close_hours = weekend_days_timings.split('-')
            weekends_days = weekends.split('&') if '&' in weekends else weekends.split('-')
            weekend_start_day = get_day_no(weekends_days[0].replace('(','').strip())
            weekend_end_day = get_day_no(weekends_days[1].strip())
            open_close_hours = work_time.split('-')

            for day in range(0,normal_end_day+1):
            
                day_open_hours = {}
                day_open_hours['day'] = get_day_key(day)
                day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)
            
            for day in range(weekend_start_day,weekend_end_day+1):
            
                day_open_hours = {}
                day_open_hours['day'] = get_day_key(day)
                day_open_hours['open_time'] = weekend_open_close_hours[0].strip() if len(weekend_open_close_hours)>0 else ''
                day_open_hours['close_time'] = weekend_open_close_hours[1].strip() if len(weekend_open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)
        
        return get_open_hours_formatted(opening_hours)