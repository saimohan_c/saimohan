from interfaces.data_source_interface import *
from crud_database.constants import HABIB_JEWELS_MY_COLUMNS

class HabibJewelsMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Habib Jewels Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://www.habibjewels.com/',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Habib Jewels stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = HABIB_JEWELS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Habib Jewels stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores_div = soup.find('div',{'class':'information-content'})
        stores_p_tags = stores_div.find_all('p')
        stores = list(filter(lambda store:'Operating Hours' in store.text,stores_p_tags))

        logger.info("Total Stores:"+str(len(stores)))

        return stores

    def get_store_details(self, store):
        
        title = store.find('a')
        title = title.text if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):


        lat,lng=0,0
        try:
            u_tags = store.find_all('u')
            u_tags = list(filter(lambda u_tag: 'Go to Showroom' in u_tag.text,u_tags))
            a_tag = u_tags[0]

            href = a_tag.find('a')
            href = href['href']
            response = get(href)
            google_map_link = response.url
            if '%40' in google_map_link:
                lat_lng = google_map_link.split('%40')
                lat_lng = lat_lng[1].split(',')
                lat = float(lat_lng[0])
                lng = float(lat_lng[1])
            elif '@' in google_map_link:
                lat_lng = google_map_link.split('@')
                lat_lng = lat_lng[1].split(',')
                lat = float(lat_lng[0])
                lng = float(lat_lng[1])
        except:
            lat = 0
            lng = 0
        
        return lat,lng

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''
        
        store_address = store.text
        store_complete_address = store_address.split('Operating Hours:') 
        store_address = store_complete_address[0] if store_address is not None else ''

        store_address = store_address.split('\n')[1:]
        store_address = ''.join(store_address)

        phone_address = store_complete_address[1].split('Hotline:')[0] if store_address is not None else ''
        phone = phone_address.split('Telephone:')[1]
        phone = phone.split('Fax:')[0] if "Fax" in phone else phone.strip()
        phone = phone.strip() if phone is not None else ''
        
        address = store_address.replace('\r','')

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone

    def get_open_hours(self, store):
    
        opening_hours = []
        store_page = store.text

        open_close_hours = store_page.split('Operating Hours:')[1] if store_page is not None else ''
        open_close_hours = open_close_hours.split('Telephone')[0].strip() if open_close_hours is not None else ''
        open_close_hours = open_close_hours.replace("\xa0",'')

        if "Ever" in open_close_hours:
            open_close_hours_everyday = open_close_hours.split('from')[1].strip()
            open_close_hours_everyday = open_close_hours_everyday.split('-')
            open_hours = open_close_hours_everyday[0].strip() if len(open_close_hours_everyday)>0 else ''
            close_hours = open_close_hours_everyday[1].strip() if len(open_close_hours_everyday)>1 else '' 
            days = ['sun','mon','tue','wed','thu','fri','sat']

            for day in days:
                day_open_hours = {}
                day_open_hours['day'] = day
                day_open_hours['open_time'] = open_hours
                day_open_hours['close_time'] = close_hours
                opening_hours.append(day_open_hours)

        else:
            open_close_hours_different_days = open_close_hours.split(' (')
            starting_days = open_close_hours_different_days[0]
            weekend_days = open_close_hours_different_days[1]
            
            normal_days_and_timings = starting_days.split('from')
            normal_days = normal_days_and_timings[0]
            normal_days_timings = normal_days_and_timings[1].split('-')
            normal_days = normal_days.replace('(','')
            normal_days = normal_days.replace(')','')
            normal_days_split = normal_days.split('-')
            normal_start_day = get_day_no(normal_days_split[0].strip())
            normal_end_day = get_day_no(normal_days_split[1].strip())

            open_hours = normal_days_timings[0].strip() if len(normal_days_timings)>0 else ''
            close_hours = normal_days_timings[1].strip() if len(normal_days_timings)>1 else ''

            for day in range(normal_start_day,normal_end_day+1):
                day_open_hours = {}
                day_open_hours['day'] = day
                day_open_hours['open_time'] = open_hours
                day_open_hours['close_time'] = close_hours
                opening_hours.append(day_open_hours)

            weekend_days_and_timings = weekend_days.split('from')
            weekends = weekend_days_and_timings[0]
            weekends_timings = normal_days_and_timings[1].split('-')
            weekends = weekends.replace(')','')
            weekends_split = weekends.split('-')
            weekend_start_day = get_day_no(weekends_split[0].strip())
            weekend_end_day = get_day_no(weekends_split[1].strip())

            open_hours = weekends_timings[0].strip() if len(weekends_timings)>0 else ''
            close_hours = weekends_timings[1].strip() if len(weekends_timings)>1 else ''

            for day in range(weekend_start_day,weekend_end_day+1):
                day_open_hours = {}
                day_open_hours['day'] = day
                day_open_hours['open_time'] = open_hours
                day_open_hours['close_time'] = close_hours
                opening_hours.append(day_open_hours)
  
        return get_open_hours_formatted(opening_hours)