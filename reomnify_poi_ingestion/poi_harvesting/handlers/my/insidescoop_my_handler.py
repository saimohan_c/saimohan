from interfaces.data_source_interface import *
from crud_database.constants import INSIDESCOOP_MY_COLUMNS

class InsideScoopMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Inside Scoop Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Inside Scoop stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = INSIDESCOOP_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Inside Scoop stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'location-card'})
        stores_working_hours = soup.find_all('div',{'class':'overlay'})

        new_stores_list = []
        for store,store_work_hours in zip(stores,stores_working_hours):
            new_stores_list.append(str(store)+str(store_work_hours))

        stores = [get_bs4_soup_content(store) for store in new_stores_list]

        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store.find('h3')
        title = title.text.strip() if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        
        directions = store.find_all('a',{'class':'img-btn'})
        directions = directions[1]
        href = directions['href']
        href = get(href)
        href = href.url
        if '@' in str(href):
            lat_lng = href.split('@')[1]
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[0])
            long = lat_lng[1]
            if '%3Fshorturl' in long:
                lng = long.split('%3Fshort')[0]
                lng = float(lng)
            elif '&q=' in long:
                lng = long.split('&q=')[0]
                lng = float(lng)
            else:
                lng = float(long)
        elif '!3d' in str(href):
            lat_lng = href.split('!3d')[1]
            lat_lng = lat_lng.split('!4d')
            lat = float(lat_lng[0])
            long = lat_lng[1]
            if '%3Fshorturl' in long:
                lng = long.split('%3Fshorturl')[0]
                lng = float(lng)
            elif '&q=' in long:
                lng = long.split('&q=')[0]
                lng = float(lng)
            else:
                lng = float(long)
        
        return lat,lng

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''
        address = store.find('p')
        address = address.text.strip() if address is not None else ''
        phone = store.find('a',{'class':'phone-link'})
        phone = phone.text if phone is not None else ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):

        opening_hours = []
        table = store.find('table')
        if table is None:
            return
        rows = table.find_all('tr')
        for row in rows:
            tds = row.find_all('td')
            open_close_hours = tds[1].text.strip()
            open_close_hours = open_close_hours.split('-')
            day_open_hours = {}
            day_open_hours['day'] = tds[0].text.strip() if tds[0] is not None else ''
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        
        return get_open_hours_formatted(opening_hours)
