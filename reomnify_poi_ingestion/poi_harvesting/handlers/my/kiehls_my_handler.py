from interfaces.data_source_interface import *
from crud_database.constants import KIEHLS_MY_COLUMNS

class KiehlsMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Kiehls Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Kiehls stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = KIEHLS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Kiehls stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        
        soup = get(url,headers=self.headers)
        stores = soup['stores']
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        store_id = store['id']
        title = store['name']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)
        return (store_id,title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            
            lat = store['latitude']
            lng = store['longitude']
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        address = store['address1']+store['address2']
        post_code = store['postalCode']
        city = store['city']
        state = store['state']
        phone = store['phone']
        email = store['email']
        return address,city,state,post_code,phone,email
    
    def get_open_hours(self, store):
       
        working_hours = store['hours']
        days_and_timings = working_hours.split(' ')
        opening_hours = []
        days = days_and_timings[0].split('-')
        start_day = get_day_no(days[0].strip())
        end_day = get_day_no(days[1].strip())
        open_close_hours = days_and_timings[1].split('-')
        for day in range(start_day,end_day+1):
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(day)
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)