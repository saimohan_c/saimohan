from interfaces.data_source_interface import *
from crud_database.constants import LOVISA_MY_COLUMNS

class LovisaMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Lovisa Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'accept': '*/*',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Lovisa stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LOVISA_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Lovisa stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        stores = get(url,headers=self.headers)
        stores = stores['stores']
        stores = list(filter(lambda store: (store['country']).lower()=='malaysia',stores))

        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store['shop_name']

        store_id = store['_id']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)

        open_hours = ''

        branch_id = store['branch']

        has_piercing = store['has_piercing']
        
        has_cc = store['has_cc']

        return (store_id,branch_id,title,address, city, post_code,phone,lat,lng,state,open_hours,has_piercing,has_cc)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            
            lat = float(store['lat'])
            lng = float(store['lng'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        post_code = store['zip'] if 'zip' in store.keys() else ''

        post_code = post_code if post_code is not None else 0
        address1 = store['address1'] if 'address1' in store.keys() else ''
        address = address1 if address1 is not None else ''
        address2 = store['address2'] if 'address2' in store.keys() else ''
        if address2 is not None and address2 != '':
            address = address+' '+address2
        state = store['state'] if 'state' in store.keys() else ''
        state = state if state is not None else ''
        phone = store['phone'] if 'phone' in store.keys() else ''
        phone = phone if phone is not None else ''
        city = store['city'] if 'city' in store.keys() else ''
        city = city if city is not None else ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
    