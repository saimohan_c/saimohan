from interfaces.data_source_interface import *
from crud_database.constants import PARIS_MIKI_MY_COLUMNS

class ParisMikiMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Paris Miki Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://paris-miki.com.my/',
   
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path
        
        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Paris Miki stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = PARIS_MIKI_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Paris Miki stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):
        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('div',{'class':'col-md-4 col-sm-6 col-xs-12'})
        logger.info('Total stores: '+str(len(stores)))
        
        return stores

    def get_store_details(self, store):
        
        ul_tags = store.find('ul')
        li_tags = store.find_all('li')
        title = li_tags[0].find('label')
        title = title.text if title is not None else ''
        lat,lng = self.get_store_geo_coordinates(store)
        
        address,city,state,post_code,phone = self.get_store_address(li_tags)
        
        open_hours = self.get_open_hours(li_tags)

        return (title,address, city, post_code,phone,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        
        if 'class="shop-marker"' in str(store):
            a_Tag = store.find('a',{'class':'shop-marker'})
            href = a_Tag['href']
            response = get(href)
            response = response.url

            if '/@' in response:
                lat_lng = response.split('/@')[1]
                lat_lng = lat_lng.split(',')
                lat = float(lat_lng[0])
                lng = float(lat_lng[1])
            elif '/%40' in response:
                lat_lng = response.split('/%40')
                lat = float(lat_lng[0])
                lng = float(lat_lng[1])
            elif '!3d' in response:
                lat_lng = response.split('!2d')
                lat_lng = lat_lng[1].split('!3d')
                lng = float(lat_lng[0])
                lat = lat_lng[1].split('!2m3!')[0] if '!2m3!' in lat_lng[1] else lat_lng[1].split('!3m2!')[0]
                lat = float(lat)

        return lat,lng

    def get_open_hours(self, li_tags):
       
        working_hours = li_tags[4].text.split('Hours:')[1].replace('\n','').strip()
        working_hours = working_hours.split(' | ') if ' | ' in working_hours else [working_hours]
        opening_hours = []
        if len(working_hours)>1:
            normal_days_timings_1 = working_hours[0].split('(')
            normal_days_1 = normal_days_timings_1[1].split('-')
            normal_start_day_1 = get_day_no(normal_days_1[0])
            normal_end_day_1 = get_day_no(normal_days_1[1].replace(')',''))
            normal_days_timings_2 = working_hours[1].split('(')
            normal_days_2 = normal_days_timings_2[1].split('-')
            normal_start_day_2 = get_day_no(normal_days_2[0])
            normal_end_day_2 = get_day_no(normal_days_2[1].replace(')',''))
            if normal_start_day_1 == 7:
                normal_start_day_1 = 0
            for day in range(normal_start_day_1,normal_end_day_1+1):
                day_open_hours = {}
                open_close_hours = normal_days_timings_1[0].split('-')
            
                day_open_hours['day'] = get_day_key(day)
                day_open_hours['open_time'] = open_close_hours[0].strip(' ') if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip(' ') if len(open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)
            for day in range(normal_start_day_2,normal_end_day_2+1):
                day_open_hours = {}
                open_close_hours = normal_days_timings_2[0].split('-')
            
                day_open_hours['day'] = get_day_key(day)
                day_open_hours['open_time'] = open_close_hours[0].strip(' ') if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip(' ') if len(open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)
        else:

            normal_days_timings = working_hours[0].split('(')
            normal_days = normal_days_timings[1].split('-')
            normal_start_day = get_day_no(normal_days[0])
            normal_end_day  = get_day_no(normal_days[1].replace(')',''))

            for day in range(normal_start_day,normal_end_day+1):
                day_open_hours = {}
                open_close_hours = normal_days_timings[0].split('-')
            
                day_open_hours['day'] = get_day_key(day)
                day_open_hours['open_time'] = open_close_hours[0].strip(' ') if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip(' ') if len(open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)

    def get_store_address(self,store):
        
        state = ''
        city = ''

        address = store[1].find('p')
        address = address.text if address is not None else ''

        post_code = 0

        try:
            if re.search(r'[0-9]{5}',address):
                post_code = re.findall(r'[0-9]{5}',address)[0]
            else:
                post_code = 0
        except:
            post_code = 0
       
        phone = store[2].find('a')
        phone = phone.text if phone is not None else ''
        
        return address,city,state,post_code,phone