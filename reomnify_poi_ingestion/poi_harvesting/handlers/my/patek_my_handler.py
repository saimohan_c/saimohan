from interfaces.data_source_interface import *
from crud_database.constants import PATEK_MY_COLUMNS

class PatekMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Patek Phillipe Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
   
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path
        
        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Patek Phillipe stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = PATEK_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Patek Phillipe stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):
        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('div',{'class':'agent'})
        logger.info('Total stores: '+str(len(stores)))
        
        return stores

    def get_store_details(self, store):
        
        
        title = store.find('p',{'class':'name'})
        title = title.text if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)
        
        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = ''

        return (title,address, city, post_code,phone,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        store = store.find('p',{'class':'byGoogle only_google'})
        a_tag = store.find('a')
        response = get(a_tag['href']).url
        response = response.split('&destination=')[1]
        lat_lng = response.split(',')
        lat = float(lat_lng[0])
        longg = lat_lng[1].split('&destination_place')[0]
        longg = longg.split('%20')
        lng = float(longg[1])
        '''if 'class="shop-marker"' in str(store):
            a_Tag = store.find('a',{'class':'shop-marker'})
            href = a_Tag['href']
            response = get(href)
            response = response.url

            if '/@' in response:
                lat_lng = response.split('/@')[1]
                lat_lng = lat_lng.split(',')
                lat = float(lat_lng[0])
                lng = float(lat_lng[1])
            elif '/%40' in response:
                lat_lng = response.split('/%40')
                lat = float(lat_lng[0])
                lng = float(lat_lng[1])
            elif '!3d' in response:
                lat_lng = response.split('!2d')
                lat_lng = lat_lng[1].split('!3d')
                lng = float(lat_lng[0])
                lat = lat_lng[1].split('!2m3!')[0] if '!2m3!' in lat_lng[1] else lat_lng[1].split('!3m2!')[0]
                lat = float(lat)'''

        return lat,lng

    
    def get_store_address(self,store):
        
        state = ''
        city = ''

        address = store.find('p',{'class':'address'})
        address = address.text if address is not None else ''
        p_tags = store.find('p',{'class':'bloc_city'})


        post_code = p_tags.find('span',{'class':'postcode'})
        
        post_code = post_code.text if post_code is not None else 0
        city = p_tags.find('span',{'class':'city'})
        city = city.text if city is not None else ''
       
        phone = store.find('p',{'class':'phone hidden_xs hidden_sm'})
        phone = phone.text.split(':')[1].strip() if phone is not None else ''
        
        return address,city,state,post_code,phone