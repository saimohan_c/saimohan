from interfaces.data_source_interface import *
from crud_database.constants import I_LOVE_YOO_MY_COLUMNS

class ILoveYooMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start I Love Yoo Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load I love Yoo stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = I_LOVE_YOO_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load I Love Yoo stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'col pad3'})
        stores = list(filter(lambda store: 'OTHER COUNTRIES' not in str(store),stores))
        stores = [store.find_all('a') for store in stores]
        stores = [sub_store for store in stores for sub_store in store]

        new_stores_list = []
        origin = self.configuration['origin']

        for store in stores:
            href = store['href']
            soup = get_url_html_content(origin+"/"+href,headers=self.headers)
            store = get_bs4_soup_content(soup)
            sub_stores = store.find_all('div',{'class':'col x123'})
            new_stores_list.extend(sub_stores)

        logger.info("Total stores: "+str(len(new_stores_list)))
        return new_stores_list

    def get_store_details(self, store):

        title = store.find('h4')
        title = title.text.strip() if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = ''
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        try:
            directions = store.find('a',{'class':'button'})
            href = directions['href']
            response = get(href)
            lat_lng = response.url
            if '@' in str(lat_lng):
                lat_lng = lat_lng.split('@')[1]
            else:
                lat_lng = lat_lng.split('/%40')[1]
            lat_lng = lat_lng.split(',')
            lat = lat_lng[0]
            lng = lat_lng[1]
        except:
            lat = 0
            lng = 0
        return lat,lng

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''

        address = store.find('p')
        address = address.text.strip() if address is not None else ''
        address = address.replace("\n","")
        address = address.replace("<br />","")
        address = address.replace("  ","")
        address_split = address.split(',')

        state_address_split = re.split(r'[0-9]',address_split[-1])
        state  = state_address_split[-1].strip() if len(state_address_split)>0 else ''

        phone = ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
