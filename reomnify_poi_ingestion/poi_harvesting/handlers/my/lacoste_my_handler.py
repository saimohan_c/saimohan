from interfaces.data_source_interface import *
from crud_database.constants import LACOSTE_MY_COLUMNS

class LacosteMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Lacoste Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Lacoste stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LACOSTE_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Lacoste stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        
        soup = get(url,headers=self.headers)
        soup = soup['searchList']
        stores = []
        for all_stores in soup:
            sub_city = all_stores['url'].split('/malaysia/')[1].strip()
            params = self.configuration['params'].format(sub_city)
            params = params.format(sub_city)
            new_url = self.configuration['origin']+params
            new_url = get(new_url)
            stores.extend(new_url['stores'])
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store['name']
        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)

        email = store['email']
        return (title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        lat = float(store['latitude'])
        lng = float(store['longitude'])
        return lat,lng

    def get_store_address(self,store):
        address = store['address']
        postal_code = store['postalCode']
        city = store['city']
        state = ''
        phone = store['phone']

        if re.search('[0-9]{5}',postal_code):
            post_code = re.findall('[0-9]{5}',postal_code)
            post_code = post_code[0]

        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):

        working_hours = store['hours']
        if working_hours == None:
            return []
        working_hours = working_hours if working_hours is not None else ''
        working_hours = working_hours.split(':')
        days = working_hours[0].split('-')
        start_day = days[0]
        end_day = days[1] if len(days)>1 else 1
        open_close_hours = working_hours[1].split('-')
        #days = ['sun','mon','tue','wed','thu','fri','sat']
        opening_hours = []
        for day in range(int(start_day),int(end_day)+1):
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(int(day))
            day_open_hours['open_time'] = open_close_hours[0][:2].strip()+':'+open_close_hours[0][2:] if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1][:2].strip()+':'+open_close_hours[1][2:4] if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)