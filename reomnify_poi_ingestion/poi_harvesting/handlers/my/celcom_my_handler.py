from interfaces.data_source_interface import *
from crud_database.constants import CELCOM_MY_COLUMNS

class CelComMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Celcom Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://www.celcom.com.my/',
   
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'CSRF': 'jg1_NU-HZIIGqUVWAoT376rCxZQw_LQiQQcsf1JS0SMjeMUE6rsnAtjkipr0AwC3-Kvi5O_Ulg',

    }     
            

        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params'] 
        params = '' if pd.isna(params) else params

        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Celcom stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = CELCOM_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Celcom stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','state':'region'})

        #df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):
        url = 'https://www.celcom.com.my/getSessionToken'
        token = get(url,headers = self.headers)
        print(token.text)
        return 
        url = self.get_request_url()
        payload = {"SearchText":None,"Region":"all","StoreType":"all","ServiceType":"all","Latitude":3.1022649,"Longitude":101.6400334,"IsUserLocation":False}
        stores = post(url, headers=self.headers,json=payload)
        print(stores.text)
        #stores = stores['']
        logger.info('Total stores: '+str(len(stores)))
        return stores[:20]

    def get_store_details(self, store):
        
        
        #store_id = store['store_id']
        store_id = ''
        title = store['name']

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code = self.get_store_address(store)
        

        open_hours = ''

        #open_hours = self.get_open_hours(open_hours)

        return (store_id,title,lat, lng, address, city,state, post_code,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng=0,0

        try:
            lat = float(store['Latitude'])
            lng = float(store['Longitude'])
        except:
            lat,lng =0,0

        return lat,lng

    def get_open_hours(self, open_hours):
        days = open_hours.keys()

        opening_hours = []

        for day in days:
            day_open_hours = {}
            open_close_hrs = open_hours[day]

            if type(open_close_hrs) == bool or open_close_hrs == False:
                open_hrs = CLOSED
                close_hrs = CLOSED
            else:
                open_hrs = open_close_hrs['open']
                close_hrs = open_close_hrs['close']

            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_hrs
            day_open_hours['close_time'] = close_hrs
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)

    def get_store_address(self,store):
        
        address=store['data']
        address = store['StoreList']
        address = get_bs4_soup_content(address).find('Address').text
        address = address.replace(', Australia','')

        address_split = address.split(' ')
        print(address_split)
        post_code = address_split[-1]
        if post_code.isdigit() == True:
            state = address_split[-2]
        else:
            state = address_split[-1]
            post_code=0
        state = get_state_code(state)
        city = ''        
        return address,city,state,post_code