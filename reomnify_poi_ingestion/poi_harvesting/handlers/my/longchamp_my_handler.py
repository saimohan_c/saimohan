from interfaces.data_source_interface import *
from crud_database.constants import LONGCHAMP_MY_COLUMNS

class LongChampMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Long Champ Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'www.longchamp.com',
    'accept': '*/*',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
    'cookie': 'dwac_5b0179b2850c1ad48476327fb0=IvM0-bb9lZa-PnljwwgvbLB8CNIhGVslUlc%3D|dw-only|||TWD|false|Asia%2FTaipei|true; cqcid=aby4vLBzodb2jrYapS6zp0DMko; cquid=||; sid=IvM0-bb9lZa-PnljwwgvbLB8CNIhGVslUlc; lc_geoloc="{\"lg\":\"en\",\"co\":\"MY\",\"lc\":\"en_MY\"}"; dwanonymous_237726e9c2696e6a79dfb3b5d18e5371=aby4vLBzodb2jrYapS6zp0DMko; __cq_dnt=0; dw_dnt=0; dwsid=p8YFogZilNo2z7hcCm0tUzVJOYmRhlpp2qYCdmjTp3h0si093E3yLcneWFBx8NFxaifCFyFUm75C0QIdVJR-hg==; _gcl_au=1.1.2101999079.1641891603; dw_cookie_choices={"dw":true,"rejectCookies":false,"rejectExempt":false,"accepted":true,"functionnal":true,"analytics":true,"social":true,"experience":true,"adversiting":true}; __cq_uuid=adVavbo6lrygxK7s0h0gGVLDeL; __cq_seg=0~0.00!1~0.00!2~0.00!3~0.00!4~0.00!5~0.00!6~0.00!7~0.00!8~0.00!9~0.00; atuserid=%7B%22name%22%3A%22atuserid%22%2C%22val%22%3A%228a0847b7-69ec-4f26-9569-9d609014fd9f%22%2C%22options%22%3A%7B%22end%22%3A%222023-02-12T09%3A00%3A05.803Z%22%2C%22path%22%3A%22%2F%22%7D%7D; atidvisitor=%7B%22name%22%3A%22atidvisitor%22%2C%22val%22%3A%7B%22vrn%22%3A%22-615651--517879-%22%7D%2C%22options%22%3A%7B%22path%22%3A%22%2F%22%2C%22session%22%3A15724800%2C%22end%22%3A15724800%7D%7D; atauthority=%7B%22name%22%3A%22atauthority%22%2C%22val%22%3A%7B%22authority_name%22%3A%22default%22%2C%22visitor_mode%22%3A%22optin%22%7D%2C%22options%22%3A%7B%22end%22%3A%222023-02-12T09%3A00%3A06.104Z%22%2C%22path%22%3A%22%2F%22%7D%7D; _ga=GA1.2.2122091656.1641891647; _gid=GA1.2.542853222.1641891647; _fbp=fb.1.1641891649018.1240045765; pageviewCount=5; _gat_UA-34609757-1=1',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Long Champ stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LONGCHAMP_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Long Champ stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        stores = get(url,headers=self.headers)
        stores = stores['stores']
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store['name']
        store_id = store['ID']
        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)

        return (store_id,title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        address = store['address1']+" "+store['address2']
        post_code = store['postalCode']
        city = store['city']
        state = ''
        phone = store['phone']
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):
       
        working_hours = store['storeHours']

        opening_hours = []
        for day_timing in working_hours:
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(day_timing['day'])
            day_open_hours['open_time'] = day_timing['openingTime']
            day_open_hours['close_time'] = day_timing['closingtime']
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)