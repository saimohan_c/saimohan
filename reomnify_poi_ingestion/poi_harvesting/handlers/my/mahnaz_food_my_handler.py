from interfaces.data_source_interface import *
from crud_database.constants import MAHNAZ_FOOD_MY_COLUMNS

class MahnazFoodMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Mahnaz Food Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'accept': '*/*',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Mahnaz Food stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = MAHNAZ_FOOD_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Mahnaz Food stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('div',{'class':'wpb_text_column wpb_content_element'})
        stores = list(filter(lambda store:'Directions' in str(store.text),stores))
        logger.info("Total stores: "+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store.find('span')
        title = title.text if title is not None else ''
        
        lat,lng = self.get_store_geo_coordinates(store)

        p_tag = store.find('p')
        p_tag = p_tag.span.next_sibling.text
        p_tag = p_tag.split('Call Us:')  if 'Call' in p_tag else p_tag.split('Contact Us:')

        phone_hours = p_tag[1].split('Week:') if 'Week:' in p_tag[1] else p_tag[1].split('Hours:')
        phone = phone_hours[0].strip()

        address,city,state,post_code = self.get_store_address(p_tag[0])        
        open_hours = self.get_open_hours(phone_hours[1:])

        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            a_tag = store.find('a',{'target':'_blank'})
            href = a_tag['href']
            response = get(href)
            response = response.url
            if '/@' in response:
                lat_lng = response.split('/@')[1]
            elif '/%40' in response:
                lat_lng = response.split('/%40')[1]
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        post_code = ''
        state = ''
        city = ''
        address = store.strip()

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code
    
    def get_open_hours(self, store):

        if len(store) == 1:
            days = ['sun','mon','tue','wed','thu','fri','sat']
            opening_hours = []
            for day in days:
                day_open_hours = {}
                day_open_hours['day'] = day
                open_close_hours = store[0].split('–') if '–' in store[0] else store[0].split('-')
                day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)
            return get_open_hours_formatted(opening_hours)
        else:
            day_timings = []
            for day_time in store:
                day_timings.append(day_time.split('Hours:'))
            opening_hours = []
            for day_time_list in day_timings:
                if 'to' in day_time_list[0]:
                    normal_days = day_time_list[0].strip().split('to')
                    normal_days_timings = day_time_list[1].split('–')
                    normal_day_start = get_day_no(normal_days[0].strip())
                    normal_day_end = get_day_no(normal_days[1].strip())
                    for day in range(normal_day_start,normal_day_end+1):
                        day_open_hours = {}
                        day_open_hours['day'] = get_day_key(day)
                        day_open_hours['open_time'] = normal_days_timings[0].strip() if len(normal_days_timings)>0 else ''
                        day_open_hours['close_time'] = normal_days_timings[1].strip() if len(normal_days_timings)>1 else ''
                        opening_hours.append(day_open_hours)
                   
                else:
                    weekends = day_time_list[0].split('And')
                    weekends_timings = day_time_list[1].split('–') 
                    weekend_start = get_day_no(weekends[0].strip())
                    weekend_end = get_day_no(weekends[1].strip())
                    for day in range(weekend_start,weekend_end+1):
                        day_open_hours = {}
                        day_open_hours['day'] = get_day_key(day)
                        day_open_hours['open_time'] = weekends_timings[0].strip() if len(weekends_timings)>0 else ''
                        day_open_hours['close_time'] = weekends_timings[1].strip() if len(weekends_timings)>1 else ''
                        opening_hours.append(day_open_hours)
            
            return get_open_hours_formatted(opening_hours)
    