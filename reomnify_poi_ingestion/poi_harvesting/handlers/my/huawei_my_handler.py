from interfaces.data_source_interface import *
from crud_database.constants import HUAWEI_MY_COLUMNS

class HuaweiMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Huawei Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Huawei stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = HUAWEI_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Huawei stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'recommend-list-cnt'})

        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):
                
        a_tag = store.find('a',{'class':'a-common'})
        href = a_tag['href']
        origin = self.configuration['origin']
        store = origin+href
        store = get_url_html_content(store,headers = self.headers)
        store = get_bs4_soup_content(store)

        title = store.find('div',{'class':'retail-store-title'})
        title = title.text.strip() if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        try:
            directions = store.find('a',{'title':'View store map'})
            href = directions['href']
            lat_lng = href.split('?&d=')[1]
            lat_lng = lat_lng.split('&s=')[0]
            lat_lng = lat_lng.split(',')
            lat = lat_lng[0]
            lng = lat_lng[1]
        except:
            lat = 0
            lng = 0
        return lat,lng

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''
        div_tags = store.find_all('div',{'class':'retail-cnt-detail'})
        address = div_tags[0].find('p').text.strip() if div_tags[0] is not None else ''
        phone = div_tags[2].text.strip() if div_tags[2] is not None else ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone

    def get_open_hours(self, store):
    
        opening_hours = []
        working_hours_list = store.find('li',{'class':'retail-workhours'})
        working_hours = working_hours_list.find('div',{'class':'retail-cnt-detail'})
        open_close_hours = working_hours.find('p')
        open_close_hours = open_close_hours.text.strip() if open_close_hours is not None else working_hours.text.strip()
        open_close_hours = open_close_hours.split('-')
        days = ['sun','mon','tue','wed','thu','fri','sat']

        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        
        return get_open_hours_formatted(opening_hours)