from interfaces.data_source_interface import *
from crud_database.constants import MARKS_AND_SPENCERS_MY_COLUMNS

class MarksAndSpencersMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Marks and Spencers Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'authority': 'www.marksandspencer.com',
    'cache-control': 'max-age=0',
    'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'upgrade-insecure-requests': '1',
    'sec-ch-ua-mobile': '?0',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'same-origin',
    'sec-fetch-dest': 'empty',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
    'cookie': '"MS_ORIGIN_COUNTRY=IN; ak_bmsc=DB2BAC73E341D819E8877F35B2D697B7~000000000000000000000000000000~YAAQTnMsMfcd9HB+AQAAXIoecQ4U0081tHnZM9rY1x9qHvo+gljdwgIanO6nLuGc82RC9kGj1VOlvPZGLIttkpVk5+99PDaErQjFTXW432qG8doK35/VVmk28aCXoyJJdTVn/3o4tM2GalnK5KLNSbLck5qaIqijSW7ZYZdiaM6jO6W/aM48FZBUBZq6SfwsLDch1V13m+tsFhWdlKTq5Yn4uSlRxkc4YGd45UrqnIQzguYGNa99yWmAL5RHolSDAro+g5BbyhGzrmlEv6WiyINemJGobDrFcDNUejViB8rzQnnvpdtzJ2sVI9jK4x/Lk9zGKJLnmMqzBGUiqIPHLeeoXJM7p0xf7O+4mad9HTH2Sop/ImPQGHmAJGeTER/t8/FLaVKLEB54TPqX7xvtFS/W2ww=; dwac_beCiaiaageDcEaaadmFn2OboyO=5d0hbrEbJyuh03n8bB5VFzZCKyCpzopiVog%3D|dw-only|||GBP|false|Europe%2FLondon|true; cqcid=ab67Zgpz2Y8HrFcRZmHe3ULpY8; cquid=||; dwpersonalization_1cb58b269223fbc31a670f29b400d0c0=46903622feca3fbdc7b1c0837320220209234500000; GlobalE_Data=%7B%22countryISO%22%3A%22MY%22%2C%22cultureCode%22%3A%22ms%22%2C%22currencyCode%22%3A%22MYR%22%2C%22apiVersion%22%3A%222.1.4%22%7D; dwanonymous_1cb58b269223fbc31a670f29b400d0c0=ab67Zgpz2Y8HrFcRZmHe3ULpY8; sid=5d0hbrEbJyuh03n8bB5VFzZCKyCpzopiVog; __cq_dnt=0; dw_dnt=0; dwsid=DFzdedh3damcg5JLFPL5eN_D1zfAotjga9hJqi9u6LDgox3PebNXTcUZ-EIlA_buV-1VBp2s8CWIQIFsviRi0Q==; GlobalE_CT_Data=%7B%22CUID%22%3A%22154529632.304893390.91%22%2C%22CHKCUID%22%3Anull%7D; GlobalE_Ref=https%3A//www.google.com/; lux_uid=164257533679518603; AMCVS_1E4022CE527845D10A490D4D%40AdobeOrg=1; s_ecid=MCMID%7C18160284182584255812409679499362247799; AMCV_1E4022CE527845D10A490D4D%40AdobeOrg=1406116232%7CMCIDTS%7C19012%7CMCMID%7C18160284182584255812409679499362247799%7CMCAAMLH-1643180137%7C12%7CMCAAMB-1643180137%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1642582537s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C2.5.0; s_cc=true; __cq_uuid=adVavbo6lrygxK7s0h0gGVLDeL; __cq_seg=0~0.00!1~0.00!2~0.00!3~0.00!4~0.00!5~0.00!6~0.00!7~0.00!8~0.00!9~0.00; GlobalE_Full_Redirect=false; s_sq=%5B%5BB%5D%5D; MS_PREFERRED_COUNTRY=en_MY; utag_main=v_id:017e711e949400911d92e5191ee80306900230610086e{_sn:1$_se:7$_ss:0$_st:1642577214778$ses_id:1642575336599%3Bexp-session$_pn:4%3Bexp-session$vapi_domain:marksandspencer.com;} s_plt=6.41; s_pltp=my%3Aweb%3Astores; akavpau_INTL_PROD_WAITING=1642576028~id=60da34f8adcc2f6b09168cf7043f3313; bm_sv=445FE887D6FA7CC5FEC6196ACE9069F9~elcgwiZSqH8qIRTwhxa51g9ARP94SWF/Uh6GIY7K8tPRuJeviO74pD6gAkuS1i761qYV16NcqsT+rtKITFv7ILt50jXHLwcNW40VPaadCErlX5e1VIsDEn9w3zOB5wpBVSTWwFOjUlTkoW1Q2VPXo2rIltG0OeBfTRJCDgvGeJE=; RT=\"z=1&dm=marksandspencer.com&si=90e63192-307f-45e3-a540-1b4224299752&ss=kyl6xz4j&sl=4&tt=e99&bcn=%2F%2F684d0d43.akstat.io%2F&ld=1rm9&nu=9y8m6cy&cl=1v5h&ul=211o\"',
}
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Marks and Spencers stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = MARKS_AND_SPENCERS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Marks and Spencers stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        soup = soup.find('ul',{'class':'store-listing'})
        stores = soup.find_all('li')

        logger.info("Total stores: "+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store.find('span',{'class':'store-name'})
        title = title.text if title is not None else ''

        product_cat = ''
        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)

        open_hours = self.get_open_hours(store)

        all_divs = store.find_all('div',{'class':'store-day-time'})
        working_hours = []

        for div in all_divs:

            working_hours.append(div.find_all('span'))

        working_hours = list(filter(lambda work_hour:len(work_hour)==1,working_hours))
        products = ''
        for div in working_hours:
            category = div[0].text
            products = category+','+products
        product_cat = products.strip()

        return (title,address, city, post_code,phone,lat,lng,state,open_hours,product_cat)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        try:
            map_button = store.find('a',{'class':'store-map'})
            href = map_button['href']
            response = get(href)
            response = response.url
            if 'addr%3D' in response:
                lat_lng  = response.split('addr%3D')[1]
            elif 'addr=' in response:
                lat_lng  = response.split('addr=')[1]
            lat_lng = lat_lng.split(',')

            lat  = float(lat_lng[0])
            lng = lat_lng[1].split('&q=')[0]
            lng = float(lng)
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        a_Tag = store.find('a',{'class':'store-map'})
        post_code = 0
        state = ''
        city = ''
        address = a_Tag.text.strip() if a_Tag is not None else ''
        phone = store.find('a',{'class':'storelocator-phone'})
        phone = phone.text.strip() if phone is not None else ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):

        all_divs = store.find_all('div',{'class':'store-day-time'})
        working_hours = []
        for div in all_divs:
            working_hours.append(div.find_all('span'))
        working_hours = list(filter(lambda work_hour:len(work_hour)>1,working_hours))
        opening_hours = []
        for span in working_hours:
            day = span[0].text.replace(':','')
            open_close_hours = span[1].text.split('-')
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)