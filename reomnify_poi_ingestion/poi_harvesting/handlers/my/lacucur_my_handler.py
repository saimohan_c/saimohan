from interfaces.data_source_interface import *
from crud_database.constants import LACUCUR_MY_COLUMNS

class LacucurMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Lacucur Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Lacucur stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LACUCUR_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Lacucur stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('div',{'class':'wpb_column pos-top pos-center align_center column_child col-lg-4 single-internal-gutter'})
        stores_details = soup.find_all('div',{'class':'uncode_text_column'})
        stores_details_p_tags = []
        for store_timing_contact in stores_details:
            p_tag = store_timing_contact.find('p')
            stores_details_p_tags.append(p_tag)

        stores_details_p_tags = stores_details_p_tags[1:]
        stores = [[store,stores_details_p_tags] for store in stores]

        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        store_title = store[0]
        title = store_title.find('h2',{'class':'h5'})
        title = title.find('span')
        title = title.text if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store[0])
        address,city,state,post_code,phone,email = self.get_store_address(store)        
        open_hours = self.get_open_hours(store[1][0])

        return (title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            a_tag = store.find('a',{'class':'custom-link btn border-width-0 btn-color-xsdn btn-outline btn-icon-left'})
            href = a_tag['href']
            response = get(href)
            href = response.url
            if '/@' in href:
                lat_lng = href.split('/@')[1]
            else:
                lat_lng = href.split('%40')[1]
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        address = store[0].find('div',{'class':'text-top-reduced'})
        address = address.find('p')
        address = address.text.replace('\n','') if address is not None else ''
        post_code = 0
        city = ''
        state = ''
        phone_email = store[1][1].text
        phone_email = phone_email.split('\n')
        phone = phone_email[0]
        email = phone_email[1]

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code,phone,email
    
    def get_open_hours(self, store):
       
        working_hours = store.text.split('\n')[0]
        working_hours = working_hours.split('Outlets –')
        days = ['sun','mon','tue','wed','thu','fri','sat']
        open_close_hours = working_hours[1].split('to')
        opening_hours = []
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)