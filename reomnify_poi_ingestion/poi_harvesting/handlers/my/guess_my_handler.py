from interfaces.data_source_interface import *
from crud_database.constants import GUESS_MY_COLUMNS

class GuessMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Guess Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://www.guess.my',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Guess stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = GUESS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Guess stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'stores-each grid__item small--one-whole medium-up--one-quarter'})
        
        
        logger.info("Total Stores:"+str(len(stores)))

        return stores

    def get_store_details(self, store):
        
        title = store.find('h4',{'class':'h4'}).text
        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = ''
        
        #GUESS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','lng','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):


        lat,lng=0,0
        
        return lat,lng

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''
        
        p_tags = store.find_all('p')

        address = p_tags[0].text

        phone = p_tags[1].text.split('Fax:')[0]
        phone = phone.split('Tel:')[1].replace('\t\n','').strip()

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code,phone
