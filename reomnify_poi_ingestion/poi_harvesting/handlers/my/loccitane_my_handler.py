from interfaces.data_source_interface import *
from crud_database.constants import LOCCITANE_MY_COLUMNS

class LoccitaneMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Loccitane Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Loccitane stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LOCCITANE_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load LOccitane stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        soup = get(url,headers=self.headers)
        stores = soup['storeList']['store']
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store['Name']
        store_id = store['storeId']
        store_code = store['StoreCode']
        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)
        category,channel,store_type,pickInStore,ShipToStore,isRetailer,isPushedOnline = self.store_remaining_details(store)
        #LOCCITANE_MY_COLUMNS = ['store_id','store_code','name','address','city','post_code','phone','lat','long','state','open_hours','category','channel','store_type','PickInStore','ShipToStore','isRetailer','isPushedOnline','ro_place_id','ds_instance_id','timestamp','created_at']
        return (store_id,store_code,title,address, city, post_code,phone,lat,lng,state,open_hours,category,channel,store_type,pickInStore,ShipToStore,isRetailer,isPushedOnline)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        store = store['coord']
        try:
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def store_remaining_details(self,store):

        category = str(store['Category'])
        channel = str(store['Channel'])
        store_type = store['Type']
        pickInStore = str(store['PickInStore'])
        ShipToStore = str(store['ShipToStore'])
        isRetailer = str(store['isRetailer'])
        isPushedOnline = str(store['isPushedOnline'])

        return category,channel,store_type,pickInStore,ShipToStore,isRetailer,isPushedOnline

    def get_store_address(self,store):

        address = store['Address1']
        address = address if address is not None else ''
        post_code = store['ZipCode']
        post_code = post_code if post_code is not None else 0
        city = store['City']
        state = store['State']
        phone = store['Phone']
        phone = phone if phone is not None else ''
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]

        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):
       
        working_hours = store['RegularOpeningHours']

        opening_hours = []
        for day_timing in working_hours:
            day_open_hours = {}
            day_open_hours['day'] = get_day_key(day_timing['Day'])
            day_open_hours['open_time'] = day_timing['OpenTime']
            day_open_hours['close_time'] = day_timing['CloseTime']
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)