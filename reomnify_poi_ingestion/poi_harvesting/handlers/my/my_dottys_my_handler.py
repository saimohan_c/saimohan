from interfaces.data_source_interface import *
from crud_database.constants import MY_DOTTYS_MY_COLUMNS

class MyDottysMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start My Dotty's Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'http://www.mydottys.com/',
   
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params']
        params = '' if pd.isna(params) else params

        
        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load My Dotty's stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = MY_DOTTYS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load My Dotty's stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):
        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('div',{'class':['bdr-brown p-2','bdr-brown-new p-2']})
        
        return stores

    def get_store_details(self, store):
        
        title = store.find('div',{'class':'branch-1 tx-brown'}).text
        title += store.find('div',{'class':'branch-2 tx-brown'}).text
        lat,lng = self.get_store_geo_coordinates(store)
        href_page = store.find('a',{'class':'bg-branch branch-link'})
        href = href_page['href']
        new_url = self.get_request_url() + href
        
        store_address_page = get_url_html_content(new_url,headers = self.headers)
        store_address_page = get_bs4_soup_content(store_address_page)
        address,city,state,post_code,phone = self.get_store_address(store_address_page)
        
        open_hours = self.get_open_hours(store_address_page)

        return (title,address, city, post_code,phone,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        return lat,lng

    def get_open_hours(self, store_address_page):
       
        table = store_address_page.find('table',{'class':'table mt-5 text-left'})
        opening_hours = []
        rows = table.find_all('tr')
        for row in rows:
            cells = row.find_all('td')
            day_open_hours = {}
            day = cells[0].text
            open_close_hours = cells[1].text
            open_close_hours = open_close_hours.split('-')
            
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip(' ') if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip(' ') if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)

    def get_store_address(self,store_address_page):
        
        address = store_address_page.find('div',{'class':'col-md-10 text-center'}).text

        address_split = address.split(',')
        address = address[10:]
        
        try:
            if re.search(r'[0-9]{5}',address_split[-2]):
                post_code = re.findall(r'[0-9]{5}',address_split[-2])[0]
            else:
                post_code = 0
        except:
            post_code = 0
        
        state = re.findall(r'[A-Z].*',address_split[-2])[0]
        state = re.sub(' \d+','',state)
        
        
        city = address_split[-3].strip()
        try:
            phone = address_split[-1].split(':')
            phone = phone[1].strip()
        except:
            phone = 0
        
        return address,city,state,post_code,phone