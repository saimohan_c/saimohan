from interfaces.data_source_interface import *
from crud_database.constants import FRED_PERRY_MY_COLUMNS

class FredPerryMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Fred Perry Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://www.fredperry.com/',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',}     
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params']
        params = '' if pd.isna(params) else params
        
        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Fred Perry stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = FRED_PERRY_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Fred Perry stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'store'})

        return stores

    def get_store_details(self, store):
        
        title = store.find('h3')
        title = title.text if title is not None else ''
        
        lat,lng = self.get_store_geo_coordinates(store)
        a_tag = store.find('a',{'class':'visit-store'})
        href = a_tag['href']
        logger.info(href)
        store_link = get_url_html_content(href,headers=self.headers)
        store_link = get_bs4_soup_content(store_link)
        
        address,city,state,post_code,phone = self.get_store_address(store_link)
        
        open_hours = self.get_open_hours(store_link)
        
        return (title,address,city,post_code,phone,lat,lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        try:
            lat = store['data-lat']
            lng = store['data-long']
        except:
            lat,lng = 0,0
        
        return lat,lng

    def get_open_hours(self, store_page):
        
        opening_hours = []
        table = store_page.find('div',{'class':'table'})

        if table is None:
            return 
            
        rows = table.find_all('div',{'class':'row'})

        for row in rows:
            day = row.find('div',{'class':'subtitle'}).text

            open_close_hours = row.find('div',{'class':'value'}).text
            open_close_hours = open_close_hours.split('-')

            open_hours = open_close_hours[0].strip() if len(open_close_hours)>0 else CLOSED
            close_hours = open_close_hours[1].strip() if len(open_close_hours)>1 else CLOSED

            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_hours
            day_open_hours['close_time'] = close_hours
            opening_hours.append(day_open_hours)
        
        return get_open_hours_formatted(opening_hours)

    def get_store_address(self,store_link):
        
        #store = self.get_store_individual_page_address(store)
        state = ''
        post_code =0

        address = (store_link.find('address').text).strip()
        address = address.replace('\n',',')

        phone = (store_link.find('div',{'class':'telephone'}).text).strip()
        phone = phone[9:].strip()
        
        #splitting the address
        address_split = address.split(',')
       
        city = address_split[-2]
        
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone