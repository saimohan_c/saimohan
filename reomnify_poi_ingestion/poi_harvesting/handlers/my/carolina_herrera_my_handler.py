from interfaces.data_source_interface import *
from crud_database.constants import CAROLINA_HERRERA_MY_COLUMNS

class CarolinaHerreraMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Carolina Herrera Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://carolinaherreraboutiques.com/en/api/stores?lat=4.210484&lng=101.975766',
   
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
  
    }     
            

        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params']
        params = '' if pd.isna(params) else params

        
        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Carolina Herrera stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = CAROLINA_HERRERA_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Carolina Herrera stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"title", 'address':'address_1',
        'post_code':'postal_code'})

        #df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):
        url = self.get_request_url()
        page = 1
        all_stores = []
        
        while True:
            try:
                formatted_url = url.format(page)
                logger.info(formatted_url)
                stores = get(formatted_url, headers=self.headers)
                
                stores = stores['data']
                rows = stores['rows']
                
                all_stores.extend(rows)
                
                '''if len(all_stores) > 0:
                    break'''
                if page == stores['pager']['total_pages']:
                    break
                page += 1
            except:
                logger.error(traceback.print_exc())
        all_stores = list(filter(lambda store:store['country_code']=='Malaysia',all_stores))
        logger.info('Total stores: '+str(len(all_stores)))
        return all_stores

    def get_store_details(self, store):
        
        
        store_id = store['store_id']
        
        title = store['store_name']

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,country = self.get_store_address(store)
    

        open_hours = ''

        #open_hours = self.get_open_hours(open_hours)
        #CAROLINA_HERRERA_MY_COLUMNS = ['store_id', 'name','address', 'city', 'post_code','country','lat', 'long', 'state', 'open_hours', 'ro_place_id', 'ds_instance_id', 'timestamp', 'created_at']

        return (store_id,title,address, city, post_code,country,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0

        try:
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat,lng =0,0

        return lat,lng

    def get_open_hours(self, open_hours):
        days = open_hours.keys()

        opening_hours = []

        for day in days:
            day_open_hours = {}
            open_close_hrs = open_hours[day]

            if type(open_close_hrs) == bool or open_close_hrs == False:
                open_hrs = CLOSED
                close_hrs = CLOSED
            else:
                open_hrs = open_close_hrs['sun_from_time']
                close_hrs = open_close_hrs['sun_to_time']

            #day_open_hours['day'] = day
            day_open_hours['open_time'] = open_hrs
            day_open_hours['close_time'] = close_hrs
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)

    def get_store_address(self,store):
        
        address=store['address_1']+store['address_2']
        try:
            post_code = int(store['postal_code'].replace('-','')) if '-' in store['postal_code'] else int(store['postal_code'])
        except:
            post_code = 0
        state = store['state']
        city = store['city']
        country = store['country_code']
        return address,city,state,post_code,country