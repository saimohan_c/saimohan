from interfaces.data_source_interface import *
from crud_database.constants import PRADA_MY_COLUMNS

class PradaMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Prada Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
   
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path
        
        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Prada stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = PRADA_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Prada stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores_divs = soup.find_all('div',{'class':'d-none'})
        stores_divs = [store for store in stores_divs if 'malaysia' in str(store)]
        stores_divs = stores_divs[0].find_all('a')
        stores_divs = list(filter(lambda store:'malaysia' in str(store),stores_divs))

        stores = []

        for store in stores_divs:
            
            html_content = get_url_html_content(store['href'],headers=self.headers)
            soup = get_bs4_soup_content(html_content)
            script = soup.find('script',{'id':'jsonldLocalBusiness'}).text.strip()
            stores.append(json.loads(script))

        logger.info('Total stores: '+str(len(stores)))
        
        return stores

    def get_store_details(self, store):
        
        
        title = store['name']
        print(title)

        lat,lng = self.get_store_geo_coordinates(store)
        
        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = self.get_open_hours(store)

        return (title,address, city, post_code,phone,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        store = store['geo']
        try:
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat = 0
            lng = 0
        return lat,lng

    
    def get_store_address(self,store):
        
        store_address = store['address']
        state = ''
        address = store_address['streetAddress']

        city = store_address['addressLocality']
        
        post_code = store_address['postalCode']
               
        phone = store['telephone']
        
        return address,city,state,post_code,phone

    def get_open_hours(self, store):
       
        working_hours = store['openingHours']
        working_hours = working_hours.strip().split(' pm ')
        opening_hours = []

        for day_time in working_hours:
            day_time = day_time.split('day')
            day_open_hours = {}
            open_close_hours = day_time[1].split(' - ')
            day_open_hours['day'] = day_time[0].strip()
            
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
            close_time = open_close_hours[1].strip() if len(open_close_hours)>0 else ''
            close_time = close_time+'pm' if 'pm' not in open_close_hours[1].strip() else open_close_hours[1].strip()
            day_open_hours['close_time'] = close_time 
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)
