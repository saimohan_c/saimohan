from interfaces.data_source_interface import *
from crud_database.constants import FUEL_SHACK_MY_COLUMNS

class FuelShackMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Fuel Shack Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://fuelshack.com.my',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params']
        params = '' if pd.isna(params) else params

        
        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Fuel Shack stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = FUEL_SHACK_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Fuel Shack stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'loc_det'})
        print("Total Store:",len(stores))
        return stores

    def get_store_details(self, store):
        
        title = store.find('h5')
        title = title.text if title is not None else ''
        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        try:
            a_tag = store.find('a',{'target':'_blank'})
            href = a_tag['href']
            response = get(href)
            google_map_link = response.url
            if '/@' in google_map_link:
                lat_lng = google_map_link.split('/@')
            else:
                lat_lng = google_map_link.split('%40')
            lat_lng = lat_lng[1].split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        except:
            lat = 0
            lng = 0
        
        return lat,lng

    def get_open_hours(self, store_page):
        
        opening_hours = []
        
        table = store_page.find('p',{'class':'time'}).text
        if '&' not in table:
            open_close_hours = table.split('-')
            open_hours = open_close_hours[0].strip() if len(open_close_hours)>0 else CLOSED
            close_hours = open_close_hours[1].strip() if len(open_close_hours)>1 else CLOSED
            days = ['mon','tue','wed','thu','fri','sat','sun']
            for day in days:
                day_open_hours = {}
                day_open_hours['day'] = day
                day_open_hours['open_time'] = open_hours.replace('pm','am')
                day_open_hours['close_time'] = close_hours
                opening_hours.append(day_open_hours)
                
        else:
            normal_days = table.split('&')
            
            normal_days_timings = normal_days[0].split('(')
            weekends_timings = normal_days[1].split('(')
            open_close_hours = normal_days_timings[0].split('-')
            open_hours = open_close_hours[0].strip() if len(open_close_hours)>0 else CLOSED
            close_hours = open_close_hours[1].strip() if len(open_close_hours)>1 else CLOSED
            normal_days = ['mon','tue','wed','thu']
            weekends = ['fri','sat','sun']
            weekends_open_close_hours = weekends_timings[0].split('-')
            weekends_open_hours = weekends_open_close_hours[0].strip() if len(weekends_open_close_hours)>0 else CLOSED
            weekends_close_hours = weekends_open_close_hours[1].strip() if len(weekends_open_close_hours)>1 else CLOSED
            for day in normal_days:
                day_open_hours = {}
                day_open_hours['day'] = day
                day_open_hours['open_time'] = open_hours
                day_open_hours['close_time'] = close_hours
                opening_hours.append(day_open_hours)

            for day in weekends:
                day_open_hours = {}
                day_open_hours['day'] = day
                day_open_hours['open_time'] = weekends_open_hours
                day_open_hours['close_time'] = weekends_close_hours
                opening_hours.append(day_open_hours)
            
        return get_open_hours_formatted(opening_hours)

    def get_store_address(self,store):
        
        post_code =0
        city = ''

        address = store.find('a',{'target':'_blank'})
        address = address.text if address is not None else ''

        phone = store.find('a',{'class':'tele'})
        phone = phone.text if phone is not None else ''
        phone = (phone.split(':')[1]).strip()

        address_split = address.split(',')

        if re.search('[0-9]{5}',address_split[-1]):
            state_split = re.split(r'[0-9]{5}',address_split[-1])
            state = state_split[-1]
        else:
            state = address_split[-1]
        
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
