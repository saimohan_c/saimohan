from interfaces.data_source_interface import *
from crud_database.constants import LAMER_MY_COLUMNS

class LamerMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Lamer Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
         'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:95.0) Gecko/20100101 Firefox/95.0',
    'Accept': '/',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    'Referer': 'https://www.lamer.com.my/stores',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-Requested-With': 'XMLHttpRequest',
    'Origin': 'https://www.lamer.com.my',
    'Connection': 'keep-alive',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
    'TE': 'trailers',
}
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin+path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Lamer stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LAMER_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Lamer stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        data = {
            'JSONRPC': '[{"method":"locator.doorsandevents","id":2,"params":[{"fields":"DOOR_ID, DOORNAME, EVENT_NAME, EVENT_START_DATE, EVENT_END_DATE, EVENT_IMAGE, EVENT_FEATURES, EVENT_TIMES, SERVICES, STORE_HOURS, ADDRESS, ADDRESS2, STATE_OR_PROVINCE, CITY, REGION, COUNTRY, ZIP_OR_POSTAL, PHONE1, STORE_TYPE, LONGITUDE, LATITUDE","radius":"0","country":"Malaysia","latitude":3.139003,"longitude":101.686855,"region_id":"33","language_id":"48"}]}]'
        }
        #payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"JSONRPC\"\r\n\r\n[{\"method\":\"locator.doorsandevents\",\"id\":2,\"params\":[{\"fields\":\"DOOR_ID, DOORNAME, EVENT_NAME, EVENT_START_DATE, EVENT_END_DATE, EVENT_IMAGE, EVENT_FEATURES, EVENT_TIMES, SERVICES, STORE_HOURS, ADDRESS, ADDRESS2, STATE_OR_PROVINCE, CITY, REGION, COUNTRY, ZIP_OR_POSTAL, PHONE1, STORE_TYPE, LONGITUDE, LATITUDE\",\"radius\":\"0\",\"country\":\"Malaysia\",\"latitude\":3.139003,\"longitude\":101.686855,\"region_id\":\"33\",\"language_id\":\"48\"}]}]\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
        #params = 'dbgmethod=locator.doorsandevents'
        html_content = post(url,headers=self.headers,data=data)
        print(html_content)
        stores = []
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = store['name']

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        open_hours = self.get_open_hours(store)
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            
            lat = float(store['lat'])
            lng = float(store['lng'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        address = store['address']
        post_code = store['postal']
        city = store['city']
        state = ''
        phone = store['phone']

        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):
       
        working_hours = store['hours1']
        days_and_timings = working_hours.split(': ')
        days = days_and_timings[0].split('-')
        start_day = get_day_no(days[0].strip())
        end_day = get_day_no(days[1].strip())
        open_close_hours = days_and_timings[1].split('–')
        open_close_hours = open_close_hours if len(open_close_hours)==2 else open_close_hours[0].split('-')
        print(days)
        print(open_close_hours)
        opening_hours = []
        for day in range(start_day,end_day+1):
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)