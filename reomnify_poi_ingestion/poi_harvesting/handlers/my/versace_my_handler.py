from interfaces.data_source_interface import *
from crud_database.constants import VERSACE_MY_COLUMNS

class VersaceMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Versace Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'http://boutiques.versace.com',
        'accept': 'application/json',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Versace stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = VERSACE_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Versace stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        stores = get(url,headers=self.headers)
        stores= stores['response']['entities']
        logger.info("Total Stores:"+str(len(stores)))

        return stores

    def get_store_details(self, store):
        
        title = store['profile']['c_internalName']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email = self.get_store_address(store)
        
        open_hours = self.get_open_hours(store)

        #open_hours = ''
        store_type = store['profile']['c_storeType']
        
        return (title,address, city, post_code,phone,email,lat, lng,state,open_hours,store_type)

    def get_store_geo_coordinates(self,store):


        lat,lng=0,0
        
        lat = float(store['profile']['yextDisplayCoordinate']['lat'])
        lng = float(store['profile']['yextDisplayCoordinate']['long'])
        
        return lat,lng

    def get_open_hours(self, store):
       
        working_hours = store['profile']['hours']['normalHours']
        opening_hours = []
        for day_time in working_hours:
            day_open_hours = {}
            day_open_hours['day'] = day_time['day']
            day_open_hours['open_time'] = str(day_time['intervals'][0]['start'])[:2]+':00'
            day_open_hours['close_time'] = str(day_time['intervals'][0]['end'])[:2]+':00'
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)





    def get_store_address(self,store):
        store_address = store['profile']['address']
        post_code = store_address['postalCode']
        city = store_address['city']
        state = store_address['region'] if store_address['region']!=None else ''
        phone = store['profile']['mainPhone']['number']
        address = store_address['line1']+store_address['line2']
        if store_address['line3']!=None:
            address = address+store_address['line3']
        email = store['profile']['emails'][0]
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        #map_column_details phone\
        return address,city,state,post_code,phone,email
