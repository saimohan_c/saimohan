from interfaces.data_source_interface import *
from crud_database.constants import GIORDANO_MY_COLUMNS

class GiordanoMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Giordano Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://giordanoappsite.giordano.com/AppSiteMY/SVC/AppsFunc.svc/rest/GetShopsMap?shopID=&style=&market=MY&langid=EN&longitude=0&latitude=0&callback=json',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        } 

        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        parameters = self.configuration['params']
        parameters = '' if pd.isna(parameters) else parameters

        url = origin + path + parameters

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Giordano stores in to the table")

        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = GIORDANO_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Giordano stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'state':'region', 'address':'street_address',
        'post_code':'postal_code', 'phone': 'phone_number'})
        
        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)
    
    def get_stores(self):
        url = self.get_request_url()
        
        stores_html_content = get_url_html_content(url,headers=self.headers)
        stores = get_bs4_soup_content(stores_html_content)
    
        stores = str(stores)
        stores = stores.replace('json(','')
        stores = stores.replace(');','')
        stores = json.loads(stores)
        stores = stores['GetShopsMapResult']

        return stores

    def get_store_details(self, store):
        
        store_id = store['ShopID']

        title = store['Name']

        lat,lng = self.get_store_geo_coordinates(store)
        
        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = ''

        return (store_id, title, address, city, post_code, phone, lat, lng, state, open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng=0,0

        try:
            lat = float(store['Latitude'])
            lng = float(store['Longitude'])
        except:
            lat,lng =0,0

        return lat,lng
    
    def get_store_address(self,store):
        
        post_code =0
        city = ''

        address = store['Address']
        phone = store['Tel']
        state = ''
        
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
