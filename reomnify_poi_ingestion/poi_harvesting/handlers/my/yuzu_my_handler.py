from interfaces.data_source_interface import *
from crud_database.constants import YUZU_MY_COLUMNS

class YuzuMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Yuzu Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {   
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path
        
        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Yuzu stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = YUZU_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Yuzu stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):
        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'id':'contactContent'})

        logger.info('Total stores: '+str(len(stores)))
        
        return stores

    def get_store_details(self, store):

        table = store.find('table',{'class':'contact'})
        rows = table.find_all('tr')
        title = 'Yuzu Japanese Restaurant'
        lat,lng = self.get_store_geo_coordinates(store)
        
        address,city,state,post_code,phone,email = self.get_store_address(rows)
        open_hours = self.get_open_hours(rows)
        

        return (title,address, city, post_code,phone,email,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        try:
            mapbutton = store.find('div',{'class':'contactMap'})
            a_tag = mapbutton.find('a',{'target':'_blank'})
            href = a_tag['href']
            response = get(href)
            response = response.url
            if '/@' in response:
                lat_lng = response.split('/@')[1]
                lat_lng = lat_lng.split(',')
                lat = float(lat_lng[0])
                lng = float(lat_lng[1])
            elif '/%40' in response:
                lat_lng = response.split('/%40')
                lat = float(lat_lng[0])
                lng = float(lat_lng[1])
            elif '!3d' in response:
                lat_lng = response.split('!2d')
                lat_lng = lat_lng[1].split('!3d')
                lng = float(lat_lng[0])
                lat = lat_lng[1].split('!2m3!')[0] if '!2m3!' in lat_lng[1] else lat_lng[1].split('!3m2!')[0]
                lat = float(lat)
        except:
            lat,lng =0,0
        return lat,lng

    def get_store_address(self,store):
        
        state = ''
        city = ''

        store_address = store[0]
        address = store_address.find('td').text.strip()

        post_code = 0

        try:
            if re.search(r'[0-9]{5}',address):
                post_code = re.findall(r'[0-9]{5}',address)[0]
            else:
                post_code = 0
        except:
            post_code = 0
       
        phone = store[1].find('td').text.strip()
        email = store[3].find('td').text.strip()

        return address,city,state,post_code,phone,email

    def get_open_hours(self, store):
       
        opening_hours = []
        open_close_hours = store[4].find('td').text.strip()
        open_close_hours = open_close_hours.split('-')
        days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)