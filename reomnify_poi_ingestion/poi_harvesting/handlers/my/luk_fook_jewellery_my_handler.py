from interfaces.data_source_interface import *
from crud_database.constants import LUK_FOOK_JEWELLERY_MY_COLUMNS

class LukFookJewelleryMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Luk Fook Jewellery Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'accept': '*/*',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Luk Fook Jewellery stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LUK_FOOK_JEWELLERY_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Luk Fook Jewellery stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        stores = get(url,headers=self.headers)
        logger.info("Total stores: "+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = 'Luk Fook Jewellery '+store['Code_EN']

        store_id = store['ID']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)

        open_hours = self.get_open_hours(store)

        return (store_id,title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            lat_lng = store['XY']
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[1])
            lng = float(lat_lng[0])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        post_code = 0
        state = ''
        city = ''
        address = store['Shop_EN']
        phone = store['T']
        phone = phone if phone is not None else ''

        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):

        hours = store['B_EN']
        days = ['sun','mon','tue','wed','thu','fri','sat']
        open_close_hours = hours.split('–')
        opening_hours = []
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)