from interfaces.data_source_interface import *
from crud_database.constants import CHILIS_MY_COLUMNS

class ChilisMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Chilis Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://chilis.com.my',
   
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params']
        params = '' if pd.isna(params) else params

        
        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Chilis stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = CHILIS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Chilis stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):
        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('div',{'class':'outlet-accordion-item'})
        return stores

    def get_store_details(self, store):
        
        title = store.find('div',attrs={'class':'outlet-accordion-item-head'})
        title = title.text if title is not None else ''
        title = title.replace('\n','')
        title = title.lstrip(' ')
        
        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code = self.get_store_address(store)
        
        open_hours = ''
        #open_hours = self.get_open_hours(open_hours)
        return (title,address, city, post_code,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0

        try:
            map_button = store.find('a',{'class':'button-link'})
            href = map_button['href']
            if '@' not in href:
                response = get(href)
                href = response.url
            
            lat_lng = href.split('@')
            lat_lng = lat_lng[1].split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        except:
            lat,lng =0,0
            

        return lat,lng

    def get_open_hours(self, open_hours):
        days = open_hours.keys()

        '''opening_hours = []
        weekdays = ['Monday','Tuesday','Wednesday','Thurday','Friday']
        weekends = ['Saturday']
        daily = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']'''



        for day in days:
            day_open_hours = {}
            open_close_hrs = open_hours[day]

            if type(open_close_hrs) == bool or open_close_hrs == False:
                open_hrs = CLOSED
                close_hrs = CLOSED
            else:
                
                open_hrs = open_close_hrs['sun_from_time']
                close_hrs = open_close_hrs['sun_to_time']

            #day_open_hours['day'] = day
            day_open_hours['open_time'] = open_hrs
            day_open_hours['close_time'] = close_hrs
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)

    def get_store_address(self,store):
        
        address = store.find('div',attrs={'class':'outlet-accordion-item-content'}).text
        adds = address.replace('\n','')
        new_add = adds
        adsplit = adds.split('Show on map')
        address = adsplit[0]
        address = address.lstrip(' ')
        new_add = new_add.replace(' ','')
        adsplit = new_add.split('Showonmap')
        new_add = adsplit[0]
        ad_split = new_add.split(',')
        
        try:
            if re.search('[0-9]{5}',ad_split[-1][:5]):
                post_code = re.findall('[0-9]{5}',ad_split[-1][:5])
                post_code = post_code[0]
            elif re.search('[0-9]{5}',ad_split[-2][:5]):
                post_code = re.findall('[0-9]{5}',ad_split[-2][:5])
                post_code = post_code[0]
            else:
                post_code = re.findall('[0-9]{5}',ad_split[-3][:5])
                post_code = post_code[0]
        except:
            post_code = 0
        
        state = ''
        city = ''
        
        return address,city,state,post_code