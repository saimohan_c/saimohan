from interfaces.data_source_interface import *
from crud_database.constants import GLO_LASER_CENTERS_MY_COLUMNS

class GloLaserCentersMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Glo Laser Centers Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://www.glolasercentres.com/',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }     
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params']
        params = '' if pd.isna(params) else params
        
        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Glo Laser Centers stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = GLO_LASER_CENTERS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Glo Laser Centers stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'wpb_column vc_column_container vc_col-sm-3'})
        stores = list(filter(lambda store: 'get directions' in str(store).lower(),stores))

        logger.info("Total Stores:"+str(len(stores)))

        return stores

    def get_store_details(self, store):
        
        title = store.find('h3')
        title = title.text if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = self.get_open_hours(store)
        
        return (title,address, city, post_code,phone,lat, lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        a_tag = store.find('a',{'rel':'noopener noreferrer'})
        href = a_tag['href']
        response = get(href)
        lat_lng = response.url

        lat_lng = lat_lng.split('@')
        lat_lng = lat_lng[1].split(',')

        lat = float(lat_lng[0])
        lng = lat_lng[1]
        
        return lat,lng

    def get_store_address(self,store):
        
        post_code =0
        city = ''
        state = ''
        phone = []
        p_tags = store.find('p')

        tel_numbers = p_tags.find_all('a')
        
        for tel in tel_numbers:
            tel = tel.text
            phone.append(tel.strip()) if tel is not None else phone.append(None)
        
        address_split = (p_tags.text).replace('\n','')
        address_split = address_split.split('T:')
        address = address_split[0]        
        
        return address,city,state,post_code,phone
