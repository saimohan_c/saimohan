from interfaces.data_source_interface import *
from crud_database.constants import FLASH_GADGETS_MY_COLUMNS

class FlashGadgetsMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Flash Gadgets Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'authority': 'https://www.flashgadgets.com.my',
   
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
    }     
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        params = self.configuration['params']
        params = '' if pd.isna(params) else params

        
        url = origin + path + params

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Flash Gadgets stores in to the table")
        
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = FLASH_GADGETS_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Flash Gadgets stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find('div',{'class':'info_mc'})
        stores_string_formatted = str(stores)
        
        stores_string_formatted = re.split('<p><br/></p>',stores_string_formatted)
        stores_string_formatted = [store.split('<p style="text-align: left;"><br/></p>') for store in stores_string_formatted]
        
        stores_string_formatted = [store.strip() for item in stores_string_formatted for store in item]
        stores_string_formatted = list(filter(lambda store: 'View On Maps' in store,stores_string_formatted))
        
        all_stores = stores_string_formatted[1:8]
        stores = [get_bs4_soup_content(html) for html in stores_string_formatted]
        logger.info('Total Stores: '+str(len(stores)))
    
        return stores

    def get_store_details(self, store):
        
        title = store.find_all('p')[0].text
        
        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = ''
        #open_hours = self.get_open_hours(open_hours)
        #MY_DOTTYS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours', 'ro_place_id', 'ds_instance_id', 'timestamp', 'created_at']
        
        return (title,address, city, post_code,phone,lat, lng,state,open_hours)


    def get_store_geo_coordinates(self,store):

        lat,lng=0,0
        try:
            map_button = store.find('a',{'target':'_blank'})
            href = map_button['href']
            response = get(href)
            href = response.url
            print(href)
            if '/@' not in href:
                lat_lng = href.split('%40')
            else:
                lat_lng = href.split('/@')
            lat_lng = lat_lng[1].split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])
        except:
            lat,lng = 0,0
        
        return lat,lng

    

    def get_store_address(self,store):
        
        
        try:
            p_tags = store.find_all('p')

            address = p_tags[1].text

            phone = p_tags[2].text
            phone = phone.split(' : ')[1]

            post_code = 0

            if re.search('[0-9]{5}',address):
                post_code = re.findall('[0-9]{5}',address)
                post_code = post_code[0]

            address_split = address.split(',')
        
            if re.search('[0-9]{5}',address_split[-1].strip()):
                city_split = address_split[-1].strip().split(" ")
                city = city_split[-2]+" "+city_split[-1]
               
            else:
                city = address_split[-1].strip()
            
        except:
            post_code = 0
            city = ''
        
        state = ''
        
        return address,city,state,post_code,phone