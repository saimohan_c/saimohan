from interfaces.data_source_interface import *
from crud_database.constants import LEGO_MY_COLUMNS

class LegoMyDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Lego Malaysia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            'Accept': '*/*',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate, br',
            'Referer': 'https://www.lego.com/en-my/stores/directory',
            'authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InB1YiI6e30sInB2dCI6IjdUTlBNbnVJcml5cittVnNEKzhjRjdGczVPV2tEblIvc09YZkN2LzZsWS8zM0ppUUoyd3U0Vm44RVJHMzY5SW1HVGVtS3hBTEU2Z1cvQ1RDdFFHQlhJQ2hweWYrNTI3VlVCdU8vMmNyNFVwMEpseTlPTmRGUWF5b0VkSEhuL21NVFJTelVEVXJIdUovNUFBek84clNhdW1kbVFZVEkvR2FrU2JZd2VzRVlCejVpTDFWWCtKbzVVMWNIak9LM1pRbTBBc21hd01lMmFEeC9VZDJyUS9oQWtmQzlvdjdQamdVR0E2VGR0Z1FOdUkvbmVNVFBQVUNOR1BrbVJ3YzB5M3FOdjRIN1Q1NHBJT21aMTRMNUtJclhWeXI5OHBKYU4yMHZROGZ3eXpCaGlmMFhGd3VqbDdES1RXK2xMTTNOZEhRWTVaQy9OL0pBcnJKYmsvbFVYOXV2UTA3QUdZYjZtTEhuQ1BhaUpQamowZ1FlMVNrTCttRXk5cXR5R1pZRGoreVpSVCt4THJUL2dIMjROUmxTNVhpbEVZZlg5UjRNWEhwUjhkWnp6Y1pVaDV1dGZuSGtpaFB0TXprdnlGNzFDbHhnNitpNG5PYzVaczloNmE4QUNaLy9DRm43Mmc2VmZESzAveit0bXVIekw1MlBBVjZUSHBHUkVSaFM2cERMMXV4cllEL3grdU1xcXpKRmNScUxiMWxJeGo3NGl1MzdVM0duNXErcU5CWU0rUHVyMFF5ZEpQK2xSbjhScEJpemcrMy5UL3QzZVNiM1JxRlIvcXd6TitFOUFRPT0ifSwiaWF0IjoxNjQxODE1MjA4LCJleHAiOjE2NDE4MjYwMDh9.mzrxwqsAKSJLLqbv1QsceQdGbmO6F_ZqDYGhQzrqDDM',
            'content-type': 'application/json',
            'features': 'rewardsCenter=false',
            'lid': '',
            'newrelic': 'eyJ2IjpbMCwxXSwiZCI6eyJ0eSI6IkJyb3dzZXIiLCJhYyI6IjE3NDY4NzEiLCJhcCI6IjEwMzI0NzQ2OCIsImlkIjoiZGRiM2E3NTQxZGIyYWQ3YyIsInRyIjoiZmJjY2UyOTNhY2M4NGFlOTBiZjNhNGFhMGJiMTVmZDAiLCJ0aSI6MTY0MTgxNTI3MjgxMX19',
            'session-cookie-id': 'irLyznnPQeUn-4cgncExZ',
            'traceparent': '00-fbcce293acc84ae90bf3a4aa0bb15fd0-ddb3a7541db2ad7c-01',
            'tracestate': '1746871@nr=0-1-1746871-103247468-ddb3a7541db2ad7c----1641815272811',
            'visitor-guid': '56d44658-22b4-4707-aece-e19f5eee526d',
            'x-lego-request-id': '0b80165c-abf6-4cd1-ac32-9d39d83882d9-shop-c',
            'x-locale': 'en-MY',
            'Origin': 'https://www.lego.com',
            'Connection': 'keep-alive',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'same-origin',
            'Sec-Fetch-Site': 'same-origin',
            'TE': 'trailers',
        }
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Lego stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = LEGO_MY_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Lego stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):
        url = self.get_request_url()
        data = '{"operationName":"StoresDirectory","variables":{},"query":"query StoresDirectory {\\n storesDirectory {\\n id\\n country\\n region\\n stores {\\n storeId\\n name\\n phone\\n state\\n phone\\n openingDate\\n certified\\n additionalInfo\\n storeUrl\\n urlKey\\n isNewStore\\n isComingSoon\\n _typename\\n }\\n _typename\\n }\\n}\\n"}'

        html_content = post(url,headers=self.headers,data=data)
        print(html_content)
        stores = []
        logger.info("Total stores: "+str(len(stores)))
        return stores

    def get_store_details(self, store):

        title = ''

        lat,lng = self.get_store_geo_coordinates(store)
        address,city,state,post_code,phone = self.get_store_address(store)        
        #open_hours = self.get_open_hours(store)
        open_hours = ''
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            
            lat = float(store['lat'])
            lng = float(store['lng'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        address = ''
        post_code = ''
        city = ''
        state = ''
        phone = ''

        return address,city,state,post_code,phone
    
    def get_open_hours(self, store):
       
        working_hours = store['hours1']
        days_and_timings = working_hours.split(': ')
        days = days_and_timings[0].split('-')
        start_day = get_day_no(days[0].strip())
        end_day = get_day_no(days[1].strip())
        open_close_hours = days_and_timings[1].split('–')
        open_close_hours = open_close_hours if len(open_close_hours)==2 else open_close_hours[0].split('-')
        print(days)
        print(open_close_hours)
        opening_hours = []
        for day in range(start_day,end_day+1):
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)
        return get_open_hours_formatted(opening_hours)