from interfaces.data_source_interface import *
from crud_database.constants import BODY_CATALYST_AU_COLUMNS

class BodyCatalystAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Body Catalyst Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'accept': '*/*',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Body Catalyst stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = BODY_CATALYST_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Body Catalyst stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        stores = soup.find_all('div',{'class':'col-12 duck-bg white-fg m-0 p-4'})
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        a_links = store.find_all('a',{'class':'btn-secondary'})
        a_links = list(filter(lambda store: store.text == 'Tell Me More',a_links))
        store_page_link = a_links[0]['href']
        html_content = get_url_html_content(store_page_link,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        
        store = soup.find('script',{'id':'wpsl-js-js-extra'}).text.strip()
        store = store.split('"locations":')[1]
        store = store.split('};')[0]
        store = store.replace(']','')
        store = store.replace('[','')
        store = json.loads(store)
        
        title = store['store']
        store_id = store['id']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code = self.get_store_address(store)

        open_hours = ''
        a_tags = soup.find_all('a',{'class':'black-fg'})
        all_div_tags = soup.find_all('div',{'class':'item-split'})
        email_tags = list(filter(lambda email_t:'Email' in str(email_t),all_div_tags))
        
        phone = a_tags[0].text.strip() if len(a_tags)>0 else ''
        email = email_tags[0].text.split('Email')[1].strip()
        return (store_id,title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            lat = store['lat'].replace('-','')
            lat = float(lat)
            lng = float(store['lng'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = store['zip']
        state = store['state']
        city = store['city']
        address = store['address']+' '+store['address2']+' '+store['city']+store['city']
        
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code




'''open_hours_details = list(filter(lambda open_hr:'Hours' in str(open_hr),all_div_tags))
        open_hours_details = open_hours_details[0].find('p')
        working_hours = open_hours_details.text.split('\r\n')
        print(working_hours)
        #open_hours = self.get_open_hours(open_hours_details)
    def get_open_hours(self, store):
       

        working_hours = store.text
        #working_hours = working_hours.replace('<br/>','')
        working_hours = working_hours.split('<br/>')
        print(working_hours)
        opening_hours = []
        open_close_hours = store[4].find('td').text.strip()
        open_close_hours = open_close_hours.split('-')
        days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)'''