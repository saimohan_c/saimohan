from interfaces.data_source_interface import *
from crud_database.constants import MY_DAISO_JAPAN_AU_COLUMNS

class MyDaisoJapanAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start My Daiso Japan Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load My Daiso Japan stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = MY_DAISO_JAPAN_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load My Daisp Japan  stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        stores = get(url,headers=self.headers)

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):


        title = store['store']
        store_id = store['id']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email,fax = self.get_store_address(store)

        open_hours = self.get_open_hours(store)

        return (store_id,title,address, city, post_code,phone,email,fax,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        
        try: 
            lat = float(store['lat'])
            lng = float(store['lng'])

        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = store['zip']
        city = store['city']
        state = get_state_code(store['state'])
        address = store['address']
        phone = store['phone']
        email = store['email']
        fax = store['fax']

        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
 
        return address,city,state,post_code,phone,email,fax


    def get_open_hours(self, store):

        working_hours = store['hours']
        soup = get_bs4_soup_content(working_hours)
        trs = soup.find_all('tr')
        
        opening_hours = []

        for day in trs:
            day_open_hours = {}
            tds = day.find_all('td')
            day_open_hours['day'] = tds[0].text.strip()
            open_close_hours = tds[1].text.strip().split('-')
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>0 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)


        