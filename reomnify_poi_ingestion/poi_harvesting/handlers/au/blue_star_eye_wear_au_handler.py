from interfaces.data_source_interface import *
from crud_database.constants import BLUE_STAR_EYE_WEAR_AU_COLUMNS

class BlueStarEyeWearAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Blue Star Eye Wear Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Blue Star Eye Wear stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = BLUE_STAR_EYE_WEAR_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Blue Star Eye Wear stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers = self.headers)
        soup = get_bs4_soup_content(html_content)
        
        stores = soup.find_all('div',{'class':'card mb-2'})

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store.find('h2').text.strip()

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email = self.get_store_address(store)

        #open_hours = self.get_open_hours(store)
        open_hours = ''

        return (title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        store = store.find('div',{'class':'row'})
        try: 
            div_tag = store.find('div',{'class':'col-12 col-sm-4'})
            a_tag = div_tag.find('a')
            href = a_tag['href']
            lat_lng = href.split('/@')[1]
            lat_lng = lat_lng.split(',')
            lat = lat_lng[0].strip()
            lat = float(lat)
            lng = lat_lng[1].strip()
            lng = float(lng)

        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        store_address = store.find('address')
        p_tags = store_address.find_all('p')

        post_code = 0
        city = ''
        state = ''
        address = p_tags[0].text.strip()
        address = address.replace('\r\n',' ')
        address = address.replace('\n',' ')

        address_split = address.split(',')
        
        state = address_split[-2].strip().split(' ')[0]

        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]

        contacts = p_tags[1].text.strip()
        contacts = contacts.split(' ')
        phone = contacts[0].strip()
        email = contacts[1].strip()

        return address,city,state,post_code,phone,email


    def get_open_hours(self, store):
        
        opening_hours = []

        working_hours = store.find('div',{'class':'amlocator-schedule-table'})
        working_hours = working_hours.find_all('div',{'class':'amlocator-row'})

        for day in working_hours:
            day_open_hours = {}
            day_open_hours['day'] = day.find('span',{'class':'amlocator-cell -day'}).text
            open_close_hours = day.find('span',{'class':'amlocator-cell -time'}).text.split('-')
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else CLOSED
            opening_hours.append(day_open_hours)


        return get_open_hours_formatted(opening_hours)


        