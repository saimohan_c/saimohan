from interfaces.data_source_interface import *
from crud_database.constants import GAMI_CHICKEN_AU_COLUMNS

class GamiChickenAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Gami Chicken Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
            'accept': 'text/plain, */*; q=0.01',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
            'cookie': 'ASP.NET_SessionId=uojz3t0kruarjo31h3qel3h5; ARRAffinity=8deff527c0ababa089921a5affcc4f13bf273f884d161fe9945c0a609330b02e; ARRAffinitySameSite=8deff527c0ababa089921a5affcc4f13bf273f884d161fe9945c0a609330b02e; _ga=GA1.3.606122432.1645682083; _gid=GA1.3.890752376.1645682083; _hjFirstSeen=1; _hjIncludedInSessionSample=0; _hjSession_2344431=eyJpZCI6ImE2ZjZiYzU0LTcwNWYtNGY1Zi04MDAzLTYzNDE4OWNkNThkNCIsImNyZWF0ZWQiOjE2NDU2ODIwODQyNjYsImluU2FtcGxlIjpmYWxzZX0=; _hjIncludedInPageviewSample=1; _hjAbsoluteSessionInProgress=1; _fbp=fb.2.1645682084558.122942917; _hjSessionUser_2344431=eyJpZCI6IjRhYjQ3YjAxLWViYjEtNTAzMS1iY2EwLTI5ODJkNDVhMWY2MyIsImNyZWF0ZWQiOjE2NDU2ODIwODMxNTAsImV4aXN0aW5nIjp0cnVlfQ==',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Degani stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = GAMI_CHICKEN_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Gami Chicken stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        locations_div = soup.find('div',{'class':'in-center'})
        a_tags = locations_div.find_all('a')
        locations = [a['onclick'].strip().split("fecthLocation('")[1][:-2].replace(' ','%20') for a in a_tags]
        params = self.configuration['params']+'/'

        stores = []
        
        for loc in locations:
            url = self.configuration['origin']+params+loc
            stores_data = post(url,headers=self.headers)
            stores.extend(stores_data['data'])
        #print(stores)
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store['title']
        store_id = store['id']
        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)
        open_hours = self.get_open_hours(store)
        slug = store['slug']
        
        return (store_id,title,address, city, post_code,phone,lat,lng,state,open_hours,slug)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        
        try: 
            lat = float(store['latitude'])
            lng = float(store['longitude'])

        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = store['zipcode']
        city = store['city'] if 'city' in store.keys() else ''
        state = store['state']
        
        phone = store['phoneNumber']
        address = store['address']

        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
 
        return address,city,state,post_code,phone

    def get_open_hours(self, store):
       
        working_hours = store['openOnDays']
        working_hours = working_hours.split(',')
        #working_hours = working_hours[:-2] if len(working_hours)>7 else working_hours
        working_hours = [workhours for workhours in working_hours if 'DAY' in workhours]

        opening_hours = []

        for day in working_hours:

            day_open_hours = {}
            day_time = day.split('DAY') if 'DAY' in day else day.split('day')

            day_open_hours['day'] = day_time[0].strip()
            open_close_hours = day_time[1].strip()

            if '/' in open_close_hours:
                open_close_hours = open_close_hours.split('/')
                for dual_hours in open_close_hours:
                    open_close_hours = dual_hours.split('-')
                    day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
                    day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
                    opening_hours.append(day_open_hours)
            
            elif 'and' in open_close_hours:
                open_close_hours = open_close_hours.split('and')
                for dual_hours in open_close_hours:
                    open_close_hours = dual_hours.split('to')
                    day_open_hours['open_time'] = open_close_hours[0].strip().replace('(','') if len(open_close_hours)>0 else ''
                    day_open_hours['close_time'] = open_close_hours[1].strip().replace(')','') if len(open_close_hours)>1 else ''
                    opening_hours.append(day_open_hours)
            
            elif 'to' in working_hours and 'and' not in working_hours:
                open_close_hours = open_close_hours.split('to')
                day_open_hours['open_time'] = open_close_hours[0].strip().replace('(','') if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip().replace(')','') if len(open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)
            
            else:
                open_close_hours = open_close_hours.split('-')
                day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
                opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours) 