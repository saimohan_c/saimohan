from interfaces.data_source_interface import *
from crud_database.constants import RED_ROOSTER_AU_COLUMNS

class RedRoosterAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Red Rooster australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        

        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        parameters = self.configuration['params']
        parameters = '' if pd.isna(parameters) else parameters

        url = origin + path + parameters

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Red Rooster stores in to the table")


        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = RED_ROOSTER_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Red Rooster stores in to the intermediate poi table")
        
        
        df = df.rename(columns={'title':"location_name", 'state':'region', 'address':'street_address',
        'post_code':'postal_code', 'phone': 'phone_number'})
        
        category = get_poi_category('take-away')

        df['top_category'] = category['top_category']
        df['sub_category'] = category['sub_category']

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin'].split(";")[0]
        
        return df

    def execute(self):

        url = self.get_request_url()

        headers = {
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
            "content-type": "application/json"
        }

        stores = get(url, headers=headers)

        self.load_source_data(stores)

    def get_store_details(self, store):

        post_code = store['postcode']

        if post_code.isdigit() == False:
            post_code = 0

        store_id = store['id']
        store_no = store['storeNumber']

        title = store['title'].title()

        lat,lng = self.get_store_geo_coordinates(store)
        
        address = store['address']
        state = get_state_code(store['state'])
        city = store['suburb'].title()
        phone = store['phone']
        slug = store['slug']
        catering = store['catering']
        delivery = store['delivery']
        catering_pickup = store['catering_pickup']
        catering_delivery = store['catering_delivery']
        drive_thru = store['driveThru']
        delivery_aggregators = str(store['deliveryAggregators'])
        catering_towncity = str(store['catering-towncity'])
        catering_post_code = str(store['catering-postcode'])
        catering_url = store['catering_url']
        gsn = store['gsn']
        gsn = gsn if str(gsn).isnumeric() == False else 0

        open_hours = store['opening_hours']

        open_hours = self.get_open_hours(open_hours)

        return (store_id, store_no, title, slug, catering, delivery, catering_pickup,
                       catering_delivery, drive_thru, delivery_aggregators, catering_towncity, catering_post_code,
                       catering_url, gsn, address, city, post_code, phone, lat, lng, state,
                       open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng=0,0

        try:
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat,lng =0,0

        return lat,lng

    def get_open_hours(self, open_hours):
        days = open_hours.keys()

        opening_hours = []

        for day in days:
            day_open_hours = {}
            open_close_hrs = open_hours[day]

            if type(open_close_hrs) == bool or open_close_hrs == False:
                open_hrs = CLOSED
                close_hrs = CLOSED
            else:
                open_hrs = open_close_hrs['open']
                close_hrs = open_close_hrs['close']

            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_hrs
            day_open_hours['close_time'] = close_hrs
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)