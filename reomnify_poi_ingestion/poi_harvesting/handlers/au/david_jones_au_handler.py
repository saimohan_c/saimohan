from interfaces.data_source_interface import *
from crud_database.constants import DAVID_JONES_AU_COLUMNS

class DavidJonesAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start David Jones Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
        'accept': '*/*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
        'cookie': 'ASP.NET_SessionId=od1alrsl021m3c3gd2cadisd; visid_incap_1686039=TKlQ1q16QjO736Yh0Kg0pYM+E2IAAAAAQUIPAAAAAACWH5pjBdYhqeBxKdX294JN; incap_ses_7227_1686039=p68uPAuDC06KYah3nnZLZIM+E2IAAAAASkvnzMl5Ty67HR1xS2+tQA==; optimizelyEndUserId=oeu1645428356766r0.42801721197298814; _gid=GA1.2.55246173.1645428358; _gat=1; _gcl_au=1.1.260451370.1645428358; _gat_UA-489931-23=1; fpFingerprint=74c9b8fdc0fe5ee3f705c5a8cd824cfa0dac7db061f4a8a6b4c546ee8a14d0eb; fpGUID=5ab089b5-a874-42a1-b77e-20e6e866a5a5; FPLC=WUvAI2iou5FX7nHcRF0W%2FKC4JWuPeoEhvKuol6DnTAzndbV8F4jBp20pnpvxc9ipZ%2F%2BnyzjCQlPo6BQBbytjgL48DKFInW2ZHsi71xN9CvWSguY4UOEaiY2v49VgFQ%3D%3D; FPID=FPID2.2.gLKV%2BM4wX9YrItuMzNQZ3g3FTJU4JxhNbIG%2FovRoG4o%3D.1645428358; IR_gbd=davidjones.com; IR_5504=1645428361020%7C1241842%7C1645428361020%7C%7C; _uetsid=8551aa2092e711eca1abf5c11b3e08e5; _uetvid=8551f42092e711eca2123360ba9b39c5; _gat_UA-489931-18=1; IR_PI=85a3d5ad-92e7-11ec-a792-697bbaeebf12%7C1645514761020; _fbp=fb.1.1645428363663.2003843185; _y2=1%3AeyJjIjp7IjE2Mzg4NCI6LTE0NzM5ODQwMDAsIjE2NDQ3MiI6LTE0NzM5ODQwMDAsIjE2NDQ3NSI6LTE0NzM5ODQwMDAsIjE2NjQ5OCI6LTE0NzM5ODQwMDAsIjE2NzQxNCI6LTE0NzM5ODQwMDAsIjE2ODI2NSI6LTE0NzM5ODQwMDAsIjE2ODgxMiI6LTE0NzM5ODQwMDAsIjE3MTg3NSI6LTE0NzM5ODQwMDAsIjE3MzUzNyI6LTE0NzM5ODQwMDAsIjE3NTY4OCI6LTE0NzM5ODQwMDAsIjE3NjcwNCI6LTE0NzM5ODQwMDB9fQ%3D%3D%3ALTE5NjU3ODQwMA%3D%3D%3A99; _hjSessionUser_873034=eyJpZCI6IjUzNTFmYjZkLWIyYWMtNTA2Mi05ZmRmLTM5YjJjMjViMjllNyIsImNyZWF0ZWQiOjE2NDU0MjgzNjQxMjUsImV4aXN0aW5nIjpmYWxzZX0=; _hjFirstSeen=1; _hjIncludedInSessionSample=0; _hjSession_873034=eyJpZCI6IjU3MDMwNDczLTJmNDgtNGE2Yi1hY2ExLWU0NWI3OGVmYTM5ZCIsImNyZWF0ZWQiOjE2NDU0MjgzNjUzODEsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=1; BVBRANDID=08e283e1-6cd7-4749-a59c-e2b3a25f47b7; BVBRANDSID=7ff227fa-fe38-4bd5-acd0-77ec12e4f23d; _derived_epik=dj0yJnU9YmV6UEZaOWc3bWxxR3V5N1hYaUNqX3FhTzQyQS16ZXgmbj1wTTNxcDg5OTdnTGhGUGRtM2N1eU1RJm09ZiZ0PUFBQUFBR0lUUG93JnJtPWYmcnQ9QUFBQUFHSVRQb3c; _pin_unauth=dWlkPVpUbGhOVGc1WkRVdE56TXdNaTAwWmpSbUxUazROamN0T1dJeU5USXpZVEV4TUdZMg; _clck=yrrnsx|1|ez6|0; _clsk=1yukh8w|1645428368952|1|0|b.clarity.ms/collect; inside-au=1054475826-f46505e96747589174832d3811c04e207c52f836c087d5092620db457dba3f89-0-0; _yi=1%3AeyJsaSI6bnVsbCwic2UiOnsiYyI6MSwiZWMiOjI1LCJsYSI6MTY0NTQyODQwMDAxNywicCI6MSwic2MiOjM2fSwidSI6eyJpZCI6IjQ2NTdlODNhLTJkMmQtNGI0Zi05YmNhLTBmNjE3NDBlOWE5MyIsImZsIjoiMCJ9fQ%3D%3D%3ALTE5NjU3ODQwMA%3D%3D%3A99; _ga_2LJLJKYZ3R=GS1.1.1645428358.1.1.1645428401.0; iSAMS=rzA8chGcjf7eGP1kr1q2K+TgJaBPWna9xDh8Bsz6xbCAVqgGJgd3q+R2kixSwiu+sod8pocLVzLOwrBl+UsXWQ==; _ga_7ZEZ2L98N2=GS1.1.1645428358.1.1.1645428403.15; _ga=GA1.1.611164656.1645428358',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
        'X-NewRelic-ID': 'VQ8GWVdRChABV1lTBwcBUFc='
        }
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load David Jones stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = DAVID_JONES_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load David Jones stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        stores = get(url,headers=self.headers)

        stores = stores.text
        stores = stores.split('stores:')[1]
        stores = stores.split('// ]]>')[0].strip()
        stores = stores.replace('\r','')
        stores = stores.replace('\n','')
        stores = stores.replace('\t','')
        stores = stores.replace(' ]}','')
        stores = stores[1:]
        stores = stores.replace('{','')
        stores = stores.replace('}','')
        stores = stores.split(' ,')
        stores = list(filter(lambda store : 'AU' in store ,stores))
        new_stores_list = []

        for store in stores:

            new_stores = store.split('",')
            dictionary = {}

            for new_store in new_stores:
                
                n_store = new_store.split(':')
                key = n_store[0]
                value = n_store[1].replace('"','').strip()
                dictionary[key] = value

            new_stores_list.append(dictionary)

        logger.info('Total stores: '+str(len(new_stores_list)))

        return new_stores_list

    def get_store_details(self, store):

        title = store['name']
        store_id = store['id']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)

        open_hours = ''

        return (store_id,title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        
        try: 
            lat = float(store['latitude'])
            lng = float(store['longitude'])

        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = store['postcode']
        city = store['city']
        state = get_state_code(store['state'])
        
        phone = store['phone']
        address = store['street']+' '+store['suburb']+' '+city+' '+state+' '+post_code

        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
 
        return address,city,state,post_code,phone
