from interfaces.data_source_interface import *
from crud_database.constants import ECCO_AU_COLUMNS

class EccoAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Ecco Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
    'cookie':'cqcid=acSZ25rIhGgKyw7nzmDfqjUN3a; cquid=||; dwanonymous_ff3e71820c29341825ad66dd876e02f7=acSZ25rIhGgKyw7nzmDfqjUN3a; __cq_dnt=0; dw_dnt=0; _gcl_au=1.1.285752306.1645613350; dw=1; dw_cookies_accepted=1; _gid=GA1.2.1120430758.1645613351; remindershown=true; _fbp=fb.1.1645613352477.1293906227; __cq_uuid=adVavbo6lrygxK7s0h0gGVLDeL; _pin_unauth=dWlkPVpUbGhOVGc1WkRVdE56TXdNaTAwWmpSbUxUazROamN0T1dJeU5USXpZVEV4TUdZMg; _hjSessionUser_281382=eyJpZCI6IjlkMmRiYTQyLTE4MzAtNTNhNC1iMDVjLWNmY2Q1YjEyMzc5NSIsImNyZWF0ZWQiOjE2NDU2MTMzNTI2NjcsImV4aXN0aW5nIjp0cnVlfQ==; _clck=1m3s55|1|ez8|0; _yi_user_id=585087f6-bb18-479b-a794-802a4861575d; _y2=1:eyJjIjp7fX0=:MTc0OTg2MjMwNA==:99; _aeaid=a0ff2d2f-1cd7-4cd6-8081-f247691d1ab3; aeatstartmessage=true; dwac_7fd197c0fafedbd3263b31df3e=253KsW8dR8GcDSGTcGd8KxMViNXIbw3PtKI=|dw-only|||AUD|false|Australia/Canberra|true; sid=253KsW8dR8GcDSGTcGd8KxMViNXIbw3PtKI; dwsid=kZ_FUictds2hhddC12RITFkXhzaAXzzDiSpQBXLMzsK4fzhpmslkAgKsKwgUkhoEQIZNn-lCwDjh-9PmOwHoFQ==; _sp_ses.696f=*; _hjSession_281382=eyJpZCI6IjI2NDMzY2VlLThhMGEtNDg1NS1hNDRhLWMxMDgyMmQzODg2MCIsImNyZWF0ZWQiOjE2NDU2Mzc0MTc3NDEsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=0; stc115872=env:1645637416|20220326173016|20220223180808|2|1054578:20230223173808|uid:1645613351698.910749103.9024205.115872.1640952583.:20230223173808|srchist:1054579:1:20220326104911|1054578:1645637416:20220326173016:20230223173808|tsa:0:20220223180808; _ga_SW2FXPPWDH=GS1.1.1645637416.3.1.1645637887.0; _ga=GA1.1.158273080.1645613351; _ga_S42WM01SGG=GS1.1.1645637416.3.1.1645637887.0; _hjIncludedInPageviewSample=1; _hjIncludedInSessionSample=0; _derived_epik=dj0yJnU9V2lUczVtVXhIdmtJRjhhNWpSeDNwbEF2TUV3NEFjRDcmbj11QTZtekhPT0pxU3RpakVkdHRSWHNRJm09ZiZ0PUFBQUFBR0lXY1FJJnJtPWYmcnQ9QUFBQUFHSVdjUUk; cto_bundle=NFuJ2l9EcDJJckslMkJESTNrQ0huazliSFpnNElEZzJWMzY1USUyQmVwcTclMkI0OWI0OHFHeklaNlNpcEVqZkdIdCUyQlJqT0NXalN5aXJlbkNZbnZBbTgxQzBPMldFV0hHRzJJVzA2TlVHWnRMQ2hOVWNlU0RkYkVFaGc3U3hQSDNoJTJCVGVHSmVtU0lTNEttUVFEVGNIUEl5cDR5VnF1cm5nJTNEJTNE; _uetsid=cb2cfd70949611ecb242a50534836e61; _uetvid=cb2e5350949611ec92313f10c6539d41; _clsk=1rxlbb5|1645638133870|1|1|b.clarity.ms/collect; _sp_id.696f=06ef0f10132d2950.1645613353.2.1645638440.1645613832; _yi=1:eyJsaSI6bnVsbCwic2UiOnsiYyI6MiwiZWMiOjExLCJsYSI6MTY0NTYzODQ0NzIwNywicCI6MSwic2MiOjMxNn0sInUiOnsiaWQiOiI1ODUwODdmNi1iYjE4LTQ3OWItYTc5NC04MDJhNDg2MTU3NWQiLCJmbCI6IjAifX0=:LTE4MDY5MDc0ODg=:99',
    'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',

    
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load ECCO stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = ECCO_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Ecco stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        scripts = soup.find_all('script')
        scripts = list(filter(lambda store: 'latitude' in str(store.text),scripts))
        stores = scripts[0]
        stores = stores.text.split('Json = ')[1]
        stores = stores.replace(';','')
        stores = json.loads(stores)

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store['name']
        store_id = store['storeID']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email = self.get_store_address(store)
        
        open_hours = self.get_open_hours(store)

        product_categories = store['productLines']

        isECCOStore = store['isECCOStore']


        return (store_id,title,address, city, post_code,phone,email,lat,lng,state,open_hours,product_categories,isECCOStore)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0

        try:
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = store['postalCode']
        state = store['stateCode']
        city = store['city']
        phone = store['phone']
        address = store['address']
        email = store['email']
        
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone,email

    def get_open_hours(self, store):
       
        working_hours = store['storeHours']
        opening_hours = []
        for day in working_hours:
            day_open_hours = {}
            day_open_hours['day'] = day['label'].replace(':','')
            open_close_hours = day['data']
            open_close_hours = open_close_hours.split('-')
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)

