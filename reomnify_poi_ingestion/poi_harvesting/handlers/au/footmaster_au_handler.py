from interfaces.data_source_interface import *
from crud_database.constants import FOOT_MASTER_AU_COLUMNS

class FootMasterAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Footmaster Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Foot Master stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = FOOT_MASTER_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Foot Master stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find('div',{'class':'rte rte--nomargin'})
        stores = stores.find('table')
        stores = stores.find_all('tr')

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        p_tags = store.find_all('p')
        title = store.find('strong')
        title = title.text if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code = self.get_store_address(p_tags)

        phone = store.find('a',{'data-dtype':'d3ifr'})
        phone = phone.text if phone is not None else ''
        if phone == '':
            phone = p_tags[4].text.strip().split(':')[1] if 'Phone'  in p_tags[4].text else p_tags[4].text.strip().split(':')[1]
        if phone =='':
            phone = p_tags[5].text.strip()

        open_hours = ''
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        latitude_longitude = store.find('iframe')

        if latitude_longitude is None:
            return 0,0
            
        latitude_longitude = latitude_longitude['src']
        try:
            lat_lng = latitude_longitude.split('!2d')[1]
            lat_lng = lat_lng.split('!3d')
            lng = float(lat_lng[0])
            lat = lat_lng[1].split('!3m')[0]
            lat = float(lat)
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = ''
        state = ''
        city = ''
        address = store[1].text.strip()+store[2].text.strip()+store[3].text.strip()
        
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code

    def get_open_hours(self, store):
       
        working_hours = store.text
        #working_hours = working_hours.replace('<br/>','')
        working_hours = working_hours.split('<br/>')
        opening_hours = []
        open_close_hours = store[4].find('td').text.strip()
        open_close_hours = open_close_hours.split('-')
        days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)