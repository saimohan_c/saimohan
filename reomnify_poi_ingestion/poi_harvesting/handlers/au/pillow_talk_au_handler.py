from interfaces.data_source_interface import *
from crud_database.constants import PILLOW_TALK_AU_COLUMNS

class PillowTalkAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Pillow Talk Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
    'slm-request-token': 'PVE2JYL38GZ0E8Y4LJ9N1MY63L281O497D0EDZXENQ0JVPGW5MV5OWPX2367QZW0MN451',
    'slm-unique-user-token': '958ab90d-ac4b-47a3-b4b1-dc05e69a77a1',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Pillow Talk stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = PILLOW_TALK_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Pillow Talk stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        stores = get(url,headers=self.headers)
        stores = stores['data']
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        
        title = store['name']
        store_id = store['id']
        #title = ''
        #store_id = ''

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code = self.get_store_address(store)

        open_hours = self.get_open_hours(store)
                
        phone = store['store']['phone']
        email = store['store']['email']
        return (store_id,title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        store = store['location']
        lat,lng = 0,0
        try:
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        store = store['location']

        post_code = store['postcode']
        state = store['state']
        city = store['city']
        address = store['address'] 
        address = address if address is not None else store['address_1']
        address = address if address is not None else address+store['address_2']
        address = address+store['address_2'] if store['address_2'] is not None else address

        
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code


    def get_open_hours(self, store):
       

        working_hours = store['store']['timetable']
        days = working_hours.keys()
        opening_hours = []
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = working_hours[day][0] if len(working_hours[day])>1 else ''
            day_open_hours['close_time'] = working_hours[day][1] if len(working_hours[day])>0 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)