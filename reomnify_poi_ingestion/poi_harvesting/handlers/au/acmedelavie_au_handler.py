from interfaces.data_source_interface import *
from crud_database.constants import ACMEDELAVIE_AU_COLUMNS

class AcmedelavieAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Acmedelavie Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Acmedelavie stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = ACMEDELAVIE_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Acmedelavie stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers = self.headers)
        soup = get_bs4_soup_content(html_content)
        
        stores = soup.find_all('div',{'class':'uncont'})
        stores = list(filter(lambda store:'iframe' in str(store),stores))

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store.find('h3')
        title = title.text.strip() if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code = self.get_store_address(store)

        open_hours = ''

        phone = ''

        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        try:
            iframe = store.find('iframe')
            iframe = iframe['src']
            lat_lng = iframe.split('!2d')[1]
            lat_lng = lat_lng.split('!3d')
            lng = float(lat_lng[0])
            lat = lat_lng[1].split('!2m')[0]
            lat = float(lat)
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        
        post_code = 0
        address = store.find('p')
        address = address.text.replace('\n','').strip() if address is not None else ''
        state = ''
        city = ''
        
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code

        