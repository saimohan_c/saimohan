from interfaces.data_source_interface import *
from crud_database.constants import BEDSHED_AU_COLUMNS

class BedshedAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Bed Shed Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Bedshed stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = BEDSHED_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Bedshed stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers = self.headers)
        soup = get_bs4_soup_content(html_content)
        
        stores = soup.find_all('div',{'class':'store'})

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):



        new_store = store.find('a',{'class':'btn btn-primary more-info'})
        new_store = new_store['href']

        html_content = get_url_html_content(new_store,headers = self.headers)
        soup = get_bs4_soup_content(html_content)


        store = soup.find('div',{'class':'store single-store'})

        title = store.find('h1',{'class':'store-locator-title'})
        title = title.text.strip() if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(soup)

        address,city,state,post_code = self.get_store_address(store)

        open_hours = self.get_open_hours(store)

        phone = store.find('div',{'class':'store-phone'})
        phone = phone.text.split(':')[1].strip() if phone is not None else ''

        email = store.find('div',{'class':'store-email'})
        email = email.find('a')
        email = email['href'].split(':')[1].strip() if email.find('a') is not None else ''

        fax = store.find('div',{'class':'store-fax'})
        fax = fax.text.split(':')[1].strip() if fax is not None else ''

        return (title,address, city, post_code,phone,email,fax,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        
        try: 
            a_tag = store.find('a',{'target':'_blank'})
            href = a_tag['href']
            script = href.split('/@')[1]
            lat_lng = script.split(',')
            lat = float(lat_lng[0])
            lng = lat_lng[1]
            lng = float(lng)
        except:
            lat,lng = 0,0

        if lat == 0 and lng == 0:
            scripts = store.find_all('script',{'type':'text/javascript'})
            scripts = list(filter(lambda script: 'lat:' in script.text,scripts))
            script = scripts[0].text.split('new google.maps.LatLng(')[1]
            script = script.split(';')[0]
            script = script.replace(')','')
            lat_lng = script.split(',')
            lat = float(lat_lng[0])
            lng = float(lat_lng[1])

        return lat,lng

    def get_store_address(self,store):

        post_code = 0
        city = ''
        state = ''
        address = store.find('div',{'class':'store-address'})
        address = address.text.strip() if address is not None else ''
        address = address.replace('\r','')
        address = address.replace('\n','')
        address = address.replace('  ','')
        address_split = address.split(',')

        if re.search('[0-9]{4}',address_split[-2]):
            post_code = re.findall('[0-9]{4}',address_split[-2])
            post_code = post_code[0]

        city_address = address_split[-2].split(post_code)
        state = city_address[0].strip()

        return address,city,state,post_code


    def get_open_hours(self, store):
        
        opening_hours = []

        starting_day_time = store.find('div',{'class':'opening-hour opening-hour-today'})
        starting_day = starting_day_time.find('span',{'class':'day'}).text
        starting_time = starting_day_time.find('span',{'class':'time'}).text

        remaining_days = store.find('div',{'class':'additional-opening-hours'})
        remaining_days = remaining_days.find_all('div',{'class':'opening-hour'})

        for day in remaining_days:
            day_open_hours = {}
            day_open_hours['day'] = day.find('span',{'class':'day'}).text
            open_close_hours = day.find('span',{'class':'time'}).text.split('-')
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>0 else ''
            opening_hours.append(day_open_hours)

        day_open_hours = {}
        day_open_hours['day'] = starting_day
        open_close_hours = starting_time.split('-')
        day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
        day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>0 else ''
        opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)


        