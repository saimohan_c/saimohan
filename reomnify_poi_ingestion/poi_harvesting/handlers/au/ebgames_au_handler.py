from interfaces.data_source_interface import *
from crud_database.constants import EBGAMES_AU_COLUMNS

class EbGamesAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Eb Games Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
    'cookie':"Session=4syLFtFHTL8csUAeD2E0HUhHnYytJ8ZxFzIPdIk1+dKdR3b1ZfHYRA/Wtg83kt4CwcMkIZuwcFd882P/o4oiYu+lruZJmTxbE9Un4NZIj5CSuPdicglvkvQgKg/BXDJ0oLoCc18/oo8VNk9zuRn2K33sBb/8YbdzaJvZqbvIbh69+IFXjaZc7NRlzOcFqhkMj06R7wFu346X4oHF5R61Sl6ryl4GRGEbqCGkWgaIpxC8vvLUambYQ2Oq0pkRMbpqL8I4lQHyjsQzcFH1iU54/sSucTA=; __RequestVerificationToken=0rryd9ioudoWxZuCzlt7EWti_5Q17v22K7Apw4ugQgKxgYhp5KEdfVL4xVOqNz86T8IXuk-UxAteBOa_gF1-Ue_MpUU1; _ga=GA1.4.1210092139.1645612939; _gid=GA1.4.1348824710.1645612939; _cs_c=1; _ga=GA1.3.1210092139.1645612939; _gid=GA1.3.1348824710.1645612939; _fbp=fb.2.1645612943189.14768925; _gcl_au=1.1.1087368400.1645612945; com.silverpop.iMAWebCookie=614a819b-2b91-b323-3e55-355c67b654e2; __cf_bm=gifNNdPhztSyPNSqNew5AotuTvSM3PfPR2gUylXX8MQ-1645613854-0-AejlfSNj5e6dRdYwUih0QEUQ+de+PTptU0/yzOapZlgKrLR+2Buna6ETuy8uiZNc6ABBuHY37fE1Czyc6oTSH2Ds+JivydqT6RmQxDMRGGAWE6iq456YCc5pq5j81SAqLsy6Q1o8NxlK+IBaxuD6hEcjXlAtav/Tg5MLmVtJi1pV; _cs_id=a05851d6-65a2-aba9-e441-f08a3379a883.1645612939.1.1645614355.1645612939.1.1679776939745; _cs_s=6.0.0.1645616155187; mf_ed0843cc-509f-4cfd-a6e7-1e890ca211e6=|.-2129066847.1645614356528|1645612946702||0|||0|0|90.08711; _gat=1",
    'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
    'authority': 'www.ebgames.com.au',
    'cache-control': 'max-age=0',
    'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Linux"',
    'upgrade-insecure-requests': '1',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'none',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',

    
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Eb Games stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = EBGAMES_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Eb Games stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('script',{'type':'text/javascript'})
        stores = list(filter(lambda store:'latitude' in str(store),stores))
        stores = stores[0].text.split('"#store-map",')[1]
        stores = stores.split('//c2-ebgames')[0]
        stores = stores[:-2].strip()
        stores = json.loads(stores)

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = ''

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)
        
        open_hours = ''

        return (title,new_address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0

        try:
            lat_lng = store.find('div',{'class':'shg-map-container'})
            lat = float(lat_lng['data-latitude'])
            lng = float(lat_lng['data-longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = ''
        state = ''
        city = ''
        phone = ''
        address = ''
        
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone
