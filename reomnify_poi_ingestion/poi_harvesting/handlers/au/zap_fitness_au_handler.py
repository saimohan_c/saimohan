from interfaces.data_source_interface import *
from crud_database.constants import ZAP_FITNESS_AU_COLUMNS

class ZapFitnessAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Zap Fitness Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Zap Fitness stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = ZAP_FITNESS_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Zap Fitness stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        stores = get(url,headers=self.headers)
        stores = stores['clubs']
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store['title']
        store_id = store['clubId']
        

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)

        open_hours = self.get_open_hours(store)

        slug = store['slug']
        facilities_provided = store['facilities']

        return (store_id,title,address, city, post_code,phone,lat,lng,state,open_hours,slug,facilities_provided)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        try:
            lat = float(store['latitude'])
            lng = float(store['longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):
        
        phone = store['phone']
        store = store['address']

        post_code = store['postcode']
        state = store['state']
        city = store['city']
        address = store['street1'] 
        address = address if address is not None else store['street2']
        address = address if address is not None else address+store['address_2']
        
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
       
        
        return address,city,state,post_code,phone


    def get_open_hours(self, store):
       

        working_hours = store['schedule']['scheduleDays']
        opening_hours = []
        for day in working_hours:
            day_open_hours = {}
            day_open_hours['day'] = day['dayName']
            open_close_hours = day['scheduledTimes']
            if len(open_close_hours)==1:
                day_open_hours['open_time'] = open_close_hours[0]['startTime'].strip().split('T')[1][:5] if len(open_close_hours)>0 else ''
                day_open_hours['close_time'] = open_close_hours[0]['endTime'].strip().split('T')[1][:5] if len(open_close_hours)>0 else ''
                opening_hours.append(day_open_hours)
            elif len(open_close_hours)>1:
                for each in range(len(open_close_hours)):
                    day_open_hours['open_time'] = open_close_hours[each]['startTime'].strip().split('T')[1][:5] if len(open_close_hours)>0 else ''
                    day_open_hours['close_time'] = open_close_hours[each]['endTime'].strip().split('T')[1][:5] if len(open_close_hours)>0 else ''
                    opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)