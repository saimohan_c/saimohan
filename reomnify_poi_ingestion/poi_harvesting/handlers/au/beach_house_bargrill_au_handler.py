from interfaces.data_source_interface import *
from crud_database.constants import BEACH_HOUSE_BARGRILL_AU_COLUMNS

class BeachHouseBargrillAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Beach House Bargrill Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
        self.addresses = {
            'BROWNSPLAINS':'Grand Plaza FS5, 27-49 Browns Plains Rd, Browns Plains QLD 4118, Australia',
            'CBD':'Shop 58, Level E, Myer Centre Corner Elizabeth &, Albert St, Brisbane City QLD 4000, Australia',
            'COOMERA':'Shop R005 Westfield Coomera, 103 Foxwell Rd, Coomera QLD 4209, Australia',
            'GARDEN CITY':'Kessels Rd, Upper Mount Gravatt QLD 4122, Australia',
            'LOGANHOLME':'Tenancy 326, Logan Hyperdome Cnr Pacific Hwy &, Bryants Rd, Loganholme QLD 4129, Australia',
            'NAMBOUR':'35 Currie St, Nambour QLD 4560, Australia',
            'NORTH LAKES':'Shop R3, Westfield North Lakes, N Lakes Dr, Mango Hill QLD 4509, Australia',
            'SPRINGFIELD':'Shop 40, Orion, 1 Main St, Springfield Central QLD 4300, Australia',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Beach House Bargrill stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = BEACH_HOUSE_BARGRILL_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Beach House Bargrill stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers = self.headers)
        soup = get_bs4_soup_content(html_content)
        
        stores = soup.find_all('div',{'class':'rowItem col-md-3 col-sm-6 col-ms-12 btTextLeft inherit'})

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store.find('span',{'class':'headline'})
        title = title.text if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code = self.get_store_address(store)

        open_hours = self.get_open_hours(store)

        phone = store.find('span',{'class':'btIco btIcoDefaultType btIcoExtraSmallSize btIcoDefaultColor btIconCircleShape'})
        phone = phone.text if phone is not None else ''
        
        email = store.find('a',{'target':'_self'})
        email = email.text if email is not None else ''

        return (title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        script = store.find('script').text

        try:
            script = script.split('map_canvas')[1]
            script = script.split(');')[0]
            lat_lng = script.split(',')
            lat = float(lat_lng[1])
            lng = lat_lng[2]
            lng = float(lng)
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = 0
        city = ''
        state = ''
        title = store.find('span',{'class':'headline'})
        title = title.text if title is not None else ''
        address = self.addresses[title] if title in self.addresses.keys() else ''
        
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
 
        return address,city,state,post_code


    def get_open_hours(self, store):
        
        working_hours = store.find('div',{'class':'btSubTitle'})
        working_hours = working_hours.text.strip().split('\r')
        opening_hours = []

        if len(working_hours)>1:

            for day_time in working_hours:
                days_timing = day_time.split(': ')
                days = days_timing[0].split('-')
                open_close_hours = days_timing[1].split('–')
                start_day = get_day_no(days[0].lower()) if days[0] != 'sun' else 0
                end_day = get_day_no(days[1].lower())

                for day in range(start_day,end_day+1):
                    day_open_hours = {}
                    day_open_hours['day'] = get_day_key(day)
                    day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
                    day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>0 else ''
                    opening_hours.append(day_open_hours)
        else:
            days_timing = working_hours[0].split(': ')
            days = days_timing[0].split('-')
            open_close_hours = days_timing[1].split('–')
            start_day = get_day_no(days[0])
            end_day = get_day_no(days[1])
            for day in range(start_day,end_day+1):
                day_open_hours = {}
                day_open_hours['day'] = get_day_key(day)
                day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
                day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>0 else ''
                opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)


        