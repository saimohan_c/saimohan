from interfaces.data_source_interface import *
from crud_database.constants import PHARMACY_4_LESS_AU_COLUMNS

class Pharmacy4LessAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Pharmacy 4 Less Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
            'authority': 'www.pharmacy4less.com.au',
    'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'x-requested-with': 'XMLHttpRequest',
    'sec-ch-ua-mobile': '?0',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
    'sec-ch-ua-platform': '"Linux"',
    'origin': 'https://www.pharmacy4less.com.au',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://www.pharmacy4less.com.au/storelocation/',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
    'cookie': 'frontend=d5303518cd83b87d70c0b2e2be84da91; frontend_cid=9aSOPxHtg7mvfUiE; mailchimp_landing_page=https%3A//www.pharmacy4less.com.au/storelocation/; _ga=GA1.3.2000603156.1645722685; _gid=GA1.3.798616497.1645722685; _fbp=fb.2.1645722686052.781975963; _caid=678a79fc-64d9-4bd5-8102-73ffe038585d; _cavisit=17f2cb7529d|; _clck=1wkivr8|1|ez9|0; _sp_ses.6467=*; _uetsid=cbb60d60959411ec9d0da92e4d263b7a; _uetvid=cbb6dba0959411ecbb5d23d3d223b24d; _clsk=1btpfhk|1645725524405|6|1|a.clarity.ms/collect; _dc_gtm_UA-44377933-2=1; _sp_id.6467=a6bd8ed06f7cba33.1645722709.1.1645725529.1645722709',
        }
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Pet Stock stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = PHARMACY_4_LESS_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Pet Stock stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        data = {
            'country': 'AU',
            'address': '',
            'localtion_latitude': '',
            'localtion_longitude': '',
            'latitude': 'null',
            'longitude': 'null'
        }


        stores = post(url,headers=self.headers,data=data)
        print(len(stores['maps']['items']))
        stores = stores['maps']['items']

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store['title']
        store_id = store['entity_id']
        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email = self.get_store_address(store)
        open_hours = self.get_open_hours(store)
        
        isactive = store['is_active']
        isactive = True if isactive == '1' else False

        covid_vaccine_store = True if store['covid_vaccine_store'] == '1' else False

        return (store_id,title,address, city, post_code,phone,email,lat,lng,state,open_hours,isactive,covid_vaccine_store)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        
        try: 
            lat = float(store['latitude'])
            lng = float(store['longitude'])

        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = store['postal_code']
        city = store['city']
        state = store['region']
        
        phone = store['phone']
        address = store['street']+' '+city+' '+post_code

        email = store['email']
 
        return address,city,state,post_code,phone,email

    def get_open_hours(self, store):
       
        working_hours = store['desc']
        #print(working_hours)
        if working_hours.strip() == 'Temporarily Closed':
            return CLOSED
        working_hours = working_hours.split('<br>\r\n') if '<br>\r\n' in working_hours else working_hours.split('</br>\r\n')
        working_hours = list(filter(lambda hour:'<br>' not in hour,working_hours))
        
        opening_hours = []

        for day in working_hours:

            day_time = day.split('day')
            if len(day_time)>1:
                day_open_hours = {}
                day_open_hours['day'] = day_time[0].strip()
                if day_time[1].strip() == '24 HOURS':
                    day_open_hours['open_time'] = '12:00am'
                    day_open_hours['close_time'] = '12:00am'
                else:
                    open_close_hours = day_time[1].split('-') if '-' in day_time[1] else day_time[1].split('–')
                    day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
                    day_open_hours['close_time'] = open_close_hours[1].strip().replace('<br>','') if len(open_close_hours)>1 else ''

                opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours) 