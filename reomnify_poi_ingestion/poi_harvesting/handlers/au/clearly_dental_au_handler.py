from interfaces.data_source_interface import *
from crud_database.constants import CLEARLY_DENTAL_AU_COLUMNS

class ClearlyDentalAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Clearly Dental Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'cookie':'ASP.NET_SessionId=nhimwbs4j5b3q3ts1rxcee0n; nlbi_2341090=da/HQBDTxE3bIk+H+EA72gAAAAD0FdWPD74kLrCNEzMMt2rP; incap_ses_709_2341090=/gASMhG2M25IsC9nReDWCVkP/WEAAAAALaf3Lv5kThHG+AxIqZQKPA==; resolution=1366; ai_user=xsAXl|2022-02-04T11:34:59.080Z; SC_ANALYTICS_GLOBAL_COOKIE=98f0152e01f34178a417334b074eefbe|True; channelflow=n|naturalsearch|0; channeloriginator=n; channelcloser=n; AMCVS_3D95661352A6473F0A490D44%40AdobeOrg=1; s_cc=true; _gcl_au=1.1.867647774.1643974503; _ga=GA1.3.2142815523.1643974505; _fbp=fb.2.1643974505288.1047762147; _hjSessionUser_2114520=eyJpZCI6IjUzMDljMzE5LWJkZjMtNTA4MC1hNDU5LTczY2RlMzQ1MGY0NiIsImNyZWF0ZWQiOjE2NDM5NzQ1MDQ2MjAsImV4aXN0aW5nIjp0cnVlfQ==; incap_ses_706_2341090=Zre9eB4+uE5vzCsd1TfMCU0P/mEAAAAAImdqwhf7MRCE2AAdnKoCCQ==; visid_incap_2341090=JlP6v4u5T1GIEmswLCXsjVcP/WEAAAAAQkIPAAAAAACA2xmiAZ0vf2ouXlurmgesfChaZy/VzqZT; incap_ses_1122_2341090=2c+FBgbcunF20xWViSWSD5sFAWIAAAAAVda+BvLZi7gL3693FakAHQ==; AMCV_3D95661352A6473F0A490D44%40AdobeOrg=-1124106680%7CMCIDTS%7C19031%7CMCMID%7C17839302333401638712342709209496814981%7CMCAAMLH-1644838944%7C12%7CMCAAMB-1644838944%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1644241344s%7CNONE%7CvVersion%7C5.2.0; _gid=GA1.3.1976322467.1644234150; _hjSession_2114520=eyJpZCI6IjBlYTdkYTEzLWYxZjUtNDk4Zi05ZTU0LTk3MTUwNzk2Y2U2ZCIsImNyZWF0ZWQiOjE2NDQyMzQxNDk4NjksImluU2FtcGxlIjp0cnVlfQ==; _hjAbsoluteSessionInProgress=1; _hjIncludedInPageviewSample=1; delaconsessid=10d13ad643684b458b842ef8e3caedae; delaconphonenums=108309,07 2105 8299,true,61 7 3289 6699,au,|; utag_main=v_id:017ec4841e520015c74dd349d4f703068002a0600086e$_sn:3$_se:5$_ss:0$_st:1644237830636$vapi_domain:clearlydental.com.au$ses_id:1644234143771%3Bexp-session$_pn:4%3Bexp-session; ai_session=w2Y2e|1644234144325|1644236031756.2',
             #ASP.NET_SessionId=nhimwbs4j5b3q3ts1rxcee0n; nlbi_2341090=da/HQBDTxE3bIk+H+EA72gAAAAD0FdWPD74kLrCNEzMMt2rP; incap_ses_709_2341090=/gASMhG2M25IsC9nReDWCVkP/WEAAAAALaf3Lv5kThHG+AxIqZQKPA==; resolution=1366; ai_user=xsAXl|2022-02-04T11:34:59.080Z; SC_ANALYTICS_GLOBAL_COOKIE=98f0152e01f34178a417334b074eefbe|True; channelflow=n|naturalsearch|0; channeloriginator=n; channelcloser=n; AMCVS_3D95661352A6473F0A490D44%40AdobeOrg=1; s_cc=true; _gcl_au=1.1.867647774.1643974503; _ga=GA1.3.2142815523.1643974505; _fbp=fb.2.1643974505288.1047762147; _hjSessionUser_2114520=eyJpZCI6IjUzMDljMzE5LWJkZjMtNTA4MC1hNDU5LTczY2RlMzQ1MGY0NiIsImNyZWF0ZWQiOjE2NDM5NzQ1MDQ2MjAsImV4aXN0aW5nIjp0cnVlfQ==; incap_ses_706_2341090=Zre9eB4+uE5vzCsd1TfMCU0P/mEAAAAAImdqwhf7MRCE2AAdnKoCCQ==; visid_incap_2341090=JlP6v4u5T1GIEmswLCXsjVcP/WEAAAAAQkIPAAAAAACA2xmiAZ0vf2ouXlurmgesfChaZy/VzqZT; incap_ses_1122_2341090=2c+FBgbcunF20xWViSWSD5sFAWIAAAAAVda+BvLZi7gL3693FakAHQ==; AMCV_3D95661352A6473F0A490D44%40AdobeOrg=-1124106680%7CMCIDTS%7C19031%7CMCMID%7C17839302333401638712342709209496814981%7CMCAAMLH-1644838944%7C12%7CMCAAMB-1644838944%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1644241344s%7CNONE%7CvVersion%7C5.2.0; _gid=GA1.3.1976322467.1644234150; _hjSession_2114520=eyJpZCI6IjBlYTdkYTEzLWYxZjUtNDk4Zi05ZTU0LTk3MTUwNzk2Y2U2ZCIsImNyZWF0ZWQiOjE2NDQyMzQxNDk4NjksImluU2FtcGxlIjp0cnVlfQ==; _hjAbsoluteSessionInProgress=1; _hjIncludedInPageviewSample=1; delaconsessid=10d13ad643684b458b842ef8e3caedae; utag_main=v_id:017ec4841e520015c74dd349d4f703068002a0600086e$_sn:3$_se:11$_ss:0$_st:1644239047504$vapi_domain:clearlydental.com.au$ses_id:1644234143771%3Bexp-session$_pn:8%3Bexp-session; ai_session=w2Y2e|1644234144325|1644237249903.1; delaconphonenums=99688,03 9993 7146,true,03 9853 1811,au,|99691,07 5631 8622,true,07 5576 4923,au,|108303,03 9102 1470,true,61 3 9380 2903,au,|108309,07 2105 8299,true,61 7 3289 6699,au,|109134,08 8120 4093,true,08 8296 0988,au,|113210,02 9167 9497,true,02 9908 3466,au,|115203,08 9468 9187,true,(08) 9315 3843,au,|115205,02 9641 5493,true,(02) 9412 3805,au,|99685,02 4203 3552,true,02 4261 1199,au,|112670,02 9167 0219,true,+61 2 9416 9200,au,|
    'Accept-Language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
    'Referer': 'https://www.google.com/',
    #'referer':'https://www.google.com/',
        }
              
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Clearly Dental stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = CLEARLY_DENTAL_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Clearly Dental stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):


        '''scraper = cloudscraper.create_scraper()

        response = scraper.get(url)'''
        url = self.get_request_url()
        #url = url.split('://')[1]
        #url = 'http://webcache.googleusercontent.com/search?q=cache:'+url
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        stores = soup.find_all('custom-card',{'class':'card-col-3'})
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        
        
        title = store.find('custom-card.title',{'slot':'title'}).text

        new_ad = store.find('custom-card.cta',{'slot':'cta'})
        new_ad = new_ad['href']
        print(new_ad)
        url = self.configuration['origin']+new_ad
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code = self.get_store_address(store)

        open_hours = ''
        phone = store.find('a').text
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        store = store.find('div',{'class':'col-12 col-md-6 order-1 order-md-2'})
        print(store)
        store = store.find('google-map')
        print(store['lat'],store['lng'])
        lat,lng = 0,0
        try:
            map_button = store.find('google-map')
            lat = float(map_button['lat'])
            lng = float(map_button['lng'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = ''
        state = ''
        city = ''
        store_address = store.find('custom-card.content',{'slot':'content'})
        p_tag = store_address.find('p')
        address = p_tag.text.strip().split(':')[1]
        phone = ''
        if re.search('[0-9]{5}',address):
            post_code = re.findall('[0-9]{5}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code

    def get_open_hours(self, store):
       

        working_hours = store.text
        #working_hours = working_hours.replace('<br/>','')
        working_hours = working_hours.split('<br/>')
        print(working_hours)
        opening_hours = []
        open_close_hours = store[4].find('td').text.strip()
        open_close_hours = open_close_hours.split('-')
        days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        for day in days:
            day_open_hours = {}
            day_open_hours['day'] = day
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>0 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else ''
            opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours)