from interfaces.data_source_interface import *
from crud_database.constants import CHEMPRO_AU_COLUMNS

class ChemproAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Chempro Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
            'authority':'www.chempro.com.au',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
    'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
    'content-type':'application/x-www-form-urlencoded; charset=UTF-8',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Chempro stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = CHEMPRO_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Chempro stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        params = self.configuration['params']

        html_content = post(url,headers=self.headers,data = params)
        stores = html_content.split('!-')
        stores = list(filter(lambda store: 'Australia' in str(store),stores))
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        store = store.split('@')[0]
        store = store.split('!')
        
        title = store[2]

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(store)

        open_hours = ''
        
        return (title,address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):
        lat,lng = 0,0
        try:
            lat = float(store[0])
            lng = float(store[1])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = ''
        state = ''
        city = ''
        address = store[3]
        print(address)
        phone = ''
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
        print(post_code)
        return address,city,state,post_code,phone

    