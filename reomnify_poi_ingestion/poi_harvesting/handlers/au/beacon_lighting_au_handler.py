from interfaces.data_source_interface import *
from crud_database.constants import BEACON_LIGHTING_AU_COLUMNS

class BeaconLightingAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Beacon Lighting Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
        
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Beacon Lighting stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = BEACON_LIGHTING_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Beacon Lighting stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        stores = get(url,headers=self.headers)
        stores = stores['items']
        
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        store_id = store['id']
        title = store['name']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email = self.get_store_address(store)

        open_hours = self.get_open_hours(store)
        fax = store['store_fax']
        attributes = store['attributes']
        attributes = [attribute['frontend_label'] for attribute in attributes]
        services = ','.join(attribute for attribute in attributes)

        return (store_id,title,address, city, post_code,phone,email,fax,services,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0

        try:
            lat = float(store['lat'])
            lng = float(store['lng'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = store['zip']
        state = store['state_data']['name']
        city = store['city']
        address = store['address']
        phone = store['phone']
        email = store['email']

        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
        
        return address,city,state,post_code,phone,email


    def get_open_hours(self, store):
        
        working_hours = store['schedule_array']
        opening_hours = []
        for key,value in working_hours.items():
            day_open_hours = {}
            day_open_hours['day'] = key
            day_open_hours['open_time'] = value['from']['hours']+':'+value['from']['minutes'] 
            day_open_hours['close_time'] = value['to']['hours']+':'+value['to']['minutes'] 
            opening_hours.append(day_open_hours)
        
        return get_open_hours_formatted(opening_hours)
        