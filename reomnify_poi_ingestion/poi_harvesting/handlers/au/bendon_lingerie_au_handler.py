from interfaces.data_source_interface import *
from crud_database.constants import BENDON_LINGERIE_AU_COLUMNS

class BendonLingerieAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Bendon Lingerie Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Bendon Lingerie stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = BENDON_LINGERIE_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Bendon Lingerie stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers = self.headers)
        soup = get_bs4_soup_content(html_content)
        
        stores = soup.find_all('div',{'class':'amlocator-store-desc'})

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):



        new_store = store.find('a',{'class':'view-store'})
        new_store = new_store['href']

        html_content = get_url_html_content(new_store,headers = self.headers)
        soup = get_bs4_soup_content(html_content)


        #store = soup.find('div',{'class':'store single-store'})

        title = soup.find('h3')
        title = title.text.strip() if title is not None else ''

        lat,lng = self.get_store_geo_coordinates(soup)

        address,city,state,post_code = self.get_store_address(soup)

        open_hours = self.get_open_hours(soup)
        #open_hours = ''

        contacts = soup.find_all('a',{'class':'amlocator-link'})

        phone = contacts[0].text.strip() if len(contacts)>1 else ''
        fax = contacts[1].text.strip() if len(contacts)>1 else ''

        return (title,address, city, post_code,phone,fax,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        
        try: 
            
            scripts = store.find_all('script')
            scripts = list(filter(lambda script: 'locationData: {' in script.text,scripts))
            script = scripts[0].text.split('locationData: ')[1]
            script = script.split('marker_url')[0]
            script = script.split(',')
            lat = script[0].split('lat:')[1].strip()
            lat = float(lat)
            lng = script[1].split('lng:')[1]
            lng = float(lng)

        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        store_address = store.find('div',{'class':'amlocator-block amlocator-location-info icon icon-location'})
        store_address = store_address.find_all('div',{'class':'amlocator-block'})

        post_code = 0
        city = ''
        state = ''
        address = ' '.join(store.text.replace('\r','').strip() for store in store_address)
        address = address.replace('\n','')

        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]

        city_address = address.split(post_code)[0]
        city_address = city_address.split(',')
        city = city_address[1].strip()

        return address,city,state,post_code


    def get_open_hours(self, store):
        
        opening_hours = []

        working_hours = store.find('div',{'class':'amlocator-schedule-table'})
        working_hours = working_hours.find_all('div',{'class':'amlocator-row'})

        for day in working_hours:
            day_open_hours = {}
            day_open_hours['day'] = day.find('span',{'class':'amlocator-cell -day'}).text
            open_close_hours = day.find('span',{'class':'amlocator-cell -time'}).text.split('-')
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>1 else CLOSED
            opening_hours.append(day_open_hours)


        return get_open_hours_formatted(opening_hours)


        