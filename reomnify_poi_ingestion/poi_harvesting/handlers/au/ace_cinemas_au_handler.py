from interfaces.data_source_interface import *
from crud_database.constants import ACE_CINEMAS_AU_COLUMNS

class AceCinemasAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Ace Cinemas Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Ace Cinemas stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = ACE_CINEMAS_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Ace Cinemas stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers = self.headers)
        soup = get_bs4_soup_content(html_content)
        
        stores = soup.find_all('script',{'type':'text/javascript'})

        stores = list(filter(lambda store:'telephone' in str(store.text),stores))
        
        stores = stores[0].text
        stores = stores.replace('// <![CDATA[', '')
        stores = stores.replace('// ]]>','')
        stores = json.loads(stores)

        a_tags = soup.find_all('a')
        a_tags = list(filter(lambda a_tag: 'maps/place' in a_tag['href'],a_tags))

        for tag,store in zip(a_tags,stores):
            store['map_url'] = tag['href']

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        
        title = store['name']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code = self.get_store_address(store)

        open_hours = ''

        phone = store['telephone']
        emails = store['email']
        email = ','.join(email for email in emails)

        return (title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0

        try:
            lat_lng = store['map_url'].split('/@')[1]
            lat_lng = lat_lng.split(',')
            lat = float(lat_lng[0])
            lng = lat_lng[1]
            lng = float(lng)
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        post_code = store['address']['postalCode'] 
        post_code = post_code if post_code is not None else ''
        state = store['address']['addressLocality'] 
        state = state if state is not None else ''
        city = ''
        address = store['address']['streetAddress'] + state + store['address']['addressRegion']
        
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
 
        return address,city,state,post_code

        