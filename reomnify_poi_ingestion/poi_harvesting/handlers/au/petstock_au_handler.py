from interfaces.data_source_interface import *
from crud_database.constants import PETSTOCK_AU_COLUMNS

class PetStockAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Pet Stock Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
            #'accept': 'application/json, text/javascript, */*; q=0.01',
            #'accept-encoding': 'gzip, deflate, br',
            #'accept-language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7',
            #'cookie': 'dwac_4d5c157a5a84515214c3c42333=XxoYjObOxaJALH-_ltgsGnOWr-cUByD_stw=|dw-only|||AUD|false|Australia/NSW|true; cqcid=bdbW1GuuiqckP7t66kVOSahko6; cquid=||; sid=XxoYjObOxaJALH-_ltgsGnOWr-cUByD_stw; geolocation=IN|India|Hyderabad|500025|Telangana|TG; dwanonymous_b459b105b4579626a49f1e4c9d1eb233=bdbW1GuuiqckP7t66kVOSahko6; ts_uid=beb18a98ae39ffafca6f2c9cfc; __cq_dnt=0; dw_dnt=0; dwsid=9IdA-NDi9CeuJTWGn7bhJ9gL95MiKxF6XNX4wXasX2DadNcitWotKeL1LtST7PjKfnXbIjcrYKV7755IuhxG3A==; optimizelyEndUserId=oeu1645719892676r0.19115892848985916; _gcl_au=1.1.541261107.1645719893; dw=1; dw_cookies_accepted=1; tfa_tra_src=Direct; sessionId=1645719899389.y6a4v1vcrw; _gid=GA1.3.936415408.1645719901; _fbp=fb.2.1645719901398.1755927628; __cq_uuid=adVavbo6lrygxK7s0h0gGVLDeL; __cq_seg=0~0.00!1~0.00!2~0.00!3~0.00!4~0.00!5~0.00!6~0.00!7~0.00!8~0.00!9~0.00; _uetsid=50d90b70958e11ecb42145d067f2327c; _uetvid=50d9b540958e11ec8862c7a897b337ec; stc121898=tsa:1645719902490.1342835154.703185.33381611279122314.:20220224165502|env:1|20220327162502|20220224165502|1|1114594:20230224162502|uid:1645719902490.2021923561.8593035.121898.334129160.:20230224162502|srchist:1114594:1:20220327162502:20230224162502; _hjSessionUser_1560689=eyJpZCI6Ijk3N2YxODg3LTEyMDQtNTUxYS05NzFlLTc1YzNkYTNlM2I5OSIsImNyZWF0ZWQiOjE2NDU3MTk5MDM0NTUsImV4aXN0aW5nIjpmYWxzZX0=; _hjFirstSeen=1; _hjIncludedInSessionSample=1; _hjSession_1560689=eyJpZCI6Ijc4Y2RkMzg4LWNjNGMtNGE4ZC1iMzdiLWIxNjQxZmYwZWFhYyIsImNyZWF0ZWQiOjE2NDU3MTk5MDM2ODYsImluU2FtcGxlIjp0cnVlfQ==; _hjIncludedInPageviewSample=1; _hjAbsoluteSessionInProgress=0; __zlcmid=18hkjNh8IUCmNwP; smc_uid=1645719911175751; smc_tag=eyJpZCI6MzUxOCwibmFtZSI6InBvbGl0aXguY29tLmF1In0=; smc_session_id=rsqJKD6X2KP3NVeRxaRU44wLhceDRvxb; smc_userLoggedIn=false; smc_refresh=18112; smc_spv=1; smc_tpv=1; smc_sesn=1; smc_not=default; smct_last_ov=[{"id":63926,"loaded":1645719913316,"open":null,"eng":null,"closed":null}]; _derived_epik=dj0yJnU9Mzk1NERCeHowMlBwTXZMR3dTV2xLU2NRSDg3RDcwb3gmbj1rbXV5V1dORDR6YmlTOEdSN2ZZTENBJm09ZiZ0PUFBQUFBR0lYc1d3JnJtPWYmcnQ9QUFBQUFHSVhzV3c; _pin_unauth=dWlkPVpUbGhOVGc1WkRVdE56TXdNaTAwWmpSbUxUazROamN0T1dJeU5USXpZVEV4TUdZMg; _ga=GA1.3.1958263231.1645719901; _dc_gtm_UA-9506652-1=1; smct_session={"s":1645719912338,"l":1645719975338,"lt":1645719975342,"t":54,"p":54}; _ga_12DV3TDL5T=GS1.1.1645719899.1.1.1645719975.0',            
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',

        }
        
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Pet Stock stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = PETSTOCK_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Pet Stock stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()

        stores = get(url,headers=self.headers)
        stores = stores['data']
        stores = list(filter(lambda store:store['address']['country']=='Australia',stores))

        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store['name']
        store_id = store['id']
        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone,email = self.get_store_address(store)
        open_hours = self.get_open_hours(store)

        services = store['services']
        services_keys = services.keys()
        services = list(filter(lambda service:services[service]=='1',services_keys))
        services = ','.join(service for service in services)
        
        return (store_id,title,address, city, post_code,phone,email,lat,lng,state,open_hours,services)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        
        try: 
            lat = float(store['location']['lat'])
            lng = float(store['location']['lon'])

        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        store = store['address']
        post_code = store['postcode']
        city = store['suburb']
        state = store['state']
        
        phone = store['phone']
        address = store['address_line1']
        address = address+store['address_line2'] if 'address_line2' in store.keys() and store['address_line2'] != '' else address

        email = store['email']
 
        return address,city,state,post_code,phone,email

    def get_open_hours(self, store):
       
        working_hours = store['open_hours']
        
        undefined_keys = list(working_hours.keys())
        last_day = list(working_hours['6'].keys())
        last_day = get_day_no(last_day[0])
        for key in undefined_keys:
            for day_check in working_hours[key]:
                if day_check == 'Today':
                    new_day = get_day_key(last_day+1)
                    working_hours[key][new_day] = working_hours[key][day_check]
                    del working_hours[key][day_check]
                if day_check == 'Tomorrow':
                    new_day = get_day_key(last_day+2)
                    working_hours[key][new_day] = working_hours[key][day_check]
                    del working_hours[key][day_check]

        opening_hours = []

        for keys in undefined_keys:
            for key,value in working_hours[keys].items():
                day_open_hours = {}
                day_open_hours['day'] = key
                
                day_open_hours['open_time'] = value['open'][:2]+':'+value['open'][2:] if value['open']!='' else ''
                day_open_hours['close_time'] = value['close'][:2]+':'+value['close'][2:] if value['close']!='' else ''

                opening_hours.append(day_open_hours)

        return get_open_hours_formatted(opening_hours) 