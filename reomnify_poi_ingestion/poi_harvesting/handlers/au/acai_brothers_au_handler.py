from interfaces.data_source_interface import *
from crud_database.constants import ACAI_BROTHERS_AU_COLUMNS

class AcaiBrothersAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Acai Brothers Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Acai Brothers stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = ACAI_BROTHERS_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Acai Brothers stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        html_content = get_url_html_content(url,headers = self.headers)
        soup = get_bs4_soup_content(html_content)
        script = soup.find('script',{'id':'map_data-js-extra'}).text
        stores = script.split('var acaiStoreLocator = ')[1].replace('/* ]]> */','')
        stores = stores.replace(';','')
        stores = json.loads(stores)
        stores = stores['locations']
        stores = stores.values()
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        
        title = store['title']

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code = self.get_store_address(store)

        open_hours = self.get_open_hours(store)

        phone = ''
        email = store['email']
        return (title,address, city, post_code,phone,email,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0
        try:
            lat = float(store['fullAddress']['lat'])
            lng = float(store['fullAddress']['lng'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        
        post_code = store['fullAddress']['post_code'] if 'post_code' in store['fullAddress'].keys() else ''
        post_code = post_code if post_code is not None else ''
        state = store['fullAddress']['state'] if 'state' in store['fullAddress'].keys() else ''
        city = store['fullAddress']['city'] if 'city' in store['fullAddress'].keys() else ''
        address = store['fullAddress']['address']
        
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
       
        
        return address,city,state,post_code


    def get_open_hours(self, store):
       
        store = store['tradingHours']
        opening_hours = []
           
        for key,value in store.items():
            day_open_hours = {}
            day_open_hours['day'] = key
            open_close_hours = value.split('-') if '-' in value else value.split('–')
            day_open_hours['open_time'] = open_close_hours[0].strip() if len(open_close_hours)>1 else ''
            day_open_hours['close_time'] = open_close_hours[1].strip() if len(open_close_hours)>0 else ''
            opening_hours.append(day_open_hours)
                   
        
        return get_open_hours_formatted(opening_hours)
        