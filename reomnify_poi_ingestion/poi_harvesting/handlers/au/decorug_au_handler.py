from interfaces.data_source_interface import *
from crud_database.constants import DECORUG_AU_COLUMNS

class DecoRugAuDataSourceParser(DataSourceInterface):

    def __init__(self, db_cursor, configuration):
        logger.info("Start Deco Rug Australia stores data source execution.")
        self.db_cursor = db_cursor
        self.configuration = configuration
        self.headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    
        }
 
        self.execute()
        super().__init__()

    def get_request_url(self):
        logger.info("Preparing data source url")

        origin = self.configuration['origin']

        path = self.configuration['path']
        path = '' if pd.isna(path) else path

        url = origin + path

        logger.info("URL: "+  url)

        return url

    def load_source_data(self, stores):
        logger.info("Load Deco Rug stores in to the table")
        
        DataSourceInterface.insert_data(stores, func = self.get_store_details, func2 = self.transform_source_data,
         data_source_columns = DECORUG_AU_COLUMNS, data_source_configuration=self.configuration)

    def transform_source_data(self, df):

        logger.info("Load Deco Rug stores in to the intermediate poi table")
        
        df = df.rename(columns={'name':"location_name", 'address':'street_address',
        'post_code':'postal_code','lng':'long','phone':'phone_number','state':'region'})

        df['iso_country_code'] = self.configuration['iso_country_code']
        df['weburl'] = self.configuration['origin']
        
        return df

    def execute(self):

        stores = self.get_stores()
        
        self.load_source_data(stores)

    def get_stores(self):

        url = self.get_request_url()
        
        html_content = get_url_html_content(url,headers=self.headers)
        soup = get_bs4_soup_content(html_content)
        
        stores = soup.find_all('div',{'class':'shg-c-lg-4 shg-c-md-4 shg-c-sm-4 shg-c-xs-12'})
        stores = list(filter(lambda store:'shg-map-container' in str(store),stores))
        logger.info('Total stores: '+str(len(stores)))

        return stores

    def get_store_details(self, store):

        title = store.find('strong').text.strip()

        new_store = store.text.strip()
        new_store = new_store.split(title)

        lat,lng = self.get_store_geo_coordinates(store)

        address,city,state,post_code,phone = self.get_store_address(new_store)
        try:
            new_address =  'Sh'+address.split('Sh')[1].strip() if 'Shopping' not in address else 'Shop S'+address.split('Shop S')[1]
            title = title+' '+address.split('Sh')[0].strip() if 'Shopping' not in address else title
        except:
            new_address =  address.split('Centre')[1].strip()+' Centre '
            title = title+' '+address.split('Centre')[0].strip()+' Centre'
        
        open_hours = ''

        return (title,new_address, city, post_code,phone,lat,lng,state,open_hours)

    def get_store_geo_coordinates(self,store):

        lat,lng = 0,0

        try:
            lat_lng = store.find('div',{'class':'shg-map-container'})
            lat = float(lat_lng['data-latitude'])
            lng = float(lat_lng['data-longitude'])
        except:
            lat,lng = 0,0

        return lat,lng

    def get_store_address(self,store):

        store = store[1].split('Ph:') if 'Ph:' in store[1] else store[1]
        post_code = ''
        state = ''
        city = ''
        phone = store[1].strip() if len(store)==2 else ''
        address = store[0].strip() if len(store)==2 else store.strip()
        
        if re.search('[0-9]{4}',address):
            post_code = re.findall('[0-9]{4}',address)
            post_code = post_code[0]
        if re.search('[A-Z]{3}',address):
            state = re.findall('[A-Z]{3}',address)
            state = state[0]
        else:
            state = re.findall('[A-Z]{2}',address)
            state = state[0]
        return address,city,state,post_code,phone
