from .au.red_rooster_au_handler import *
from .my.celcom_my_handler import *
from .my.carolina_herrera_my_handler import *
from .my.chilis_my_handler import *
from .my.my_dottys_my_handler import *
from .my.flash_gadgets_my_handler import *
from .my.fred_perry_my_handler import *
from .my.fuel_shack_my_handler import *
from .my.genki_sushi_my_handler import *
from .my.giordano_my_handler import *
from .my.glo_laser_centers_my_handler import *
from .my.valiram_my_handler import *
from .my.guess_my_handler import *
from .my.habib_jewels_my_handler import *
from .my.health_lane_family_pharmacy_my_handler import *
from .my.hla_my_handler import *
from .my.hublot_my_handler import *
from .my.huawei_my_handler import *
from .my.i_love_yoo_my_handler import *
from .my.innisfree_my_handler import *
from .my.insidescoop_my_handler import *
from .my.hai_lau_shan_my_handler import *
from .my.isetan_my_handler import *
from .my.chillipanmee_my_handler import *
from .my.juice_works_my_handler import *
from .my.ken_chan_curry_my_handler import *
from .my.kenzo_my_handler import *
from .my.kiehls_my_handler import *
from .my.koi_the_my_handler import *
from .my.lacoste_my_handler import *
from .my.lacucur_my_handler import *
from .my.lancome_my_handler import *
from .my.lego_my_handler import *
from .my.levis_my_handler import *
from .my.lamer_my_handler import *
from .my.loccitane_my_handler import *
from .my.laneige_my_handler import *
from .my.longchamp_my_handler import *
from .my.le_labo_my_handler import *
from .my.louis_vuitton_my_handler import *
from .my.lovisa_my_handler import *
from .my.mahnaz_food_my_handler import *
from .my.luk_fook_jewellery_my_handler import *
from .my.maje_my_handler import *
from .my.marks_and_spencers_my_handler import *
from .my.massimo_dutto_my_handler import *
from .my.pandora_my_handler import *
from .my.paris_miki_my_handler import *
from .my.patek_my_handler import *
from .my.versace_my_handler import *
from .my.prada_my_handler import *
from .my.vincci_my_handler import *
from .my.whoosh_my_handler import *
from .my.ysl_my_handler import *
from .my.yuzu_my_handler import *
from .au.body_catalyst_au_handler import *
from .au.chempro_au_handler import *
from .au.clearly_dental_au_handler import *
from .au.footmaster_au_handler import *
from .au.pillow_talk_au_handler import *
from .au.zap_fitness_au_handler import *
from .au.absolute_makeover_au_handler import *
from .au.acai_brothers_au_handler import *
from .au.ace_cinemas_au_handler import *
from .au.acmedelavie_au_handler import *
from .au.beacon_lighting_au_handler import *
from .au.beach_house_bargrill_au_handler import *
from .au.bedshed_au_handler import *
from .au.bendon_lingerie_au_handler import *
from .au.billbong_au_handler import *
from .au.camera_house_au_handler import *
from .au.blue_star_eye_wear_au_handler import *
from .au.my_daiso_japan_au_handler import *
from .au.david_jones_au_handler import *
from .au.decorug_au_handler import *
from .au.degani_au_handler import *
from .au.ebgames_au_handler import *
from .au.ecco_au_handler import *
from .au.gami_chicken_au_handler import *
from .au.politix_au_handler import *
from .au.petstock_au_handler import *
from .au.pharmacy_4_less_au_handler import *

class DataSourceFactory:
    """Data source factory"""

    def __init__(self):
        self._creators = {}

    def register_handler(self, handler, creator):
        self._creators[handler] = creator

    def get_data_source(self, handler):
        print("Called: Get data source:{0}".format(handler))
        creator = self._creators.get(handler)
        if not creator:
            print("Please register your data source")
            raise ValueError(handler)
        return creator


factory = DataSourceFactory()

factory.register_handler('red_rooster_au',RedRoosterAuDataSourceParser)
factory.register_handler('celcom_my',CelComMyDataSourceParser)
factory.register_handler('carolina_herrera_my',CarolinaHerreraMyDataSourceParser)
factory.register_handler('chilis_my',ChilisMyDataSourceParser)
factory.register_handler('my_dottys_my',MyDottysMyDataSourceParser)
factory.register_handler('flash_gadgets_my',FlashGadgetsMyDataSourceParser)
factory.register_handler('fred_perry_my',FredPerryMyDataSourceParser)
factory.register_handler('fuel_shack_my',FuelShackMyDataSourceParser)
factory.register_handler('genki_sushi_my',GenkiSushiMyDataSourceParser)
factory.register_handler('giordano_my',GiordanoMyDataSourceParser)
factory.register_handler('glo_laser_centers_my',GloLaserCentersMyDataSourceParser)
factory.register_handler('valiram_my',ValiramMyDataSourceParser)
factory.register_handler('guess_my',GuessMyDataSourceParser)
factory.register_handler('habib_jewels_my',HabibJewelsMyDataSourceParser)
factory.register_handler('health_lane_family_pharmacy_my',HealthLaneFamilyPharmacyMyDataSourceParser)
factory.register_handler('hla_my',HLAMyDataSourceParser)
factory.register_handler('hublot_my',HublotMyDataSourceParser)
factory.register_handler('huawei_my',HuaweiMyDataSourceParser)
factory.register_handler('i_love_yoo_my',ILoveYooMyDataSourceParser)
factory.register_handler('innisfree_my',InnisfreeMyDataSourceParser)
factory.register_handler('insidescoop_my',InsideScoopMyDataSourceParser)
factory.register_handler('hai_lau_shan_my',HaiLauShanMyDataSourceParser)
factory.register_handler('isetan_my',IsetanMyDataSourceParser)
factory.register_handler('chilli_pan_mee_my',ChilliPanMeeMyDataSourceParser)
factory.register_handler('juice_works_my',JuiceWorksMyDataSourceParser)
factory.register_handler('ken_chan_curry_my',KenChanCurryMyDataSourceParser)
factory.register_handler('kenzo_my',KenzoMyDataSourceParser)
factory.register_handler('kiehls_my',KiehlsMyDataSourceParser)
factory.register_handler('koi_the_my',KoiTheMyDataSourceParser)
factory.register_handler('lacoste_my',LacosteMyDataSourceParser)
factory.register_handler('lacucur_my',LacucurMyDataSourceParser)
factory.register_handler('lancome_my',LancomeMyDataSourceParser)
factory.register_handler('lego_my',LegoMyDataSourceParser)
factory.register_handler('levis_my',LevisMyDataSourceParser)
factory.register_handler('lamer_my',LamerMyDataSourceParser)
factory.register_handler('loccitane_my',LoccitaneMyDataSourceParser)
factory.register_handler('laneige_my',LaneigeMyDataSourceParser)
factory.register_handler('longchamp_my',LongChampMyDataSourceParser)
factory.register_handler('le_labo_my',LeLaboMyDataSourceParser)
factory.register_handler('louis_vuitton_my',LouisVuittonMyDataSourceParser)
factory.register_handler('lovisa_my',LovisaMyDataSourceParser)
factory.register_handler('mahnaz_food_my',MahnazFoodMyDataSourceParser)
factory.register_handler('luk_fook_jewellery_my',LukFookJewelleryMyDataSourceParser)
factory.register_handler('maje_my',MajeMyDataSourceParser)
factory.register_handler('marks_and_spencers_my',MarksAndSpencersMyDataSourceParser)
factory.register_handler('massimo_dutti_my',MassimoDuttiMyDataSourceParser)
factory.register_handler('pandora_my',PandoraMyDataSourceParser)
factory.register_handler('paris_miki_my',ParisMikiMyDataSourceParser)
factory.register_handler('patek_my',PatekMyDataSourceParser)
factory.register_handler('versace_my',VersaceMyDataSourceParser)
factory.register_handler('prada_my',PradaMyDataSourceParser)
factory.register_handler('vincci_my',VincciMyDataSourceParser)
factory.register_handler('whoosh_my',WhooshMyDataSourceParser)
factory.register_handler('ysl_my',YSLBeautyMyDataSourceParser)
factory.register_handler('yuzu_my',YuzuMyDataSourceParser)
factory.register_handler('body_catalyst_au',BodyCatalystAuDataSourceParser)
factory.register_handler('chempro_au',ChemproAuDataSourceParser)
factory.register_handler('clearly_dental_au',ClearlyDentalAuDataSourceParser)
factory.register_handler('footmaster_au',FootMasterAuDataSourceParser)
factory.register_handler('pillow_talk_au',PillowTalkAuDataSourceParser)
factory.register_handler('zap_fitness_au',ZapFitnessAuDataSourceParser)
factory.register_handler('absolute_makeover_au',AbsoluteMakeOverAuDataSourceParser)
factory.register_handler('acai_brothers_au',AcaiBrothersAuDataSourceParser)
factory.register_handler('ace_cinemas_au',AceCinemasAuDataSourceParser)
factory.register_handler('acmedelavie_au',AcmedelavieAuDataSourceParser)
factory.register_handler('beacon_lighting_au',BeaconLightingAuDataSourceParser)
factory.register_handler('beach_house_bargrill_au',BeachHouseBargrillAuDataSourceParser)
factory.register_handler('bedshed_au',BedshedAuDataSourceParser)
factory.register_handler('bendon_lingerie_au',BendonLingerieAuDataSourceParser)
factory.register_handler('billbong_au',BillBongAuDataSourceParser)
factory.register_handler('camera_house_au',CameraHouseAuDataSourceParser)
factory.register_handler('blue_star_eye_wear_au',BlueStarEyeWearAuDataSourceParser)
factory.register_handler('my_daiso_japan_au',MyDaisoJapanAuDataSourceParser)
factory.register_handler('david_jones_au',DavidJonesAuDataSourceParser)
factory.register_handler('decorug_au',DecoRugAuDataSourceParser)
factory.register_handler('degani_au',DeganiAuDataSourceParser)
factory.register_handler('ebgames_au',EbGamesAuDataSourceParser)
factory.register_handler('ecco_au',EccoAuDataSourceParser)
factory.register_handler('gami_chicken_au',GamiChickenAuDataSourceParser)
factory.register_handler('politix_au',PolitixAuDataSourceParser)
factory.register_handler('petstock_au',PetStockAuDataSourceParser)
factory.register_handler('pharmacy_4_less_au',Pharmacy4LessAuDataSourceParser)