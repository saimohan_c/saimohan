from .data_source_factory import *

def init(db_cursor,  configuration):
    handler = configuration['handler']

    parser = factory.get_data_source(handler)
                
    parser(db_cursor, configuration)