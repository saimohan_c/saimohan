import requests
import pandas
# import geopandas as gpd
import shapely.geometry
import json
import os
import traceback
from datetime import datetime
from reomnify_poi_ingestion.poi_harvesting.helpers.configurations import *
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from geopy import distance
from bs4 import BeautifulSoup

def get(url, headers=None, params=None,verify=True):

    retry_strategy = Retry(
        total=5,
        status_forcelist=[429, 500, 502, 503, 504],
        method_whitelist=["HEAD", "GET", "OPTIONS"]
    )

    adapter = HTTPAdapter(max_retries=retry_strategy)
    http = requests.Session()
    http.mount("https://", adapter)
    http.mount("http://", adapter)

    if headers is None and params is None:
        response = requests.get(url)

    if params is None and headers is not None:
        if verify == False:
            response = requests.get(url, headers=headers,verify=verify)
        else:
            response = requests.get(url, headers=headers)

    if params is not None and headers is None:
        if verify == False:
            response = requests.get(url, params=params,verify=verify)
        else:
            response = requests.get(url, params=params)

    if params is not None and headers is not None:
        if verify == False:
            response = requests.get(url, headers=headers, params=params,verify=verify)
        else:
            response = requests.get(url, headers=headers, params=params)

    try:
        response = response.json()
        
        return response
    except:
        return response

def post(url, headers=None, data=None, json=None,verify=True):

    retry_strategy = Retry(
        total=5,
        status_forcelist=[429, 500, 502, 503, 504],
    )

    adapter = HTTPAdapter(max_retries=retry_strategy)
    http = requests.Session()
    http.mount("https://", adapter)
    http.mount("http://", adapter)

    if data is None and json is None:
        response = requests.post(url,headers=headers,verify=verify)

    if data is not None and json is None:
        response = requests.post(url, headers=headers,data=data,verify=verify)

    if data is None and json is not None:
        response = requests.post(url,headers=headers, json=json,verify=verify)

    try:
        response = response.json()
        return response
    except:
        
        return response

# Get HTML content for specified URL
def get_url_html_content(url, headers,verify=True):
    response = requests.get(url, headers=headers,verify=verify)
    content = response.content
    return content

def get_bs4_soup_content(html_content):
    soup = BeautifulSoup(html_content,'html.parser')
    return soup

def get_12hr_format_time(time):
    try:
        if time.count(":") == 2:
            d = datetime.strptime(time, "%H:%M:%S")
        else:
            d = datetime.strptime(time, "%H:%M")

        return d.strftime("%I:%M%P").lower()
    except:
        return time.lower()

def get_day_key(day):
    day = str(day).lower().strip(" ")

    monday_keys = ['monday', 'mon', 'mo', '1']
    tuesday_keys = ['tuesday', 'tue', 'tu', 'tues', '2']
    wedensday_keys = ['wedensday', 'wednesday', 'we', 'wed', 'wednes','WEDNES','3']
    thursday_keys = ['thursday', 'thu', 'thurs','th', '4']
    friday_keys = ['friday', 'fri', 'fr','5']
    saturday_keys = ['saturday', 'sat','sa', '6']
    sunday_keys = ['sunday', 'sun', 'su','7','0']

    if day in monday_keys:
        return 'Mon'

    if day in tuesday_keys:
        return 'Tue'

    if day in wedensday_keys:
        return 'Wed'

    if day in thursday_keys:
        return 'Thu'

    if day in friday_keys:
        return 'Fri'

    if day in saturday_keys:
        return 'Sat'

    if day in sunday_keys:
        return 'Sun'

    return ''

def get_day_no(day):
    day = str(day).lower()

    monday_keys = ['monday', 'mon','mo','Mo', '1']
    tuesday_keys = ['tuesday', 'tue', 'tues','tu' ,'Tu','2']
    wedensday_keys = ['wedensday', 'wednesday', 'wed','We','we','wednes','WEDNES', '3']
    thursday_keys = ['thursday', 'thu', 'thur', 'thurs','Th' ,'th','4']
    friday_keys = ['friday', 'fri', 'Fr','fr','5']
    saturday_keys = ['saturday', 'sat','sa','Sa', '6']
    sunday_keys = ['sunday', 'sun','Su','su', '7','0']

    if day in monday_keys:
        return 1

    if day in tuesday_keys:
        return 2

    if day in wedensday_keys:
        return 3

    if day in thursday_keys:
        return 4

    if day in friday_keys:
        return 5

    if day in saturday_keys:
        return 6

    if day in sunday_keys:
        return 7

    return 0

def get_current_timestamp(frequency):

    timestamp = ''
    
    date_value = datetime.now()
    
    week = date_value.isocalendar()[1]
    
    month_year = str(date_value.month) + "_" + str(date_value.year)
    
    if frequency == 'monthly':
        timestamp = month_year
    elif frequency == 'weekly':
        timestamp = str(week) + '_' + month_year
    elif frequency == 'daily':
        timestamp = str(date_value.day) + "_" + str(week)+"_" + month_year
        
    return timestamp

def get_open_hours_formatted(opening_hours):
    opening_hours_formatted = "{"

    for day_open_hours in opening_hours:
        day = get_day_key(str(day_open_hours['day']).strip(" "))

        open_time = day_open_hours['open_time']
        close_time = day_open_hours['close_time']

        if day == '' or open_time == '' or close_time == '':
            continue

        if day not in opening_hours_formatted:
            opening_hours_formatted += "{'"+day+"':[['" + get_12hr_format_time(open_time) + "','"+get_12hr_format_time(close_time) + "']]},"

    opening_hours_formatted = opening_hours_formatted.strip(",")
    opening_hours_formatted += "}"
    opening_hours_formatted = opening_hours_formatted.replace(" ", '')

    if opening_hours_formatted == '{}':
        opening_hours_formatted = ''
            
    return opening_hours_formatted

def get_state_code(state):
    
    if state is None:
        return ''

    state = state.strip(" ")
    state = state.lower()

    if state in ['victoria','vic']:
        return 'VIC'

    if state in ['queensland','qld','que']:
        return 'QLD'    

    if state in ['new south wales','nsw']:
        return 'NSW'        

    if state in ['australian capital territory','act']:
        return 'ACT'

    if state in ['tasmania','tas']:
        return 'TAS'

    if state in ['western australia','wa']:
        return 'WA'

    if state in ['south australia','sa']:
        return 'SA'    

    if state in ['northern territory','nt']:
        return 'NT'

    if state in ['singapore','sg']:
        return 'Singapore'
        
    return ''

def get_current_time():
    current_time = datetime.now()
    
    return current_time
