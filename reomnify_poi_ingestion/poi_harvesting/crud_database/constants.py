POI_MERGED_COLUMNS =  [ 'ro_place_id', 'parent_ro_place_id', 'location_name', 'location_name_cleansed', 
                    'lat', "long", 'street_address', 'city', 'region', 'postal_code',
                    'phone_number', 'email', 'fax', 'weburl','open_hours','last_relevance_date',
                    'category_tags','top_category', 'sub_category', 'iso_country_code','ro_brand_ids', 'brands',
                    'ds_instance_id', 'timestamp', 'created_at']

RED_ROOSTER_AU_COLUMNS = ['store_id', 'store_no', 'title', 'slug', 'catering', 'delivery', 'catering_pickup', 'catering_delivery', 'drive_thru', 'delivery_aggregators', 'catering_towncity', 'catering_post_code', 'catering_url', 'gsn', 'address', 'city', 'post_code', 'phone', 'lat', 'long', 'state', 'open_hours', 'ro_place_id', 'ds_instance_id', 'timestamp', 'created_at']

SOUL_ORIGIN_AU_COLUMNS = ['store_id','name','lat', 'long', 'address', 'city','state', 'post_code','open_hours','ro_place_id','ds_instance_id', 'timestamp', 'created_at']

CELCOM_MY_COLUMNS = ['store_id','name','lat', 'long', 'address','city','state', 'post_code','ro_place_id','ds_instance_id', 'timestamp', 'created_at']

CAROLINA_HERRERA_MY_COLUMNS = ['store_id', 'name','address', 'city', 'post_code','country','lat', 'long', 'state', 'open_hours', 'ro_place_id', 'ds_instance_id', 'timestamp', 'created_at']

CHILIS_MY_COLUMNS = ['name','address', 'city', 'post_code','lat', 'long', 'state', 'open_hours', 'ro_place_id', 'ds_instance_id', 'timestamp', 'created_at']

MY_DOTTYS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours', 'ro_place_id', 'ds_instance_id', 'timestamp', 'created_at']

FLASH_GADGETS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

FRED_PERRY_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

FUEL_SHACK_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

GENKI_SUSHI_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

GIORDANO_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

GLO_LASER_CENTERS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

VALIRAM_MY_COLUMNS = ['name','brand','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

GUESS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

HABIB_JEWELS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

HEALTH_LANE_FAMILY_PHARMACY_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

HLA_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

HUBLOT_MY_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

HUAWEI_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

I_LOVE_YOO_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

INNISFREE_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

INSIDESCOOP_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

HAI_LAU_SHAN_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

ISETAN_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

CHILLIPANMEE_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

JUICE_WORKS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

KEN_CHAN_CURRY_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

KENZO_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

KIEHLS_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

KOI_THE_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LACOSTE_MY_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LACUCUR_MY_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LANCOME_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LAMER_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LEGO_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LEVIS_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LANEIGE_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LOCCITANE_MY_COLUMNS = ['store_id','store_code','name','address','city','post_code','phone','lat','long','state','open_hours','category','channel','store_type','PickInStore','ShipToStore','isRetailer','isPushedOnline','ro_place_id','ds_instance_id','timestamp','created_at']

LONGCHAMP_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LE_LABO_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LOUIS_VUITTON_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','product_categories','ro_place_id','ds_instance_id','timestamp','created_at']

LOVISA_MY_COLUMNS = ['store_id','branch_id','name','address','city','post_code','phone','lat','long','state','open_hours','has_piercing','has_cc','ro_place_id','ds_instance_id','timestamp','created_at']

MAHNAZ_FOOD_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

LUK_FOOK_JEWELLERY_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

MAJE_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

MARKS_AND_SPENCERS_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','product_categories','ro_place_id','ds_instance_id','timestamp','created_at']

MASSIMO_DUTTI_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

PANDORA_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','type','store_type', 'ro_place_id','ds_instance_id','timestamp','created_at']

PARIS_MIKI_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

PATEK_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

VERSACE_MY_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','store_type','ro_place_id','ds_instance_id','timestamp','created_at']

PRADA_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

VINCCI_MY_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

WHOOSH_MY_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

YSL_MY_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

YUZU_MY_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

BODY_CATALYST_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

CHEMPRO_AU_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

CLEARLY_DENTAL_AU_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

FOOT_MASTER_AU_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

PILLOW_TALK_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

ZAP_FITNESS_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','slug','facilities_provided','ro_place_id','ds_instance_id','timestamp','created_at']

ABSOLUTE_MAKEOVER_AU_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

ACAI_BROTHERS_AU_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

ACE_CINEMAS_AU_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

ACMEDELAVIE_AU_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

BEACON_LIGHTING_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','fax','services','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

BEACH_HOUSE_BARGRILL_AU_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

BEDSHED_AU_COLUMNS = ['name','address','city','post_code','phone','email','fax','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

BENDON_LINGERIE_AU_COLUMNS = ['name','address','city','post_code','phone','fax','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

BILLBONG_LIGHTING_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

CAMERA_HOUSE_AU_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

BLUE_STAR_EYE_WEAR_AU_COLUMNS = ['name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

MY_DAISO_JAPAN_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','fax','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

DAVID_JONES_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

DECORUG_AU_COLUMNS = ['name','address','city','post_code','phone','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

DEGANI_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

EBGAMES_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','ro_place_id','ds_instance_id','timestamp','created_at']

ECCO_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','product_categories','iseccostore','ro_place_id','ds_instance_id','timestamp','created_at']

GAMI_CHICKEN_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','lat','long','state','open_hours','slug','ro_place_id','ds_instance_id','timestamp','created_at']

POLITIX_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','fax','lat','long','state','open_hours','store_type','ro_place_id','ds_instance_id','timestamp','created_at']

PETSTOCK_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','services','ro_place_id','ds_instance_id','timestamp','created_at']

PHARMACY_4_LESS_AU_COLUMNS = ['store_id','name','address','city','post_code','phone','email','lat','long','state','open_hours','isactive','covid_vaccine_store','ro_place_id','ds_instance_id','timestamp','created_at']
